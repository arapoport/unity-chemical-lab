/*------------------------------------------------
// CUBEMAPPER
// Version: See Readme
// Created by: Rainer Liessem
// Website: http://www.spreadcamp.com
//
// PLEASE RESPECT THE LICENSE TERMS THAT YOU
// AGREED UPON WITH YOUR PURCHASE OF THIS ASSET
------------------------------------------------*/
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cubemapper
{
    public class CubemapWindow : EditorWindow
    {
        #region Variables
        CubemapGenerator manager = null;
        public CubemapNode[] nodes; // array of nodes to generate from, can be filled from other scripts

        // Cubemap Resolution
        string[] resolutions = { "32x32", "64x64", "128x128", "256x256", "512x512", "1024x1024", "2048x2048" };
        int[] resSizes = { 32, 64, 128, 256, 512, 1024, 2048 };

        // Camera Clear Flags
        string[] m_camClearFlagOptions = { "Skybox", "Solid Color", "Depth only", "Don't Clear" };
        int m_camClearFlagSelected = 0;

        // Window Settings
        Vector2 scrollPos;
        bool generationInProgress = false;

        bool showAdvancedSettings = false;

        string currentMenu = "Generate";
        float horizontalWidth = 325f;

        string regPrefix = "cubemapper_"; // for EditorPrefs

        // Images
        List<Texture> windowImages = new List<Texture>();
        Texture img_bttn_generate = null;
        Texture img_bttn_assignment = null;
        Texture img_bttn_tools = null;
        Texture img_bttn_settings = null;
        Texture img_bttn_addNode = null;

        // User Settings
        bool centerNodesOnScreen = true;
        float nodeDistanceToCam = 2f;
        bool previewNodes = true;

        // Variables to pass to Cubemap Manager
        string cm_pathCubemaps = "Assets/Cubemapper/Generated Cubemaps";
        string cm_pathCubemapPNG = "Cubemapper/Generated Cubemap Textures";
        bool cm_useLinearSpace = false;
        bool cm_useMipMaps = false;
        int cm_resolution = 32;
        bool cm_makePNG = false;
        float cm_mipmapBias = 0.0f;
        bool cm_smoothEdges = false;
        int cm_smoothEdgeWidth = 1;

        CameraClearFlags cm_camClearFlags = CameraClearFlags.Skybox;
        Color cm_camBGColor = new Color(0.192f, 0.302f, 0.475f, 0.020f);
        LayerMask cm_cullingMask = -1;
        float cm_farClipPlane = 1000f;
        #endregion


        #region Initializing and Destroying of the Window
        [MenuItem("Window/Cubemapper")]
        static void Init()
        {
            if (Path.GetFileNameWithoutExtension(EditorApplication.currentScene).Length == 0)
            {
                EditorUtility.DisplayDialog("Please save your Scene first!", "You can't use the Cubemapper on unsaved Scenes.", "OK");
                return;
            }

            // Window Set-Up
            CubemapWindow window = EditorWindow.GetWindow(typeof(CubemapWindow), false, "Cubemapper", true) as CubemapWindow;
            window.minSize = new Vector2(341, 250);
            window.autoRepaintOnSceneChange = true;
            window.Show();
        }

        // Window opens...
        void OnEnable()
        {
            // Destroy old instances of the manager
            DestroyManager();

            // Load Settings
            LoadPrefs();

            // Load our Images for later use throughout the window
            LoadImages();
        }

        // Window closes...
        void OnDestroy()
        {
            DestroyManager(); // Destroy old instances of the manager
            SavePrefs();
        }
        #endregion

        #region Editor GUI and Menus
        void OnGUI()
        {
            // Set Window Title Icon
            Texture windowIcon = GetImage("CIMG_WINDOWICON");
            CMEditor.SetWindowTitle(this, windowIcon);

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

            // Don't display anything if we are generating
            if (generationInProgress)
            {
                EditorGUILayout.BeginHorizontal();
                CMEditor.Helpbox("Creating Cubemaps, please wait... do NOT stop playback!", MessageType.Warning);
                EditorGUILayout.EndHorizontal();
            }
            // Proceed with display
            else
            {
                MenuButtons();
                //EditorGUILayout.Space();				
                ShowMenu(currentMenu);
            }

            EditorGUILayout.EndScrollView();

            if (GUI.changed)
                SavePrefs();

            Repaint();
        }

        void MenuButtons()
        {
            GUILayoutOption[] menuButtonLayout = new GUILayoutOption[] { GUILayout.Height(55), GUILayout.Width(60) };
            Color buttonColorDefault = Color.gray;
            Color buttonColorHover = CMEditor.ColorRed;

            // Load Images (if they are null)
            if (img_bttn_generate == null)
                img_bttn_generate = GetImage("CIMG_BTTN_GENERATE");

            if (img_bttn_assignment == null)
                img_bttn_assignment = GetImage("CIMG_BTTN_ASSIGNMENT");

            if (img_bttn_addNode == null)
                img_bttn_addNode = GetImage("CIMG_BTTN_ADDNODE");

            if (img_bttn_tools == null)
                img_bttn_tools = GetImage("CIMG_BTTN_TOOLS");

            if (img_bttn_settings == null)
                img_bttn_settings = GetImage("CIMG_BTTN_SETTINGS");


            // BUTTONS
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            {
                // Generate Button
                GUI.backgroundColor = (currentMenu == "Generate") ? buttonColorHover : buttonColorDefault;
                if (GUILayout.Button(img_bttn_generate, menuButtonLayout))
                {
                    currentMenu = "Generate";
                }

                // Assign Button
                GUI.backgroundColor = (currentMenu == "Assignment") ? buttonColorHover : buttonColorDefault;
                if (GUILayout.Button(img_bttn_assignment, menuButtonLayout))
                {
                    currentMenu = "Assignment";
                }

                // Node Button with Context Menu
                GUI.backgroundColor = (currentMenu == "Nodes") ? buttonColorHover : buttonColorDefault;
                if (GUILayout.Button(img_bttn_addNode, menuButtonLayout))
                {
                    GenericMenu nodesMenu = new GenericMenu();

                    nodesMenu.AddItem(new GUIContent("Create New Node"), false, CreateNode);

                    nodesMenu.AddSeparator("");

                    if (currentMenu == "Nodes")
                        nodesMenu.AddItem(new GUIContent("Node Management"), true, OpenNodesMenu);
                    else
                        nodesMenu.AddItem(new GUIContent("Node Management"), false, OpenNodesMenu);

                    /*
                    nodesMenu.AddSeparator("");
                
                    if (Selection.activeGameObject != null)
                        nodesMenu.AddItem(new GUIContent("Delete selected Node"), false, DeleteSelectedNode);
                    else
                        nodesMenu.AddDisabledItem(new GUIContent("Delete selected Node"));
                     */

                    // Offset menu from right of editor window
                    nodesMenu.DropDown(new Rect(Screen.width - 200, 45, 0, 16));
                    //EditorGUIUtility.ExitGUI();
                }

                // Tools Button		
                GUI.backgroundColor = (currentMenu == "Tools") ? buttonColorHover : buttonColorDefault;
                if (GUILayout.Button(img_bttn_tools, menuButtonLayout))
                {
                    currentMenu = "Tools";
                }

                // Settings Button	
                GUI.backgroundColor = (currentMenu == "Settings") ? buttonColorHover : buttonColorDefault;
                if (GUILayout.Button(img_bttn_settings, menuButtonLayout))
                {
                    currentMenu = "Settings";
                }

                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndHorizontal();

            /*
            // Supercool Submenu Experiment, but won't use for now because right now IMO it detracts from the simplicity. Might come back to it later
            GUILayoutOption[] submenuButtonLayout = new GUILayoutOption[] { GUILayout.Height(30), GUILayout.Width(78) };

            GUIStyle submenuButton = new GUIStyle("Button");
            submenuButton.padding = new RectOffset(0, 0, 0, 0);
            submenuButton.margin = new RectOffset(0, 1, 0, 0);

            GUIStyle submenuButtonLast = new GUIStyle("Button");
            submenuButtonLast.padding = submenuButton.padding;
            submenuButtonLast.margin = new RectOffset(submenuButton.margin.left, 0, submenuButton.margin.top, submenuButton.margin.bottom);

            GUIStyle submenuBox = new GUIStyle("HelpBox");
            submenuBox.padding = new RectOffset(0, 0, 0, 0);

            GUI.backgroundColor = Color.black;
            EditorGUILayout.BeginHorizontal(submenuBox, GUILayout.MaxWidth(horizontalWidth - 10));
            {
                GUILayout.FlexibleSpace();
                GUI.backgroundColor = (currentMenu == "TODO") ? buttonColorHover : buttonColorDefault;
                if (GUILayout.Button(img_bttn_settings, submenuButton, submenuButtonLayout))
                {
                    // empty
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button(img_bttn_settings, submenuButton, submenuButtonLayout))
                {
                    // empty
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button(img_bttn_settings, submenuButton, submenuButtonLayout))
                {
                    // empty
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button(img_bttn_settings, submenuButtonLast, submenuButtonLayout))
                {
                    // empty
                }
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
            GUI.backgroundColor = Color.white;
            */

            CMEditor.Separator();
        }


        void ShowMenu(string menu)
        {
            // Make sure we have only one Node container in the Scene
            if (CMGenerate.CountNodeContainers > 1)
            {
                EditorGUILayout.BeginHorizontal();
                CMEditor.Helpbox("It looks like there is more than one Node Container present in the scene. Please make sure there is only one Container before proceeding!", MessageType.Error);
                EditorGUILayout.EndHorizontal();
            }
            // Check if we have made nodes in the scene
            else if (CMGenerate.CountNodes == 0)
            {
                EditorGUILayout.BeginHorizontal();
                CMEditor.Helpbox("Please add one or more Cubemap Nodes to your Scene. You can't generate Cubemaps until Nodes are present.\n\nNodes are used to indicate where in your Environment you'd like to take Cubemaps from. They also are used for the One-Click Assign of your Cubemaps to Cubemap Users (see Readme)", MessageType.Warning);
                EditorGUILayout.EndHorizontal();
            }
            // We have Nodes, we can now proceed using the rest of the System...
            else
            {
                if (menu == null) return;

                switch (menu)
                {
                    case "Generate": Menu_Generate(); break;
                    case "Assignment": Menu_Assignment(); break;
                    case "Nodes": Menu_Nodes(); break;
                    case "Tools": Menu_Tools(); break;
                    case "Settings": Menu_Settings(); break;
                }
            }
        }

        void Menu_Generate()
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(horizontalWidth));
            {
                CMEditor.Headline("Generate Cubemaps", "To generate Cubemaps, configure your settings here and then press the button.");

                cm_makePNG = EditorGUILayout.Toggle("Generate PNGs? (slow)", cm_makePNG);
                cm_resolution = EditorGUILayout.IntPopup("Resolution: ", cm_resolution, resolutions, resSizes);

                // Known Issue Warning (Only Unity Pro, I guess Playmode gives more power to process Linear faster than from only in-editor)
                if (UnityEditorInternal.InternalEditorUtility.HasPro() && (cm_resolution >= 256) && cm_useLinearSpace)
                {
                    CMEditor.Helpbox("This resolution together with Linear Space might result in a short freeze of the editor after the generation. Don't worry, it will recover. Recovery time depends on the selected resolution. To 'fix' it try to reduce Cubemap resolution again or disable \"Use Linear Space\" in the Advanced Settings.\n\nThe cause of the freeze is something internal to Unity that I cannot fix, hence this warning.", MessageType.Warning);
                }

                EditorGUILayout.Space();

                // Advanced Options Foldout
                if (!showAdvancedSettings)
                    showAdvancedSettings = CMEditor.Foldout(showAdvancedSettings, "Advanced Settings (click to expand)", true, EditorStyles.foldout);
                else
                {
                    // Fancy Time - Prettier Foldout for a pretty box
                    GUI.color = Color.white;
                    GUI.backgroundColor = Color.gray;
                    showAdvancedSettings = CMEditor.Foldout(showAdvancedSettings, "Advanced Settings", true, CMEditor.styleFoldoutTab);
                    GUI.backgroundColor = Color.white;
                    GUI.color = Color.white;

                    EditorGUILayout.BeginVertical(CMEditor.styleFoldoutContent);
                    {
                        CMEditor.SmallHeadline("Misc. Settings");

                        cm_useMipMaps = EditorGUILayout.Toggle("Mip Maps?", cm_useMipMaps);
                        cm_mipmapBias = cm_useMipMaps ? EditorGUILayout.IntSlider("Mip Map Bias", (int)cm_mipmapBias, 0, 6) : 0;

                        cm_useLinearSpace = EditorGUILayout.Toggle("Use Linear Space", cm_useLinearSpace);

                        cm_smoothEdges = EditorGUILayout.Toggle("Smooth Edges?", cm_smoothEdges);
                        if (cm_smoothEdges)
                        {
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.BeginVertical(GUILayout.Width(150));
                                {
                                    EditorGUILayout.LabelField("Edge Smooth Width:", GUILayout.Width(150));
                                }
                                EditorGUILayout.EndVertical();

                                EditorGUILayout.BeginVertical(GUILayout.Width(30));
                                {
                                    cm_smoothEdgeWidth = EditorGUILayout.IntField(cm_smoothEdgeWidth, GUILayout.Width(30));
                                }
                                EditorGUILayout.EndVertical();

                                EditorGUILayout.BeginVertical(GUILayout.Width(30));
                                {
                                    EditorGUILayout.LabelField("px", GUILayout.Width(30));
                                }
                                EditorGUILayout.EndVertical();
                            }
                            EditorGUILayout.EndHorizontal();
                        }

                        EditorGUILayout.Space();

                        CMEditor.SmallHeadline("Camera Settings");

                        m_camClearFlagSelected = EditorGUILayout.Popup("Clear Flags", m_camClearFlagSelected, m_camClearFlagOptions);
                        cm_camClearFlags = CMEditor.SetClearFlagFromInt(m_camClearFlagSelected);
                        if (cm_camClearFlags == CameraClearFlags.Color || cm_camClearFlags == CameraClearFlags.Skybox)
                        {
                            cm_camBGColor = EditorGUILayout.ColorField("Background Color:", cm_camBGColor);
                        }

                        cm_cullingMask = CMEditor.LayerMaskField("Culling Mask", cm_cullingMask, true);
                        cm_farClipPlane = EditorGUILayout.FloatField("Far Clipping Plane", cm_farClipPlane);
                    }
                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.Space();

                GUI.backgroundColor = CMEditor.ColorGreen;
                if (GUILayout.Button("Generate Cubemaps!", GUILayout.Height(40)))
                {
                    nodes = CMGenerate.GetAllNodes(); // assign all nodes now before generate
                    InitGeneration();
                }
                else
                    DestroyManager();

                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndVertical();
        }

        void OpenNodesMenu()
        {
            currentMenu = "Nodes";
        }

        void Menu_Nodes()
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(horizontalWidth));
            {
                CMEditor.Headline("Node Management", "Tells you at a glance if your Nodes are set up correctly for using features like blending between Nodes. More detailed settings are available in the Inspector if you select a Node.");

                EditorGUILayout.BeginVertical("HelpBox");
                {
                    EditorGUILayout.BeginVertical(GUILayout.Width(horizontalWidth - 20));
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.BeginVertical();
                            {
                                CMEditor.SmallHeadline("Overview of Nodes in Scene");
                                EditorGUILayout.LabelField(CMGenerate.CountNodes + " Nodes found", EditorStyles.miniBoldLabel);
                            }
                            EditorGUILayout.EndVertical();

                            EditorGUILayout.BeginVertical();
                            {
                                GUILayoutOption[] menuButtonLayout = new GUILayoutOption[] { GUILayout.Height(55), GUILayout.Width(60) };

                                // Add Node Button
                                if (img_bttn_addNode == null)
                                    img_bttn_addNode = GetImage("CIMG_BTTN_ADDNODE");

                                GUI.backgroundColor = Color.gray;
                                if (GUILayout.Button(img_bttn_addNode, menuButtonLayout))
                                    CreateNode();
                            }
                            EditorGUILayout.EndVertical();
                        }
                        EditorGUILayout.EndHorizontal();

                        if (CMGenerate.CountNodes > 0)
                        {
                            CubemapNode[] nodes = CMGenerate.GetAllNodes();
                            nodes = nodes.OrderBy(item => item.name).ToArray(); // Sort Alphanumeric

                            if (!CMGenerate.NodesHaveCubemaps())
                            {
                                CMEditor.Helpbox("Some Nodes don't seem to have a Cubemap assigned yet. That's okay, it only means some advanced features are unavailable.", MessageType.Info);
                            }

                            CMEditor.Separator();

                            //EditorGUILayout.LabelField("Color Legend", EditorStyles.boldLabel);

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.BeginVertical(GUILayout.Width(20));
                                {
                                    GUI.backgroundColor = Color.green;
                                    EditorGUILayout.HelpBox("\t", MessageType.None);
                                    GUI.backgroundColor = Color.white;
                                }
                                EditorGUILayout.EndVertical();

                                GUI.color = CMEditor.ColorGreen;
                                GUILayout.Label("Cubemap assigned, Generate enabled");
                                GUI.color = Color.white;
                            }
                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.BeginVertical(GUILayout.Width(20));
                                {
                                    GUI.backgroundColor = Color.yellow;
                                    EditorGUILayout.HelpBox("\t", MessageType.None);
                                    GUI.backgroundColor = Color.white;
                                }
                                EditorGUILayout.EndVertical();

                                GUI.color = CMEditor.ColorOrange;
                                GUILayout.Label("Cubemap assigned, Generate disabled");
                                GUI.color = Color.white;
                            }
                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.BeginVertical(GUILayout.Width(20));
                                {
                                    GUI.backgroundColor = Color.red;
                                    EditorGUILayout.HelpBox("\t", MessageType.None);
                                    GUI.backgroundColor = Color.white;
                                }
                                EditorGUILayout.EndVertical();

                                GUI.color = CMEditor.ColorRed;
                                GUILayout.Label("No Cubemap assigned");
                                GUI.color = Color.white;
                            }
                            EditorGUILayout.EndHorizontal();

                            /*
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.BeginVertical(GUILayout.Width(20));
                                {
                                    GUI.backgroundColor = Color.gray;
                                    EditorGUILayout.HelpBox("\t", MessageType.None);
                                    GUI.backgroundColor = Color.white;
                                }
                                EditorGUILayout.EndVertical();

                                GUILayout.Label("Undetermined");
                            }
                            EditorGUILayout.EndHorizontal();
                            */

                            // List of Nodes 
                            for (int i = 0; i < nodes.Length; i++)
                            {
                                CMEditor.Separator(1f);

                                Color bgColor;

                                if (nodes[i].allowCubemapGeneration && nodes[i].cubemap != null)
                                    bgColor = CMEditor.ColorGreen;
                                else if (!nodes[i].allowCubemapGeneration && nodes[i].cubemap != null)
                                    bgColor = CMEditor.ColorOrange;
                                else if (nodes[i].cubemap == null)
                                    bgColor = CMEditor.ColorRed;
                                else
                                    bgColor = Color.gray;

                                GUI.backgroundColor = bgColor;
                                EditorGUILayout.BeginHorizontal(CMEditor.styleTableRow);
                                {
                                    EditorGUILayout.BeginVertical();
                                    {
                                        GUILayout.Label("#" + (i + 1).ToString(), EditorStyles.boldLabel, GUILayout.Width(20), GUILayout.ExpandWidth(true));
                                    }
                                    EditorGUILayout.EndVertical();

                                    EditorGUILayout.BeginVertical();
                                    {
                                        GUILayout.Label(nodes[i].name, EditorStyles.wordWrappedLabel, GUILayout.Width(160), GUILayout.ExpandWidth(false));
                                    }
                                    EditorGUILayout.EndVertical();

                                    EditorGUILayout.BeginVertical();
                                    {
                                        EditorGUILayout.BeginHorizontal();
                                        {
                                            GUI.backgroundColor = CMEditor.ColorGreen;
                                            if (GUILayout.Button("Select"))
                                            {
                                                Selection.activeGameObject = nodes[i].gameObject;
                                            }

                                            GUI.backgroundColor = CMEditor.ColorRed;
                                            if (GUILayout.Button("Delete"))
                                            {
                                                if (EditorUtility.DisplayDialog("Node Deletion", "Are you sure you want to delete " + nodes[i].name + " ?", "Yes", "No"))
                                                {
                                                    Undo.DestroyObjectImmediate(nodes[i].gameObject);
                                                }
                                            }
                                            GUI.backgroundColor = Color.white;
                                        }
                                        EditorGUILayout.EndHorizontal();
                                    }
                                    EditorGUILayout.EndVertical();
                                }
                                EditorGUILayout.EndHorizontal();
                                GUI.backgroundColor = Color.white;
                            }
                        }
                        else
                            EditorGUILayout.LabelField("No Nodes found in scene.", EditorStyles.miniBoldLabel);
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndVertical();

                if (GUILayout.Button("Back to top"))
                {
                    scrollPos = Vector2.zero;
                }
            }
            EditorGUILayout.EndVertical();
        }

        void Menu_Assignment()
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(horizontalWidth));
            {
                CMEditor.Headline("Cubemap One-Click Assignment", "Quickly assign Cubemaps to your objects and create Cubemap Users for advanced features (see Readme)");

                if (CMGenerate.CountUsers > 0)
                {
                    GUI.backgroundColor = CMEditor.ColorBlue;
                    if (GUILayout.Button("Assign to Nodes + Users", GUILayout.Height(40)))
                        CMGenerate.AssignCubemapsToAll();
                    GUI.backgroundColor = Color.white;
                }

                EditorGUILayout.BeginHorizontal();
                {
                    if (CMGenerate.CountUsers > 0)
                    {
                        GUI.backgroundColor = CMEditor.ColorBlue;
                        if (GUILayout.Button("Assign to Users", GUILayout.Height(40)))
                            CMGenerate.AssignCubemapsToUsers();
                        GUI.backgroundColor = Color.white;
                    }

                    GUI.backgroundColor = CMEditor.ColorBlue;
                    if (GUILayout.Button("Assign to Nodes", GUILayout.Height(40)))
                    {
                        bool assignedToNodes = false;
                        CMGenerate.AssignCubemapsToNodes(out assignedToNodes);
                    }
                    GUI.backgroundColor = Color.white;
                }
                EditorGUILayout.EndHorizontal();

                // Show a Button to report which Nodes don't have Cubemaps
                if (!CMGenerate.NodesHaveCubemaps())
                {
                    CMEditor.Helpbox("Some Nodes don't seem to have a Cubemap assigned yet. If you want to use advanced features they need to be generated and assigned to your Nodes.", MessageType.Info);
                    EditorGUILayout.BeginHorizontal();
                    {
                        GUI.backgroundColor = CMEditor.ColorOrange;
                        if (GUILayout.Button("Show Nodes with missing Cubemaps", GUILayout.Height(25)))
                        {
                            OpenNodesMenu();
                        }
                        GUI.backgroundColor = Color.white;
                    }
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();
                CMEditor.Separator(1f);

                CMEditor.Headline("Cubemap Users");
                CMEditor.Helpbox("For realtime switching you need to assign Cubemap Users here. Select a game object, then press Convert.", MessageType.Info);

                // Button to add users
                EditorGUILayout.BeginHorizontal();
                {
                    if (Selection.gameObjects.Length > 0)
                    {
                        GUI.backgroundColor = CMEditor.ColorGreen;
                        if (GUILayout.Button("Convert Selection(s) to Cubemap User", GUILayout.Height(35), GUILayout.MinWidth(130)))
                        {
                            MakeTransformsToUser();
                        }
                        GUI.backgroundColor = Color.white;
                    }
                    else
                    {
                        GUI.backgroundColor = Color.gray;
                        GUILayout.Button("Convert Selection(s) to Cubemap User", GUILayout.Height(35), GUILayout.MinWidth(130));
                        GUI.backgroundColor = Color.white;
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                // List current Users found in scene
                EditorGUILayout.BeginVertical("HelpBox");
                {
                    EditorGUILayout.BeginVertical(GUILayout.Width(horizontalWidth - 20));
                    {
                        CMEditor.SmallHeadline("Overview of \"Cubemap Users\" in Scene");

                        if (CMGenerate.CountUsers > 0)
                        {
                            CubemapUser[] users = CMGenerate.GetAllUsers();
                            users = users.OrderBy(item => item.name).ToArray(); // Sort Alphanumeric

                            EditorGUILayout.LabelField(CMGenerate.CountUsers + " Users found", EditorStyles.miniBoldLabel);

                            for (int i = 0; i < users.Length; i++)
                            {
                                CMEditor.Separator(1f);

                                bool darker = ((i % 2) == 0);
                                GUI.backgroundColor = (darker) ? CMEditor.colorTableRow1 : CMEditor.colorTableRow2;
                                EditorGUILayout.BeginHorizontal(CMEditor.styleTableRow);
                                {
                                    EditorGUILayout.BeginVertical();
                                    {
                                        GUILayout.Label("#" + (i + 1).ToString(), EditorStyles.boldLabel, GUILayout.Width(20), GUILayout.ExpandWidth(true));
                                    }
                                    EditorGUILayout.EndVertical();

                                    EditorGUILayout.BeginVertical();
                                    {
                                        GUILayout.Label(users[i].name, EditorStyles.wordWrappedLabel, GUILayout.Width(180), GUILayout.ExpandWidth(false));
                                    }
                                    EditorGUILayout.EndVertical();

                                    EditorGUILayout.BeginVertical();
                                    {
                                        GUI.backgroundColor = CMEditor.ColorGreen;
                                        if (GUILayout.Button("Select"))
                                        {
                                            Selection.activeGameObject = users[i].gameObject;
                                        }
                                        GUI.backgroundColor = Color.white;
                                    }
                                    EditorGUILayout.EndVertical();
                                }
                                EditorGUILayout.EndHorizontal();
                                GUI.backgroundColor = Color.white;
                            }
                        }
                        else
                            EditorGUILayout.LabelField("No Cubemap Users currently active.", EditorStyles.miniBoldLabel);
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();

                if (GUILayout.Button("Back to top"))
                {
                    scrollPos = Vector2.zero;
                }
            }
            EditorGUILayout.EndVertical();
        }

        void Menu_Tools()
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(horizontalWidth - 10f));
            {
                CMEditor.Headline("Tools", "You might find these additional tools useful when working with Cubemaps. Tools marked as \"Experimental\" should be used at your own risk and could produce unexpected results.");

                GUI.backgroundColor = CMEditor.ColorBlue;

                GUILayout.Space(5);

                if (GUILayout.Button("Extract PNG from existing Cubemap", GUILayout.Height(35)))
                {
                    CubemapToolPNGExtract pngWindow = EditorWindow.GetWindow(typeof(CubemapToolPNGExtract), false, "PNG Extract", true) as CubemapToolPNGExtract;
                    pngWindow.outputPath = (EditorPrefs.HasKey(regPrefix + "PNGToolOutputPath"))
                        ? EditorPrefs.GetString(regPrefix + "PNGToolOutputPath") : cm_pathCubemapPNG;
                }

                GUILayout.Space(5);

                if (GUILayout.Button("Property Applier for existing Cubemaps\n(Experimental)", GUILayout.Height(35)))
                {
                    CubemapToolPropertyModifier propertyWindow = EditorWindow.GetWindow(typeof(CubemapToolPropertyModifier), false, "Property Applier", true) as CubemapToolPropertyModifier;
                    propertyWindow.minSize = new Vector2(341, 400);
                }

                /*
                GUILayout.Space(5);

                if (GUILayout.Button("Cubemap Users Overview\n(Experimental)", GUILayout.Height(35)))
                {
                    EditorWindow.GetWindow(typeof(CubemapToolUserOverview), false, "C-User List", true);
                }
                */
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndVertical();
        }

        void Menu_Settings()
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(horizontalWidth));
            {
                CMEditor.Headline("Settings", "Here you find some general settings to improve your workflow or change how the tool works.");

                // Version Display + Check for Update
                EditorGUILayout.LabelField("Current Version:", CMVersion.currentVersion, EditorStyles.boldLabel);
                EditorGUILayout.LabelField("Latest Version:", CMVersion.latestVersion, EditorStyles.boldLabel);

                if (!string.IsNullOrEmpty(CMVersion.checkResult))
                    EditorGUILayout.LabelField("Result:", CMVersion.checkResult, EditorStyles.boldLabel);

                GUI.backgroundColor = CMEditor.ColorGreen;
                if (CMVersion.isOutdated)
                {
                    if (GUILayout.Button("Download Update", GUILayout.Width(150), GUILayout.Height(25)))
                    {
                        Application.OpenURL(CMVersion.assetURL);
                    }
                }
                else
                {
                    if (GUILayout.Button("Check for Updates", GUILayout.Width(150), GUILayout.Height(25)))
                    {
                        CMVersion.CheckForUpdates();
                    }
                }
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndVertical();

            CMEditor.Separator();

            //==== Node Settings
            CMEditor.SmallHeadline("Node Settings", "Affects placement and presentation of new Nodes");

            previewNodes = EditorGUILayout.Toggle("3D Preview Nodes", previewNodes);

            centerNodesOnScreen = EditorGUILayout.Toggle("Center to Viewport", centerNodesOnScreen);
            if (centerNodesOnScreen)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical(GUILayout.Width(50));
                nodeDistanceToCam = EditorGUILayout.FloatField("Node Distance to Cam", nodeDistanceToCam);
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical();
                GUILayout.Label("Units");
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
            }

            CMEditor.Separator();

            //==== Cubemap Output Folders
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(horizontalWidth));
            {
                CMEditor.SmallHeadline("Output Folders", "Defines where assets generated by Cubemapper are saved at. Needs to be a folder inside your projects \"Assets\" folder.\n\nNOTE: The path is saved to the registry and used globally. If you have multiple projects then please make sure they all use the same folder structure in order to avoid errors!");
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(GUILayout.Width(200));
            GUILayout.Label("Cubemap Output Folder", EditorStyles.boldLabel);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUILayout.MinWidth(200), GUILayout.MaxWidth(200));
            GUILayout.Label(CMFile.VerifyPath(cm_pathCubemaps), EditorStyles.boldLabel);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(65));
            {
                GUI.backgroundColor = CMEditor.ColorBlue;
                if (GUILayout.Button("Choose..", GUILayout.MaxWidth(65)))
                {
                    string newCubemapPath = EditorUtility.OpenFolderPanel("Specify Output Folder for Cubemaps", "", "");
                    string sanitizedCubemapPath = CMFile.MakeUnityPath(newCubemapPath, true);

                    // Path seems ok, proceed assigning to variable
                    if (!string.IsNullOrEmpty(sanitizedCubemapPath))
                    {
                        cm_pathCubemaps = sanitizedCubemapPath;
                        //Debug.Log("Sanitized Path: " + sanitizedCubemapPath + " | Raw Path: " + newCubemapPath);
                    }
                    // Something went wrong, show error
                    else
                        EditorUtility.DisplayDialog("Path NOT changed", "Path was not changed because selection of the new Output folder was either aborted or invalid. Please try again or select a different folder.", "Okay");
                }
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            GUILayout.Label(cm_pathCubemaps);
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();


            EditorGUILayout.Space();


            //==== PNG Output Folder		
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(GUILayout.Width(200));
            GUILayout.Label("PNG Output Folder", EditorStyles.boldLabel);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUILayout.MinWidth(200), GUILayout.MaxWidth(200));
            GUILayout.Label(CMFile.VerifyPath(cm_pathCubemapPNG), EditorStyles.boldLabel);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(65));
            {
                GUI.backgroundColor = CMEditor.ColorBlue;
                if (GUILayout.Button("Choose..", GUILayout.MaxWidth(65)))
                {
                    string newCubemapPNGPath = EditorUtility.OpenFolderPanel("Specify Output Folder for PNG Files", "", "");
                    string sanitizedCubemapPNGPath = CMFile.MakeUnityPath(newCubemapPNGPath, false);

                    // Path seems ok, proceed assigning to variable
                    if (!string.IsNullOrEmpty(sanitizedCubemapPNGPath))
                    {
                        cm_pathCubemapPNG = sanitizedCubemapPNGPath;
                        //Debug.Log("Sanitized Path: " + sanitizedCubemapPNGPath + " | Raw Path: " + newCubemapPNGPath);
                    }
                    // Something went wrong, show error
                    else
                        EditorUtility.DisplayDialog("Path NOT changed", "Path was not changed because selection of the new Output folder was either aborted or invalid. Please try again or select a different folder.", "Okay");
                }
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            GUILayout.Label("Assets/" + cm_pathCubemapPNG);
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region Cubemapper Features
        // Set's manager up for generation and goes to playmode
        public void InitGeneration()
        {
            DestroyManager(); // Destroy old instances of the manager
            manager = NewManager(); // Create a new manager

            ///////////////////////////////////////////////////////////////////////////////////////////
            // Cubemapper Settings - Passed on to the Manager hidden in the scene
            ///////////////////////////////////////////////////////////////////////////////////////////
            manager.nodes = nodes;
            manager.pathCubemaps = cm_pathCubemaps;
            manager.pathCubemapPNG = cm_pathCubemapPNG;
            manager.makePNG = cm_makePNG;
            manager.useLinearSpace = cm_useLinearSpace;
            manager.useMipMaps = cm_useMipMaps;
            manager.resolution = cm_resolution;
            manager.cullingMask = cm_cullingMask;
            manager.farClipPlane = cm_farClipPlane;
            manager.camClearFlags = cm_camClearFlags;
            manager.camBGColor = cm_camBGColor;
            manager.mipMapBias = cm_mipmapBias;
            manager.smoothEdges = cm_smoothEdges;
            manager.smoothEdgesWidth = cm_smoothEdgeWidth;
            manager.textureFormat = (cm_useLinearSpace) ? TextureFormat.ARGB32 : TextureFormat.RGB24;

            // UNITY PRO - Capturing Cubemap without playmode
            if (UnityEditorInternal.InternalEditorUtility.HasPro() && SystemInfo.supportsRenderToCubemap)
            {
                manager.generateMethod = CubemapGenerator.GenerateMethod.Pro;
                manager.Generate_UnityPro();
            }
            // UNITY BASIC - Capture Cubemap Images during playmode
            else
            {
                manager.generateMethod = CubemapGenerator.GenerateMethod.Basic;
                manager.generate = true;

                // Start game and inform this window about the generation
                generationInProgress = true;
                EditorApplication.isPlaying = true;
            }
        }

        // Track Progress
        void Update()
        {
            // Check Cubemap Manager status once playing so we know when to go out of playmode
            if (EditorApplication.isPlaying && !EditorApplication.isPaused && generationInProgress)
            {
                manager = FindObjectOfType(typeof(CubemapGenerator)) as CubemapGenerator;

                if (manager != null && manager.completedTakingScreenshots)
                {
                    EditorApplication.isPlaying = false;

                    if (EditorUtility.DisplayDialog("Cubemaps generated!",
                        "You can now proceed assigning your cubemaps to your objects.",
                        "Yay!"))
                    {
                        generationInProgress = false;
                        EditorApplication.isPlaying = false;
                    }
                }
            }
        }

        void CreateNode()
        {
            // Find Cubemap Container, create one if there is none
            CubemapNodeContainer container = FindObjectOfType(typeof(CubemapNodeContainer)) as CubemapNodeContainer;
            if (container == null || !container)
            {
                GameObject newContainer = new GameObject("Cubemap Nodes");
                newContainer.AddComponent<CubemapNodeContainer>();
                container = newContainer.GetComponent<CubemapNodeContainer>();
            }

            // Figure out the numbering the Node should have if we already have nodes
            // Otherwise we might get in a situation where we have Node 1,2,4 (because we deleted 3), want to add another, and now have 1,2,4,4 and produce errors
            int desiredNumber = 0;
            int expectedNumber = 0;

            if (CMGenerate.CountNodes > 0)
            {
                CubemapNode[] existingNodes = CMGenerate.GetAllNodes();
                List<int> existingNumbers = new List<int>();
                foreach (CubemapNode node in existingNodes)
                {
                    var numberStrMatch = System.Text.RegularExpressions.Regex.Match(node.name, @"\d+");

                    if (numberStrMatch.Success)
                    {
                        existingNumbers.Add(System.Int32.Parse(numberStrMatch.Value));
                    }
                }

                existingNumbers = existingNumbers.OrderBy(item => item).ToList();

                for (int i = 0; i < existingNumbers.Count; i++)
                {
                    expectedNumber++;

                    if (existingNumbers[i] != expectedNumber)
                    {
                        Debug.Log("Mismatch found: " + expectedNumber);
                        desiredNumber = expectedNumber;
                        break;
                    }
                }

                if (desiredNumber == 0)
                    desiredNumber = CMGenerate.CountNodes + 1;
            }
            else
                desiredNumber = 1;

            // Create Node, assign component and parent to container
            GameObject go = new GameObject("Cubemap Node " + desiredNumber.ToString());
            go.AddComponent<CubemapNode>();
            go.transform.parent = container.transform;

            // Position in center of screen if desired by User
            if (centerNodesOnScreen)
            {
                if (SceneView.currentDrawingSceneView.camera != null)
                {
                    Vector3 centerScreen = SceneView.currentDrawingSceneView.camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, nodeDistanceToCam));
                    go.transform.position = centerScreen;
                }
            }

            // Select our new Node
            Selection.activeObject = go;

            // Undo
            Undo.RegisterCreatedObjectUndo(go, "Created Cubemap Node");

            Debug.Log("CUBEMAP NODE CREATED!");
        }


        void MakeTransformsToUser()
        {
            // Undo post-4.3 (it also assigns the component)
            foreach (Transform t in Selection.transforms)
            {
                if (t.GetComponent<CubemapUser>() == null)
                    Undo.AddComponent<CubemapUser>(t.gameObject);
            }
        }

        CubemapGenerator NewManager()
        {
            GameObject go = new GameObject("Cubemap Manager (temporary for Generation)", typeof(CubemapGenerator));
            return go.GetComponent<CubemapGenerator>();
        }

        /// <summary>
        /// Destroys all manager instances found in scene upon being called
        /// </summary>
        void DestroyManager()
        {
            CubemapGenerator[] managers = FindObjectsOfType(typeof(CubemapGenerator)) as CubemapGenerator[];

            foreach (CubemapGenerator obj in managers)
            {
                DestroyImmediate(obj.gameObject);
            }
        }
        #endregion



        #region Integrity Checks and Helpers
        void LoadImages()
        {
            // Path to current scripts location
            MonoScript script = MonoScript.FromScriptableObject(this);
            string assetPath = AssetDatabase.GetAssetPath(script);

            // Get Root path		
            DirectoryInfo dirInfo = new DirectoryInfo(assetPath);
            dirInfo = dirInfo.Parent.Parent; // 2 folders up from here

            // Split the path and reassemble into something Unity can read
            string searchPath = CMFile.MakeUnityPath(dirInfo.FullName, true);
            searchPath += "/Images/"; // Append our Image path (no need for useless searches in the other directories)

            string[] files = Directory.GetFiles(searchPath, "*.png", SearchOption.AllDirectories);

            if (files != null)
            {
                foreach (string file in files)
                {
                    Object obj = AssetDatabase.LoadAssetAtPath(file, typeof(Object)) as Object;
                    Texture asset = obj as Texture;

                    if (asset != null)
                        windowImages.Add(asset);
                }
            }
        }

        Texture GetImage(string imageName)
        {
            Texture image = null;

            foreach (Texture t in windowImages)
            {
                if (Path.GetFileNameWithoutExtension(t.name) == imageName)
                    image = t;
            }

            return (image != null) ? image : null;
        }
        #endregion

        #region Manage Editor Prefs
        /// <summary>
        /// Saves the Cubemapper Editor Preferences.
        /// </summary>
        void SavePrefs()
        {
            EditorPrefs.SetString(regPrefix + "CurrentMenu", currentMenu);
            EditorPrefs.SetFloat(regPrefix + "FarClipPlane", cm_farClipPlane);
            EditorPrefs.SetInt(regPrefix + "CullingMask", cm_cullingMask);
            EditorPrefs.SetInt(regPrefix + "CamClearFlag", m_camClearFlagSelected);
            EditorPrefs.SetFloat(regPrefix + "CamBGColor_R", cm_camBGColor.r);
            EditorPrefs.SetFloat(regPrefix + "CamBGColor_G", cm_camBGColor.g);
            EditorPrefs.SetFloat(regPrefix + "CamBGColor_B", cm_camBGColor.b);
            EditorPrefs.SetFloat(regPrefix + "CamBGColor_A", cm_camBGColor.a);
            EditorPrefs.SetBool(regPrefix + "MakePNG", cm_makePNG);
            EditorPrefs.SetBool(regPrefix + "SmoothEdges", cm_smoothEdges);
            EditorPrefs.SetInt(regPrefix + "SmoothEdgeWidth", cm_smoothEdgeWidth);
            EditorPrefs.SetFloat(regPrefix + "MipMapBias", cm_mipmapBias);
            EditorPrefs.SetInt(regPrefix + "Resolution", cm_resolution);
            EditorPrefs.SetBool(regPrefix + "UseMipMaps", cm_useMipMaps);
            EditorPrefs.SetBool(regPrefix + "UseLinearSpace", cm_useLinearSpace);
            EditorPrefs.SetString(regPrefix + "OutputPathCubemaps", cm_pathCubemaps);
            EditorPrefs.SetString(regPrefix + "OutputPathPNG", cm_pathCubemapPNG);
            EditorPrefs.SetString(regPrefix + "CurrentMenu", currentMenu);
            EditorPrefs.SetBool(regPrefix + "CenterNewNodes", centerNodesOnScreen);
            EditorPrefs.SetBool(regPrefix + "PreviewNodes", previewNodes);
            EditorPrefs.SetFloat(regPrefix + "NodeDistanceToCam", nodeDistanceToCam);
            EditorPrefs.SetBool(regPrefix + "ShowAdvancedGenerateSettings", showAdvancedSettings);
        }

        /// <summary>
        /// Loads the Cubemapper Editor Preferences
        /// </summary>
        void LoadPrefs()
        {
            if (EditorPrefs.HasKey(regPrefix + "CurrentMenu"))
                currentMenu = EditorPrefs.GetString(regPrefix + "CurrentMenu");

            if (EditorPrefs.HasKey(regPrefix + "FarClipPlane"))
                cm_farClipPlane = EditorPrefs.GetFloat(regPrefix + "FarClipPlane");

            if (EditorPrefs.HasKey(regPrefix + "CullingMask"))
                cm_cullingMask = EditorPrefs.GetInt(regPrefix + "CullingMask");

            if (EditorPrefs.HasKey(regPrefix + "CamClearFlag"))
                m_camClearFlagSelected = EditorPrefs.GetInt(regPrefix + "CamClearFlag");

            if (EditorPrefs.HasKey(regPrefix + "CamBGColor_R") && EditorPrefs.HasKey(regPrefix + "CamBGColor_G") && EditorPrefs.HasKey(regPrefix + "CamBGColor_B") && EditorPrefs.HasKey(regPrefix + "CamBGColor_A"))
                cm_camBGColor = new Color(EditorPrefs.GetFloat(regPrefix + "CamBGColor_R"), EditorPrefs.GetFloat(regPrefix + "CamBGColor_G"), EditorPrefs.GetFloat(regPrefix + "CamBGColor_B"), EditorPrefs.GetFloat(regPrefix + "CamBGColor_A"));

            if (EditorPrefs.HasKey(regPrefix + "MakePNG"))
                cm_makePNG = EditorPrefs.GetBool(regPrefix + "MakePNG");

            if (EditorPrefs.HasKey(regPrefix + "SmoothEdges"))
                cm_smoothEdges = EditorPrefs.GetBool(regPrefix + "SmoothEdges");

            if (EditorPrefs.HasKey(regPrefix + "SmoothEdgeWidth"))
                cm_smoothEdgeWidth = EditorPrefs.GetInt(regPrefix + "SmoothEdgeWidth");

            if (EditorPrefs.HasKey(regPrefix + "MipMapBias"))
                cm_mipmapBias = EditorPrefs.GetFloat(regPrefix + "MipMapBias");

            if (EditorPrefs.HasKey(regPrefix + "Resolution"))
                cm_resolution = EditorPrefs.GetInt(regPrefix + "Resolution");

            if (EditorPrefs.HasKey(regPrefix + "UseMipMaps"))
                cm_useMipMaps = EditorPrefs.GetBool(regPrefix + "UseMipMaps");

            if (EditorPrefs.HasKey(regPrefix + "UseLinearSpace"))
                cm_useLinearSpace = EditorPrefs.GetBool(regPrefix + "UseLinearSpace");

            if (EditorPrefs.HasKey(regPrefix + "OutputPathCubemaps"))
                cm_pathCubemaps = EditorPrefs.GetString(regPrefix + "OutputPathCubemaps");

            if (EditorPrefs.HasKey(regPrefix + "OutputPathPNG"))
                cm_pathCubemapPNG = EditorPrefs.GetString(regPrefix + "OutputPathPNG");

            if (EditorPrefs.HasKey(regPrefix + "CurrentMenu"))
                currentMenu = EditorPrefs.GetString(regPrefix + "CurrentMenu");

            if (EditorPrefs.HasKey(regPrefix + "CenterNewNodes"))
                centerNodesOnScreen = EditorPrefs.GetBool(regPrefix + "CenterNewNodes");

            if (EditorPrefs.HasKey(regPrefix + "PreviewNodes"))
                previewNodes = EditorPrefs.GetBool(regPrefix + "PreviewNodes");

            if (EditorPrefs.HasKey(regPrefix + "NodeDistanceToCam"))
                nodeDistanceToCam = EditorPrefs.GetFloat(regPrefix + "NodeDistanceToCam");

            if (EditorPrefs.HasKey(regPrefix + "ShowAdvancedGenerateSettings"))
                showAdvancedSettings = EditorPrefs.GetBool(regPrefix + "ShowAdvancedGenerateSettings");
        }
        #endregion
    }
}
/*------------------------------------------------
// CUBEMAPPER
// Version: See Readme
// Created by: Rainer Liessem
// Website: http://www.spreadcamp.com
//
// PLEASE RESPECT THE LICENSE TERMS THAT YOU
// AGREED UPON WITH YOUR PURCHASE OF THIS ASSET
------------------------------------------------*/

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Cubemapper
{
    [ExecuteInEditMode]
    public class CubemapNode : MonoBehaviour
    {
        public bool allowGeneratePNG = true; // allows PNG generation on this node
        public bool allowCubemapGeneration = true; // allows Cubemap generation on this node
        public bool allowAssign = true; // will update this node on one-click building
        public bool overrideResolution = false; // can override resolution picked on cubemap manager
        public int resolution = 32; // the resolution for this node
        public float blendRadius = 10f; // blend radius for more reliable blending
        public bool livePreview = false; // PRO ONLY: Toggle to enable a approx. live preview
        public Cubemap cubemap; // stores an corresponding cubemap in this variable for access by other scripts

        [HideInInspector]
        public GameObject previewSphere;
        private bool allowPreview;
        private Cubemap previewCubemap;
        private Material previewMaterial;
        private Camera previewCamera;

        // At Runtime, we don't want the player to see our preview sphere

        void Start()
        {
            if (previewSphere != null && previewSphere.GetComponent<MeshRenderer>())
            {
                previewSphere.GetComponent<MeshRenderer>().enabled = false;
            }
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "cNode.png");
            NodeDebugDisplay();
        }

        void OnDrawGizmosSelected()
        {
            //NodeDebugDisplay();
        }

        void NodeDebugDisplay()
        {
            if (blendRadius <= 0) return;

            Color blue = Color.blue;
            blue.a = 0.5f;
            Gizmos.color = blue;

            Gizmos.DrawWireSphere(transform.position, blendRadius);
        }

        #region 3D-Node Preview (Editor only!)
        void OnEnable()
        {
            // Remove old Preview Sphere (because, bugs! specifically the null condition further below might be triggered making bad duplicates)
            foreach (Transform t in transform)
            {
                if (t.name == name + " Preview Sphere")
                    DestroyImmediate(t.gameObject);
            }

            // Make a new Preview Sphere
            if (previewSphere == null)
            {
                previewSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                previewSphere.name = name + " Preview Sphere";
                DestroyImmediate(previewSphere.GetComponent<SphereCollider>(), false);
            }
            previewSphere.transform.position = transform.position;
            previewSphere.transform.parent = transform;
            previewSphere.hideFlags = HideFlags.HideInHierarchy;

            // Create the preview material + shader
            Material tmpMaterial = Resources.Load("CubemapperNodePreviewMaterial", typeof(Material)) as Material;
            previewMaterial = new Material(tmpMaterial);
            previewMaterial.hideFlags = HideFlags.HideAndDontSave;

            MeshRenderer targetRenderer = previewSphere.GetComponent<MeshRenderer>();
            targetRenderer.enabled = true;
            targetRenderer.castShadows = false;
            targetRenderer.receiveShadows = false;
            targetRenderer.material = previewMaterial;
        }

        void OnDisable()
        {
            // Clean up materials to prevent leaking
            DestroyImmediate(previewMaterial, true);
        }

        void Update()
        {
            // Don't do preview during Playmode
            if (EditorApplication.isPlaying)
            {
                if (previewSphere.GetComponent<MeshRenderer>().enabled)
                    previewSphere.GetComponent<MeshRenderer>().enabled = false;
            }
            // During Editmode
            else
            {
                allowPreview = (EditorPrefs.HasKey("cubemapper_PreviewNodes")) ? EditorPrefs.GetBool("cubemapper_PreviewNodes") : true;
                if (allowPreview)
                {
                    // Re-enable preview if it was previously disabled
                    if (!previewSphere.GetComponent<MeshRenderer>().enabled)
                        previewSphere.GetComponent<MeshRenderer>().enabled = true;

                    // Keep updating the preview to the ASSIGNED cubemap
                    if (!livePreview)
                        AssignedPreview();
                    // Unity Pro Live Preview - Without some of the advanced effects in Cubemapper like edge smoothing or even camera culling
                    else
                        ProLivePreview();
                }
                else
                {
                    if (previewSphere.GetComponent<MeshRenderer>().enabled)
                        previewSphere.GetComponent<MeshRenderer>().enabled = false;
                }

                // Lock to scale of 1,1,1 (because why not?)
                if (previewSphere.transform.localScale != Vector3.one)
                    previewSphere.transform.localScale = Vector3.one;
            }
        }

        void AssignedPreview()
        {
            // Conditions help to prevent making the user have to save the scene all the time (and save performance too)
            if (cubemap != null)
            {
                previewMaterial.SetTexture("_Cube", cubemap);
            }
            else
            {
                if (previewMaterial.GetTexture("_Cube") != null)
                    previewMaterial.SetTexture("_Cube", null);
            }
        }

        void ProLivePreview()
        {
            TextureFormat targetTextureFormat = (PlayerSettings.colorSpace == ColorSpace.Linear) ? TextureFormat.ARGB32 : TextureFormat.RGB24;

            // Create our Cubemap File that we will render into
            if (previewCubemap == null)
            {
                previewCubemap = new Cubemap(128, targetTextureFormat, true);
                previewCubemap.hideFlags = HideFlags.DontSave;
            }

            // Create and position temporary camera for rendering
            if (previewCamera == null)
            {
                var go = new GameObject("Cubemapper Node Preview Camera (Temporary)", typeof(Camera));
                go.hideFlags = HideFlags.HideAndDontSave;
                previewCamera = go.GetComponent<Camera>();
            }

            // Render into cubemap with our preview camera
            previewCamera.transform.position = transform.position;
            previewCamera.transform.rotation = Quaternion.identity;
            previewCamera.enabled = true;
            previewCamera.hdr = (PlayerSettings.colorSpace == ColorSpace.Linear) ? true : false;
            previewCamera.RenderToCubemap(previewCubemap);
            previewCamera.enabled = false;

            // Finally, assign to our material for display
            previewMaterial.SetTexture("_Cube", previewCubemap);
        }
        #endregion
#endif
    }
}
﻿WORK RESULTS
According to the results of experience, make a report on the laboratory work, in which:
1) Specify the color of the solution in the initial state of equilibrium;
2) Fill in the table 1 - select the color intensity of the observed changes in test tubes 1-3;
3) Record:
    * Equation studied reversible reaction;
    * Formulas and names of substances that are in equilibrium;
    * Mathematical expression of chemical equilibrium constants;
4) Fill in the table 2 - according to the increase or decrease in the intensity of the color of the solution concentration change, specify the substances and the direction of displacement of chemical equilibrium;
5) Make conclusions about the impact on the state of chemical equilibrium:
    * Increasing the concentration of the starting materials;
    * Increasing concentrations of the reaction products.
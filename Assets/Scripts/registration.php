<?
// CONNECTIONS =========================================================
$host = "mysql.freehostingnoads.net"; //хост БД
$username = "u796047560_stud"; //логин БД
$password = "111111"; //пароль к БД
$dbname = "u796047560_unity"; //имя БД

$user = $_POST['username'];
$pass = md5($_POST['password']);
$confirmPass = md5($_POST['passwordConfirm']);
$email = $_POST['email'];
$lastName = $_POST['lastName'];
$firstName = $_POST['firstName'];
$middleName = $_POST['middleName'];
$groupTitle = $_POST['groupTitle'];

$errorMessage = null;
try {
	$dbConnection = new PDO('mysql:dbname='.$dbname.';host='.$host.';charset=utf8', $username, $password);
	$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
	die('Connection problem');
}

if (strlen($user) == 0)
{
	$errorMessage = $errorMessage."\r\nПожалуйста, введите логин. Логин должен быть длиной от 4 до 20 символов.";
}
else if (!ctype_alnum($user)) {
	$errorMessage = $errorMessage."\r\nВы ввели некорректный логин. Используйте только английские буквы и цифры.";
}
else if(strlen($user) < 4 || strlen($user) > 20){	
	$errorMessage = $errorMessage."\r\nЛогин должен быть длиной от 4 до 20 символов. Вы ввели " .strlen($user). " символов.";
}
else {	
	try{	
		$stmt = $dbConnection->prepare('SELECT * FROM Students WHERE login = :login');
	}
	catch (PDOException $e) {
		die('Не могу подготовить SQL Query. Проверьте, что таблица Students существует.');
	}
	$stmt->execute(array(':login' => $user));
	$count = $stmt->rowCount();
	if ($count != 0 || $count != null){
		die("Этот пользователь уже зарегистрирован!");
	}
}
if (strlen($_POST['password']) == 0) 
{
	$errorMessage = $errorMessage."\r\nПожалуйста, введите пароль. Пароль должен быть длиной от 5 до 20 символов.";
}
else if (strlen($_POST['password']) < 5 || strlen($_POST['password']) > 20) 
{
	$errorMessage = $errorMessage."\r\nПарольдолжен быть длиной от 5 до 20 символов. Вы ввели " .strlen($_POST['password']). " символов.";
}
if (strlen($_POST['passwordConfirm']) == 0)
{
	$errorMessage = $errorMessage."\r\nПожалуйста, подтвердите пароль";
}
else if ($pass != $confirmPass)
{
		$errorMessage = $errorMessage."\r\nПароли не совпадают!";
}
if (strlen($email) == 0)
{
	$errorMessage = $errorMessage."\r\nПожалуйста, введите e-mail.";
}
else if (strlen($email) > 50)
{	
	$errorMessage = $errorMessage."\r\nEmail должен быть длиной до 50 символов. Вы ввели " .strlen($email). " символов.";
}
else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$errorMessage = $errorMessage."\r\nПожалуйста, введите корректный e-mail. Например: example@gmail.com";
}
if (strlen($lastName) == 0)
{
	$errorMessage = $errorMessage."\r\nПожалуйста, введите свою фамилию.";
}
else if (strlen($lastName) < 2 || strlen($lastName) > 25)
{
	$errorMessage = $errorMessage."\r\nФамилия должна быть длиной от 2 до 25 символов. Вы ввели " .strlen($lastName). " символов.";
}
if (strlen($firstName) == 0)
{
	$errorMessage = $errorMessage."\r\nПожалуйста, введите свое имя.";
}
else if (strlen($firstName) < 2 || strlen($firstName) > 25)
{
	$errorMessage = $errorMessage."\r\nИмя должно быть длиной от 2 до 25 символов. Вы ввели " .strlen($firstName). " символов.";
}
if (strlen($middleName) > 25)
{
	$errorMessage = $errorMessage."\r\nОтчество должно быть длиной до 25 символов. Вы ввели " .strlen($middleName). " символов.";
}
if (strlen($groupTitle) == 0)
{
	$errorMessage = $errorMessage."\r\nНе выбрана ни одна группа!";
}
if ($errorMessage!=null)
{
	die($errorMessage);
}
try{	
	$stmt = $dbConnection->prepare('SELECT id FROM Groups WHERE title = :groupTitle');
}
catch (PDOException $e) {
	die('Не могу подготовить SQL Query. Проверьте, что таблица Groups существует.');
}	
$stmt->execute(array(':groupTitle' => $groupTitle));
$count = $stmt->rowCount();
if ($count == 0 || $count == null){
	echo "Не найдено такой группы";
}else {
	$groupId = $stmt -> fetchColumn();
}

try{	
	$stmt = $dbConnection->prepare('INSERT INTO Students (lastName, firstName, middleName, login, password, email, id_group) VALUES (:lastName, :firstName, :middleName, :login, :password, :email, :groupId)');
}
catch (PDOException $e) {
	die('Не могу подготовить SQL Query. Проверьте, что таблица Students существует.');
}
$stmt->execute(array(':lastName' => $lastName, ':firstName' => $firstName, ':middleName' => $middleName, ':login' => $user, ':password' => $pass, ':email' => $email, ':groupId' => $groupId));
$count = $stmt->rowCount();
if ($count == 0 || $count == null){
	echo "Ошибка при попытке вставить строку в таблицу Students.";
}else {
	echo "Success";
}
// Close mySQL Connection
$dbConnection = null; 

?>
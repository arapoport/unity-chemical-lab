﻿using System.Collections;
using UnityEngine;

public class Microspatula : SourceParent
{

	// Use this for initialization
	void Start () {
        mover = GameObject.Find("mover").GetComponent<Move>();
        controller = GameObject.Find("controller").GetComponent<Controller>();
        microspatula = gameObject.transform.FindChild("microspatula").gameObject;
        //наклон по умолчанию
        defaultRotate = new Vector3(microspatula.transform.rotation.eulerAngles.x,
                                microspatula.transform.rotation.eulerAngles.y,
                                microspatula.transform.rotation.eulerAngles.z);

        defaultPosition = microspatula.transform.position; //стартовая позиция

		substance = GetSubstance();
        
        reactions = GameObject.Find("reactions").GetComponent<Reactions>();
		check = GameObject.Find ("ready").GetComponent<CheckLabs> ();
	}

    Move mover;
    Controller controller;
    Reactions reactions;
	CheckLabs check;

    Vector3 defaultPosition;
    Vector3 defaultRotate;
    GameObject microspatula;

    public float Upup=0.05f;
    public float OffsetZ=0.01f;

    GameObject substance;

    void OnMouseUp()
    {
        controller.AddMyGameObject(this);       
    }

    public override void SetNewDefaultPosition(Vector3 newPosition)
    {
        defaultPosition = newPosition;
    }

    public override IEnumerator Select()
    {
        mover.MoveRelative(microspatula,0,Upup,0, 0.4f); //время
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
        mover.DoMove(microspatula, defaultPosition, 0.4f);
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator StartAnimation(Destination destination)
    {
		var destinationSubstance = destination.GetSubstance();

        var v3 = new Vector3(destination.transform.position.x,
                destination.transform.position.y + Upup,
                destination.transform.position.z-OffsetZ);

        mover.DoMove(microspatula, v3, 0.7f); //по горизонтали к назначению

        yield return mover.WaitForCompletion();

        var rotate = new Vector3(microspatula.transform.rotation.eulerAngles.x,
                                microspatula.transform.rotation.eulerAngles.y,
                                0);

        mover.Rotate(microspatula, rotate, 0.4f); // повернуть для того чтоб высыпать в пробирку
        yield return mover.WaitForCompletion();

        //Насыпали----------------------------------------------------------------------- 
		destination.SetDestinationSubstanceMaterialAndContent(name,check,reactions);
		StartCoroutine(mover.Pour(substance, destinationSubstance, Pour, 0.1f));
        yield return mover.WaitForCompletion();
        //-------------------------------------------------------------------------------

        mover.Rotate(microspatula, defaultRotate, 0.4f); //повернуть в исходное положение
        yield return mover.WaitForCompletion();

        v3 = new Vector3(defaultPosition.x, defaultPosition.y + Upup, defaultPosition.z); //по горизонтали к стартовому месту

        mover.DoMove(microspatula, v3, 0.7f);//по горизонтали к стартовому месту
        yield return mover.WaitForCompletion();

        mover.DoMove(microspatula, defaultPosition, 0.4f); //в исходную позицию
        yield return mover.WaitForCompletion();

        controller.Finish();

    }

}

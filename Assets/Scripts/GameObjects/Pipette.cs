﻿using System.Collections;
using UnityEngine;

public class Pipette : SourceParent {

	// Use this for initialization
	void Start () {
	    mover = FindObjectOfType<Move>();
	    controller = FindObjectOfType<Controller>();
        pipette = gameObject.transform.FindChild("pipette").gameObject;
        defaultPosition = pipette.transform.position;
		substance = GetSubstance();
        try
        {
            menu = gameObject.transform.FindChild("menu").gameObject.GetComponent<Menu>();
        }
        catch
        {
            menu = null;
        }
		drop = pipette.transform.FindChild("substance").gameObject; //капля
        defaultDropPosition = drop.transform.position; //позиция капли
        dropScaleY = drop.transform.localScale.y;

	    reactions = FindObjectOfType<Reactions>();
	    bubbles = FindObjectOfType<Bubbles>();
	    check = FindObjectOfType<CheckLabs>();
	}

    Move mover;
    Controller controller;
	CheckLabs check;
    Menu menu;
    Bubbles bubbles;

    GameObject substance;
    Vector3 defaultPosition;
    Vector3 defaultDropPosition;
    GameObject drop; //капля

    Reactions reactions;

    float dropScaleY;
    GameObject pipette;

    public float Upup = 0.05f;

    void OnMouseUp()
    {
        controller.AddMyGameObject(this);
    }

    public override void SetNewDefaultPosition(Vector3 newPosition)
    {
        defaultPosition = newPosition;
    }

    public override IEnumerator Select()
    {
        mover.MoveRelative(pipette, 0, Upup, 0,  0.4f); //время
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
        mover.DoMove(pipette, defaultPosition, 0.4f);
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public IEnumerator Resume()
    {
        var destinationSubstance = destination.GetSubstance();
        var destTop = destination.GetTop(); //пустой объект в destination который расположен сверху
        var destBot = destination.GetBottom(); //а этот снизу   
        var content = gameObject.name;

        if (menu != null)
        {
            content += "_" + menu.SelectedValue;
        }

        var v3 = new Vector3(destination.transform.position.x,
                destination.transform.position.y + Upup,
                destination.transform.position.z);
        mover.DoMove(pipette, v3, 0.7f); //по горизонтали к назначению
        yield return mover.WaitForCompletion();

        v3 = new Vector3(destination.transform.position.x,
                destTop.transform.position.y,
                destination.transform.position.z);
        mover.DoMove(pipette, v3, 0.4f); // опустить в пробирку
        yield return mover.WaitForCompletion();

        //Падение капли---------------------------------------------------------
        StartCoroutine(mover.Drop(drop, destBot.transform.position, 0.5f)); //падение капли
        yield return mover.WaitForCompletion();
        //------------------------------------------------------------------------

        //Наполнение пробирки-----------------------------------------------------------       
        
        var bubblesMat = reactions.GetBubblesMaterial(destination.Content + "+" + content); //проверяем будут ли пузыри
        var result = "";
        var material = reactions.GetMaterial(destination.Content + "+" + content, out result);
        destination.Content = result;
        if (material.name != "fail" && bubblesMat.name == "fail")
        {
            destinationSubstance.renderer.material = material; //если нет пузырей то новый материал накладываем сразу
        }
        else if (bubblesMat.name != "fail")
        {
            destinationSubstance.renderer.material = bubblesMat;
            bubbles.BubbleWithoutScaling(destinationSubstance);
        }
        if (result == "fail")
        {
            destination.FailCounter++;
            if (check.FailCount() > 4)
            {
                check.Check();
            }
        }

		StartCoroutine(mover.Pour(substance, destinationSubstance, Pour, 0.5f)); //наполнение пробирки
        yield return mover.WaitForCompletion();

        if (bubblesMat.name != "fail")
        {
            StartCoroutine(bubbles.Bubble(0.5f));
            yield return new WaitForSeconds(1);
            destinationSubstance.renderer.material = material;
        }

        //-------------------------------------------------------------------------------

        v3 = new Vector3(destination.transform.position.x,
                destination.transform.position.y + Upup,
                destination.transform.position.z);
        mover.DoMove(pipette, v3, 0.4f); //поднять пипетку
        yield return mover.WaitForCompletion();

        v3 = new Vector3(defaultPosition.x, defaultPosition.y + Upup, defaultPosition.z); //по горизонтали к стартовому месту
        mover.DoMove(pipette, v3, 0.7f);//по горизонтали к стартовому месту
        yield return mover.WaitForCompletion();

        mover.DoMove(pipette, defaultPosition, 0.4f); //в исходную позицию
        yield return mover.WaitForCompletion();

        //вернуть каплю на место---------------------------------------------------------
        drop.transform.position = defaultDropPosition;
        drop.transform.localScale = new Vector3(drop.transform.localScale.x, dropScaleY, drop.transform.localScale.z);
        drop.renderer.enabled = true;
        //--------------------------------------------------------------------------------

        controller.Finish();
    }

    Destination destination;
    public override IEnumerator StartAnimation(Destination destination)
    {
        this.destination = destination;
        if (menu != null)
        {
            menu.Show(destination.transform.position);
        }
        else
        {
            StartCoroutine(Resume());
        }
        yield return new WaitForSeconds(0);

    }

}

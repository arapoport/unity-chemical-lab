﻿using System.Collections;
using UnityEngine;

public class TestTube : MyGameObject {

	// Use this for initialization
	void Start () {
	    mover = FindObjectOfType<Move>();
	    controller = FindObjectOfType<Controller>();
        destination = GetComponent<Destination>();
        defaultPosition = gameObject.transform.position;
	    testTubes = FindObjectOfType<MainStand>();
	}

    Move mover;
    Controller controller; 
    Destination destination;
    MainStand testTubes;
    Vector3 defaultPosition;
    public float Upup = 0.06f;

	public string GetContent()
	{
		return destination.Content;
	}

    public override void SetNewDefaultPosition(Vector3 position)
    {
        defaultPosition = position;
    }

    public override IEnumerator StartAnimation(Destination destination)
    {
        yield return null;
    }

    public override IEnumerator Select()
    {
        mover.DoMove(gameObject,
           new Vector3(gameObject.transform.position.x,
               gameObject.transform.position.y + Upup,
               gameObject.transform.position.z),
               0.4f); //время

        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
        mover.DoMove(gameObject, defaultPosition, 0.4f);
        yield return new WaitForSeconds(0.4f);
        controller.FinishSelect();
    }

    public IEnumerator Replace(Vector3 position)
    {
        destination.Active = true;
        mover.MoveRelative(gameObject, 0, Upup, 0, 0.5f);
        yield return mover.WaitForCompletion();
        var v3 = new Vector3(position.x, position.y + Upup, position.z);
        mover.DoMove(gameObject, v3, 1);
        yield return mover.WaitForCompletion();
        mover.DoMove(gameObject, position, 0.5f);
        yield return mover.WaitForCompletion();
        enabled = false;
        testTubes.AddTestTube(position);
        controller.Finish();
    }

    void OnMouseUp()
    {
        if (destination.Active)
        {
            controller.AddMyGameObject(this);
        }
        else
        {
            controller.Reserve(this);   
        }
    }
}

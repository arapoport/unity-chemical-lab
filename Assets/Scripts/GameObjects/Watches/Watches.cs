﻿using System.Collections;
using UnityEngine;

public class Watches : TextProcessing {

	// Use this for initialization
	void Start () {
		StartCoroutine("OneTick");
		guiText.fontSize = 30;
		LetTheCountBegin = false; 
	}
	
	// Update is called once per frame
	void Update () {
        guiText.fontSize = GetFontSize()+5;
	}
    public static bool LetTheCountBegin; 
	private static int seconds;
	private static int minutes;
	private static int hours;
	public static float TimeSpeed = 1;

    public static int GetTimeInSeconds()
    {
        return hours * 3600 + minutes * 60 + seconds;
    }

    public static int GetSeconds()
    {
        return seconds;
    }
    public static int GetMinutes()
    {
        return minutes;
    }

	private IEnumerator OneTick() {
		if (hours < 10) {
			guiText.text = "0"+hours+":"+minutes+":"+seconds;
			if (minutes<10) {
				guiText.text = "0"+hours+":"+"0"+minutes+":"+seconds;
				if (seconds<10)
					guiText.text = "0"+hours+":"+"0"+minutes+":"+"0"+seconds;
			}
			else if (seconds<10) {
				guiText.text = "0"+hours+":"+minutes+":"+"0"+seconds;
			}
		}
		else if (minutes<10) {
			guiText.text = hours+":"+"0"+minutes+":"+seconds;
			if (seconds<10)
				guiText.text = hours+":"+"0"+minutes+":"+"0"+seconds;
		}
		else if (seconds<10) {
			guiText.text = hours+":"+minutes+":"+"0"+seconds;
		}
		else {
			guiText.text = hours+":"+minutes+":"+seconds;
		}
		guiText.text += "  (x" + 1/TimeSpeed + ")";
		yield return new WaitForSeconds (TimeSpeed);
		if (seconds == 59) {
			if (minutes == 59) {
				hours++;
				minutes = 0;
				seconds = 0;
			}
			else {
				minutes++;
				seconds = 0;
			}
		}
        if (LetTheCountBegin)
		    seconds++;
		StartCoroutine("OneTick");
	}
}

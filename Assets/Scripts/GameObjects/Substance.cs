﻿using UnityEngine;

/* Класс вещества */
public class Substance : MonoBehaviour {

    private bool residueStatus; //состояние осадка (не путать с состоянием вещества!)
    public State State;
    private string substanceName; //текущее название вещества
    private SubstanceState substanceState;

    public void SetSubstanceName(string name) { substanceName = name; }
    public string GetSubstanceName() { return substanceName; }
    public void SetResidueStatus(bool status) { residueStatus = status; }
    public bool GetResidueStatus() { return residueStatus; }

	// Use this for initialization
	void Start () {
	    substanceState = GameObject.Find("substanceState").GetComponent<SubstanceState>(); 
	}
	
	// Update is called once per frame
	void Update ()
	{
	    substanceState.GetSubstanceStatusByContent(substanceName);
	}
}

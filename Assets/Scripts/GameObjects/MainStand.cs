﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class MainStand : MonoBehaviour
{
	// Use this for initialization
	void Start () {
	    for (var i = 1; i < 10; i++)
	    {
            try
            {
                testTubePosList.Add(GameObject.Find("testTube" + i).transform.position);
                isTestTubeEmptyList.Add(false);
            }
            catch
            {
                break;
            }
	    }
	}

    private List<Vector3> testTubePosList = new List<Vector3>();
    private List<bool> isTestTubeEmptyList = new List<bool>();  

	public int GetTestTubeNumberByPosition(Vector3 position) 
	{
	    for (var i = 0; i < testTubePosList.Count(); i++)
	    {
	        if (testTubePosList[i] == position)
	        {
	            return i+1;
	        }
	    }
	    return -1;
	}

    public Vector3 GetPosTestTube(int number)
    {
        return testTubePosList[--number]!=null ? testTubePosList[--number] : Vector3.zero;
    }

    public bool EmptySlotExist()
    {
        var answer = false;
        for (var i = 0; i < testTubePosList.Count(); i++)
        {
            answer = isTestTubeEmptyList[i] && isTestTubeEmptyList[i];
        }
        return answer;
    }

    public Vector3 GetPosEmptySlot()
    {
        for (var i = 0; i < testTubePosList.Count(); i++)
        {
            if (isTestTubeEmptyList[i])
            {
                return testTubePosList[i];
            }
        }
        return Vector3.zero;
    }

    public void DelTestTube(Vector3 v3)
    {
        for (var i = 0; i < testTubePosList.Count(); i++)
        {
            if (v3.x == testTubePosList[i].x && v3.z == testTubePosList[i].z)
            {
                isTestTubeEmptyList[i] = true;
            }
        }
    }

    public void AddTestTube(Vector3 v3)
    {
        for (var i = 0; i < testTubePosList.Count(); i++)
        {
            if (v3 == testTubePosList[i])
            {
                isTestTubeEmptyList[i] = false;
            }
        }
    }
}

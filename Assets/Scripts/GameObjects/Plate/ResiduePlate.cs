﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class ResiduePlate : MonoBehaviour {
    Controller controller;
	// Use this for initialization
	void Start () {
        controller = GameObject.Find("controller").GetComponent<Controller>();
        testTubes = GameObject.Find("mainStand").GetComponent<MainStand>();
	}

    private const float UpUp = 0.05f;
    private MainStand testTubes;
    private Vector3 v30;
    private Tween myTween0;

    IEnumerator SolidSubstanceMove(TestTube testTube) //в пробирке твердое, невыливающееся вещество. Пробирка летит проверяться к раковине и возвращается обратно в стойку.
    {
        var destinationTestTubeX = testTube.transform.position.x;
        var destinationTestTubeY = testTube.transform.position.y;
        var destinationTestTubeZ = testTube.transform.position.z;
        StartCoroutine("MoveTestTubeToThePlate", testTube);
        yield return myTween0.WaitForCompletion();
        v30 = new Vector3(180, 0, 0);
        myTween0 = testTube.transform.DORotate(v30, 0.5f);
        yield return myTween0.WaitForCompletion();
        yield return new WaitForSeconds(0.8f);
        var v3 = new Vector3(destinationTestTubeX,
                                 destinationTestTubeY,
                                 destinationTestTubeZ);

        Tween myTween = testTube.transform.DOMove(v3, 1);
        yield return myTween.WaitForCompletion();

        v3 = new Vector3(destinationTestTubeX,
                         destinationTestTubeY - testTube.Upup,
                         destinationTestTubeZ);
        myTween = testTube.transform.DOMove(v3, 0.8f);
        testTube.transform.DORotate(new Vector3(0, 0, 0), 0.2f);
        yield return myTween.WaitForCompletion();
        controller.Finish();
    }

    IEnumerator LiquidSubstanceMove(TestTube testTube) // в пробирке жикость. Пробирка летит к раковине и всё её содержимое туда выливается. Потом пробирка летит и падает в коробку.
    {
        var box = GameObject.Find("box");
        var plateSubstance = transform.FindChild("substance").gameObject;
        var testTubeSubstance = testTube.transform.FindChild("substance").gameObject;
        var destination = testTube.GetComponent<Destination>();
        testTubes.DelTestTube(testTube.transform.position);
        StartCoroutine("MoveTestTubeToThePlate", testTube);
        yield return myTween0.WaitForCompletion();
        v30 = new Vector3(180, 0, 0);
        myTween0 = testTube.transform.DORotate(v30, 0.5f);
        yield return myTween0.WaitForCompletion();
        myTween0 = testTubeSubstance.transform.DOMove(transform.position, 1);
        yield return myTween0.WaitForCompletion();
        plateSubstance.renderer.enabled = true;
        myTween0 = testTubeSubstance.transform.DOScaleY(0, 0.2f);
        plateSubstance.renderer.material.mainTexture = testTubeSubstance.renderer.material.mainTexture;
        plateSubstance.transform.DOLocalMoveY(plateSubstance.transform.localPosition.y + 0.01f, 0.2f);
        plateSubstance.transform.DOScaleY(plateSubstance.transform.localScale.y + 0.01f, 0.2f);
        yield return myTween0.WaitForCompletion();
        testTubeSubstance.renderer.enabled = false;
        var v3 = new Vector3(box.transform.position.x,
                                 testTube.transform.position.y,
                                 box.transform.position.z);

        Tween myTween = testTube.transform.DOMove(v3, 0.8f);
        yield return myTween.WaitForCompletion();
        v3 = new Vector3(box.transform.position.x,
                         box.transform.position.y,
                         box.transform.position.z);
        myTween = testTube.transform.DOMove(v3, 0.8f);
        testTube.transform.DORotate(new Vector3(90, 0, 0), 0.2f);
        yield return myTween.WaitForCompletion();
        destination.Active = false;
        controller.Finish();
    }

    IEnumerator AmorphousSubstanceMove(TestTube testTube) // в пробирке аморфное вещество. Пробирка летит к раковине и всё её содержимое туда выливается. Потом пробирка летит и падает в коробку.
    {
        var box = GameObject.Find("box");
        var plateSubstance = transform.FindChild("substance").gameObject;
        var testTubeSubstance = testTube.transform.FindChild("substance").gameObject;
        var destination = testTube.GetComponent<Destination>();
        testTubes.DelTestTube(testTube.transform.position);
        StartCoroutine("MoveTestTubeToThePlate", testTube);
        yield return myTween0.WaitForCompletion();
        v30 = new Vector3(180, 0, 0);
        myTween0 = testTube.transform.DORotate(v30, 0.5f);
        yield return myTween0.WaitForCompletion();
        myTween0 = testTubeSubstance.transform.DOMove(transform.position, 2.5f);
        yield return myTween0.WaitForCompletion();
        plateSubstance.renderer.enabled = true;
        myTween0 = testTubeSubstance.transform.DOScaleY(0, 0.5f);
        plateSubstance.renderer.material.mainTexture = testTubeSubstance.renderer.material.mainTexture;
        plateSubstance.transform.DOLocalMoveY(plateSubstance.transform.localPosition.y + 0.01f, 0.2f);
        plateSubstance.transform.DOScaleY(plateSubstance.transform.localScale.y + 0.01f, 0.2f);
        yield return myTween0.WaitForCompletion();
        testTubeSubstance.renderer.enabled = false;
        var v3 = new Vector3(box.transform.position.x,
                                 testTube.transform.position.y,
                                 box.transform.position.z);

        Tween myTween = testTube.transform.DOMove(v3, 0.8f);
        yield return myTween.WaitForCompletion();
        v3 = new Vector3(box.transform.position.x,
                         box.transform.position.y,
                         box.transform.position.z);
        myTween = testTube.transform.DOMove(v3, 0.8f);
        testTube.transform.DORotate(new Vector3(90, 0, 0), 0.2f);
        yield return myTween.WaitForCompletion();
        destination.Active = false;
        controller.Finish();
    }

    private IEnumerator MoveTestTubeToThePlate(TestTube testTube)
    {
        v30 = new Vector3(transform.position.x,
                                  transform.position.y + transform.lossyScale.y * 2 + UpUp,
                                  transform.position.z);
        myTween0 = testTube.transform.DOMove(v30, 1);
        yield return myTween0.WaitForCompletion();
    }

    void OnMouseEnter()
    {
        MyCursor.SetClick();
    }

    void OnMouseExit()
    {
        MyCursor.SetNormal();
    }

    void OnMouseUp()
    {
        controller.CheckResidue(this);
    }

	// Update is called once per frame
	void Update () {
	
	}
}

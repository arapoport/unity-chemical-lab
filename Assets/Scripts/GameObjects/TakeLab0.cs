﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TakeLab0 : MonoBehaviour {

	// Use this for initialization
	void Start () {
        inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
        startPosition = transform.position;
        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
        child = new List<GameObject>();

        var index = 0;
        var buf = gameObject;

        for (var i = 0; i <= buf.transform.childCount; i++)
        {
			if(i<buf.transform.childCount)
			{
            	child.Add(buf.transform.GetChild(i).gameObject);
			}
            if (i == buf.transform.childCount - 1 || buf.transform.childCount == 0)
            {
                if (index < child.Count)
                {
                    i = -1;
                    buf = child[index];
                    index++;
                }
            }
        }        
	}      
    
    Vector3 startPosition;
    InventoryLab0 inventory;
    public Texture Texture;

    void OnMouseUp()
    {
        StartCoroutine(TakeThis());
    }

    void OnMouseEnter()
    {
        MyCursor.SetTake();
    }

    void OnMouseExit()
    {
        MyCursor.SetNormal();
    }
    List<GameObject> child;
    IEnumerator TakeThis()
    {
        gameObject.collider.enabled = false;
        var player = GameObject.Find("Main Camera");
        var v3 = player.transform.position;
        Tween myTween= transform.DOMove(v3, 0.3f);
        yield return myTween.WaitForCompletion();
        inventory.Add(this);
        renderer.enabled = false;

        foreach (var t in child)
        {
            try
            {
                t.renderer.enabled = false;
            }
            catch {}
        }
    }

    public IEnumerator DropThis()
    {
       renderer.enabled = true;
       var player = GameObject.Find("Main Camera");
       transform.position = player.transform.position;
       Tween myTween = transform.DOMove(startPosition, 0.3f);
       yield return myTween.WaitForCompletion();
       gameObject.collider.enabled = true;
       for (var i = 0; i < child.Count; i++)
       {
           try
           {
               child[i].renderer.enabled = true;
           }
           catch {}
       }    
    }
}

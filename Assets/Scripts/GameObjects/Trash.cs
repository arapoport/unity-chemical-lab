﻿using System.Collections;
using UnityEngine;

public class Trash : MonoBehaviour {

	// Use this for initialization
	void Start () {
        bank = GameObject.Find("bank");
        bankSubstance = bank.transform.FindChild("substance").gameObject;
        boxPosition = GameObject.Find("box").transform.position;
	    controller = FindObjectOfType<Controller>();
	    mover = FindObjectOfType<Move>();
	}

    Controller controller;
    Move mover;
    GameObject bankSubstance;
    Vector3 boxPosition;
    GameObject bank;

    public IEnumerator Remove(TestTube testTube)
    {
        Vector3 v3;
        var destination = testTube.GetComponent<Destination>();

        if (destination.Content != "empty")
        {
            var removeSubstance = destination.transform.FindChild("substance").gameObject;
            v3 = new Vector3(bank.transform.position.x, bank.transform.position.y + testTube.Upup, bank.transform.position.z);
            mover.DoMove(testTube.gameObject, v3, 1);
            yield return mover.WaitForCompletion();
            v3 = new Vector3(-180, testTube.gameObject.transform.eulerAngles.y, testTube.gameObject.transform.eulerAngles.z);
            mover.Rotate(testTube.gameObject, v3, 0.5f);
            yield return mover.WaitForCompletion();
            mover.WaterStart(testTube.gameObject.transform.position, bankSubstance.transform.position, removeSubstance.renderer.material, 0.5f);
            yield return mover.WaitForCompletion();
            bankSubstance.renderer.material = removeSubstance.renderer.material;
			StartCoroutine(mover.Pour(removeSubstance, bankSubstance, 100, 1));
            yield return mover.WaitForCompletion();
            StartCoroutine(mover.WaterEnd(bankSubstance.transform.position, 0.5f));
            yield return mover.WaitForCompletion();
        }

        v3 = new Vector3(boxPosition.x, boxPosition.y + testTube.Upup, boxPosition.z);
        mover.DoMove(testTube.gameObject, v3, 1);
        yield return mover.WaitForCompletion();
        v3 = new Vector3(-90, testTube.gameObject.transform.eulerAngles.y, testTube.gameObject.transform.eulerAngles.z);
        mover.Rotate(testTube.gameObject, v3, 0.5f);
        yield return mover.WaitForCompletion();
        mover.DoMove(testTube.gameObject, boxPosition, 0.6f);
        yield return mover.WaitForCompletion();
        destination.Active = false;
        controller.Finish();
    }

    void OnMouseEnter()
    {
        MyCursor.SetClick();
    }

    void OnMouseExit()
    {
        MyCursor.SetNormal();
    }

    void OnMouseUp()
    {
        controller.AddTrash(this);
    }
}

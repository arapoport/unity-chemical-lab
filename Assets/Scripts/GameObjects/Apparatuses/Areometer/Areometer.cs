﻿using System.Collections;
using UnityEngine;

public class Areometer : Apparatuses {

	// Use this for initialization
	void Start () {
		mover = GameObject.Find("mover").GetComponent<Move>();
        controller = GameObject.Find("controller").GetComponent<Controller>();
		defaultPosition = gameObject.transform.position;
	}

    Controller controller;
	Vector3 defaultPosition;
    public float Upup = 0.05f;
	Move mover;

    void OnMouseUp()
    {
        controller.AddMyGameObject(this);       
    }
    public override IEnumerator Select()
    {
		mover.MoveRelative(gameObject, 0, Upup, 0, 0.4f); //время
        yield return null;
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
		mover.DoMove(gameObject, defaultPosition, 0.4f);
        yield return null;
        controller.FinishSelect();
    }

    public override void SetNewDefaultPosition(Vector3 position) { }

    public override IEnumerator StartAnimation(Destination destination)
    {
        var v32 = new Vector3(destination.transform.position.x,
		                           destination.GetTop().transform.position.y + 0.01f,
		                           destination.transform.position.z);
		mover.DoMove(gameObject, v32, 0.4f); // опустить в пробирку
		yield return mover.WaitForCompletion();
		yield return new WaitForSeconds(1.0f);
        mover.DoMove(gameObject, defaultPosition, 0.4f);
		yield return mover.WaitForCompletion();
        TipsLab6.Tips = GetTheSolutionDensity(destination);
        controller.Finish();
    }
    public string GetTheSolutionDensity(Destination destination)
    {
        TipsLab6.Show = true;
        if (destination.Content == "sodium_carbonate+measuringCylinder250ml")
        {           
            return "Плотность раствора равна 1.15 г/мл";
        }
        if (destination.Content == "sodium_carbonate+measuringCylinder250ml+beaker")
        {
            return "Плотность раствора равна 1.15 г/мл";
        }
        if (destination.Content == "sodium_carbonate+measuringCylinder250ml+beaker+measuringCylinder100ml")
        {
            return "Плотность раствора равна 1.15 г/мл";
        }
        if (destination.Content == "sodium_carbonate+measuringCylinder250ml+beaker+measuringCylinder100ml+measuringCylinder250ml")
        {
            return "Плотность раствора равна 1.15 г/мл";
        }
        if (destination.Content == "sodium_carbonate+measuringCylinder250ml+beaker+measuringCylinder100ml+measuringCylinder250ml+distilled_water")
        {          
            return "Плотность раствора равна 1.08 г/мл";
        }
        return "Плотность раствора неизвестна";
    }

	// Update is called once per frame
	/*void Update () {
	}*/
}
 
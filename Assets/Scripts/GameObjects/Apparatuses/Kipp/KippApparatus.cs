﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class KippApparatus : Apparatuses {

	// Use this for initialization
	void Start () {
        mover = GameObject.Find("mover").GetComponent<Move>();
        testTubes = GameObject.Find("mainStand").GetComponent<MainStand>();
        reactions = GameObject.Find("reactions").GetComponent<Reactions>();
        check = GameObject.Find("ready").GetComponent<CheckLabs>();
        controller = GameObject.Find("controller").GetComponent<Controller>();
        defaultPosition = gameObject.transform.position;
        bubbles = GameObject.Find("bubbles").GetComponent<Bubbles>();
		for (int i = 1; i < 10; i++) {
			try { pipes.Add (GameObject.Find("pipe" + i).GetComponent<Pipe>()); } //шланг киппа
			catch { if (i==1) { throw new Exception("ERROR No Pipes!"); } else { Debug.Log("есть только " + --i + " шланг(-ов)"); } break;}
		}
        try { apparatusTime = GameObject.Find("apparatusTime").GetComponent<ApparatusTime>(); }
        catch { Debug.Log("нет аппаратного времени");}
	}

    List<Pipe> pipes = new List<Pipe>();
    public float Upup = 0.05f;
    Controller controller;
    Reactions reactions;
    MainStand testTubes;
    CheckLabs check;
    Bubbles bubbles;
    ApparatusTime apparatusTime; 
    Destination destination;
	GameObject destinationSubstance;
    Move mover;
    Vector3 defaultPosition;

    void OnMouseUp()
    {
        controller.AddMyGameObject(this);
    }

    public override IEnumerator Select()
    {
        mover.MoveRelative(gameObject, 0, Upup, 0, 0.4f); //время
        yield return null;
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
        mover.DoMove(gameObject, defaultPosition, 0.4f);
        yield return null;
        controller.FinishSelect();
    }

    public override void SetNewDefaultPosition(Vector3 position) {}     
      
    public override IEnumerator StartAnimation(Destination destination)
    {
        mover.DoMove(gameObject, defaultPosition, 0.4f);
        yield return new WaitForSeconds(0.6f);
        this.destination = destination;
		destinationSubstance = destination.GetSubstance();
        ConnectPipe(); 
		destinationSubstance.renderer.material = reactions.GetBubblesMaterial(gameObject.name);
        bubbles.StartBubbles(destinationSubstance);
		if (apparatusTime) 
		{
			apparatusTime.SetTimeWhenPipeConnected(Watches.GetTimeInSeconds());
		}
        yield return null;     
    }
    
    public void RemovePipe(Pipe pipe) //убираем шланг, по которому кликнул пользователь.
    {
		bubbles.StopBubbles();
		destination.SetDestinationSubstanceMaterialAndContent(name, check, reactions);
		if (apparatusTime) 
		{
			apparatusTime.SetTimeWhenPipeDisconnected(Watches.GetTimeInSeconds());
			apparatusTime.AddToDictionary(apparatusTime.ContentTimes, destination.Content, apparatusTime.GetTimeWithPipe(), apparatusTime.GetTimeWhenPipeDisconnected());
		}
		HideGameObjectAndDisableCollider(pipe);
        controller.Finish();
    }

    private void ConnectPipe() //подцепить нужный шланг
    {
		var pipeNumber = testTubes.GetTestTubeNumberByPosition(destination.transform.position);
        if (pipeNumber==-1)
        {
            throw new Exception("pipe does not exist"); //ошибка - пытаемся подцепить куда-то не туда
        }
        VisualizeGameObjectAndEnableCollider(pipes[--pipeNumber]); // номер у шланга нормальный - отображаем его.
    }
}

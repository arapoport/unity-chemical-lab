﻿using System.Collections;
using UnityEngine;

public class MixingStick : Apparatuses{

	// Use this for initialization
	void Start () {
        mover = GameObject.Find("mover").GetComponent<Move>();
        controller = GameObject.Find("controller").GetComponent<Controller>();
        defaultPosition = gameObject.transform.position;

        defaultRotate = new Vector3(gameObject.transform.rotation.eulerAngles.x,
                        gameObject.transform.rotation.eulerAngles.y,
                        gameObject.transform.rotation.eulerAngles.z);

        reactions = GameObject.Find("reactions").GetComponent<Reactions>();
        check = GameObject.Find("ready").GetComponent<CheckLabs>();
	}
    Move mover;
    Controller controller;
    Vector3 defaultPosition;
    Vector3 defaultRotate;
    Reactions reactions;
    CheckLabs check;

    public float Upup = 0.05f;

    void OnMouseUp()
    {
        controller.AddMyGameObject(this);
    }

    public override void SetNewDefaultPosition(Vector3 newPosition)
    {
        defaultPosition = newPosition;
    }

    public override IEnumerator Select()
    {
        mover.MoveRelative(gameObject, 0, Upup, 0, 0.4f); //время
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
        mover.DoMove(gameObject, defaultPosition, 0.4f);
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator StartAnimation(Destination destination)
    {     
        if (!destination.name.Contains("testTube"))
        {

            mover.Rotate(gameObject, 0, 0, 0, 0.5f);
            yield return mover.WaitForCompletion();

            var v30 = gameObject.transform.position; //откуда начали, поднят над столом

            var v31 = new Vector3(destination.transform.position.x,
                    destination.transform.position.y + Upup,
                    destination.transform.position.z);

            mover.DoMove(gameObject, v31, 0.7f); //по горизонтали к назначению
            yield return mover.WaitForCompletion();

            var v32 = new Vector3(destination.transform.position.x,
                    destination.GetTop().transform.position.y,
                    destination.transform.position.z);
            mover.DoMove(gameObject, v32, 0.4f); // опустить в пробирку
            yield return mover.WaitForCompletion();

            mover.Shake(gameObject, 0.8f); //помешали
            yield return mover.WaitForCompletion();

			destination.SetDestinationSubstanceMaterialAndContent(name, check, reactions);

            mover.DoMove(gameObject, v31, 0.4f); //над пробиркой
            yield return mover.WaitForCompletion();

            mover.DoMove(gameObject, v30, 0.7f); //откуда начали
            yield return mover.WaitForCompletion();

            mover.Rotate(gameObject, defaultRotate, 0.5f);
            yield return mover.WaitForCompletion();

            mover.DoMove(gameObject, defaultPosition, 0.4f);
            yield return mover.WaitForCompletion();
        }
        else
        {
            mover.DoMove(gameObject, defaultPosition, 0.4f);
            yield return mover.WaitForCompletion();
        }

        controller.Finish();
    }
}
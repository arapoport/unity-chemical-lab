﻿using System.Collections;
using UnityEngine;

public class Glass : SourceParent {

	// Use this for initialization
	void Start () {
	    mover = FindObjectOfType<Move>();
	    controller = FindObjectOfType<Controller>();
        defaultPosition = gameObject.transform.position;
        defaultRotate = new Vector3(gameObject.transform.rotation.eulerAngles.x,
                        gameObject.transform.rotation.eulerAngles.y,
                        gameObject.transform.rotation.eulerAngles.z);
		substance = GetSubstance();
	    reactions = FindObjectOfType<Reactions>();
	    check = FindObjectOfType<CheckLabs>();
        menu = gameObject.GetComponent<MenuTextBox>();
	}
    Move mover;
    Controller controller;
    MenuTextBox menu;
    Vector3 defaultPosition;
    Vector3 defaultRotate;
    GameObject substance;
    Reactions reactions;
	CheckLabs check;

    public float volume; //при наличии меню
    float subtract=0; //при наличии меню

    public float Upup = 0.05f;
    public float OffsetZ = 0.01f;
    public float XRotate;
    public float YRotate;
    public float ZRotate;

    public bool BrimDestinationFlag=false;

    public bool setSubtract(float subtract)
    {
        this.subtract = subtract;

        Pour = 100 * subtract / volume;

        return destination.CheckAvailableVolume(this);        
    }

    public override void SetNewDefaultPosition(Vector3 newPosition)
    {
        defaultPosition = newPosition;
    }

    void OnMouseUp()
    {
        controller.AddMyGameObject(this);
    }

    public IEnumerator Resume()
    {
        volume = volume - subtract;
        var destinationGlass = destination.GetComponent<Glass>();
        if (destinationGlass != null)
        {
            destinationGlass.volume += subtract;
        }

        var destinationSubstance = destination.GetSubstance();
        var destTop = destination.GetTop(); //пустой объект в destination который расположен сверху
        var destBot = destination.GetBottom(); //а этот снизу
        string content;

        //Если вдруг стакан и источник и приемник то content будет браться из скрипта destination
        var destInSrc = gameObject.GetComponent<Destination>();

        if (BrimDestinationFlag)
        {
           Pour = destination.GetSourcePercent(this);
        }

        if (destInSrc != null)
        {
            content = destInSrc.Content;
            if (Pour == 100)
            {
                destInSrc.Content = destInSrc.DefaultContent;
            }
        }
        else //иначе из имени объекта
        {
            content = name;
        }

        if (content != "empty")
        {
            var v30 = gameObject.transform.position;

            mover.DoMove(gameObject, new Vector3(destTop.transform.position.x, destTop.transform.position.y, destTop.transform.position.z - OffsetZ), 0.7f); //по горизонтали к назначению
            yield return mover.WaitForCompletion();

            mover.Rotate(gameObject, XRotate, YRotate, ZRotate, 0.4f); // повернуть для того чтоб налить
            yield return mover.WaitForCompletion();

            //Наливание воды и заполнение пробирки--------------------------------------
            mover.WaterStart(destTop.transform.position,
                destBot.transform.position, substance.renderer.material, 0.3f);
            yield return mover.WaitForCompletion();

            destination.SetDestinationSubstanceMaterialAndContent(content, check, reactions);

            StartCoroutine(mover.Pour(substance, destinationSubstance, Pour, 1));
            yield return mover.WaitForCompletion();

            StartCoroutine(mover.WaterEnd(destBot.transform.position, 0.7f));
            yield return mover.WaitForCompletion();
            //-----------------------------------------------------------------------

            mover.Rotate(gameObject, defaultRotate, 0.4f); //повернуть в исходное положение
            yield return mover.WaitForCompletion();

            mover.DoMove(gameObject, v30, 0.7f);//по горизонтали к стартовому месту
            yield return mover.WaitForCompletion();

            mover.DoMove(gameObject, defaultPosition, 0.4f); //в исходную позицию
            yield return mover.WaitForCompletion();
        }
        else
        {
            mover.DoMove(gameObject, defaultPosition, 0.4f); //в исходную позицию
            yield return mover.WaitForCompletion();
        }

        controller.Finish();
    }

    Destination destination;
    public override IEnumerator StartAnimation(Destination destination)
    {
        this.destination = destination;
        if (menu==null)
        {
            Debug.Log("Меню нет");
            StartCoroutine(Resume());
        }
        else
        {
            menu.showMenu();
        }
        yield return new WaitForSeconds(0);
    }

    public override IEnumerator Select()
    {
        mover.MoveRelative(gameObject,0, Upup, 0,  0.4f); //время
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }

    public override IEnumerator Deselect()
    {
        //Debug.Log("glass deselect");
        mover.DoMove(gameObject, defaultPosition, 0.4f);
        yield return mover.WaitForCompletion();
        controller.FinishSelect();
    }
}

﻿using System.Collections;
using UnityEngine;

public abstract class MyGameObject : MonoBehaviour{
    public abstract IEnumerator Select();
    public abstract IEnumerator Deselect();
    public abstract void SetNewDefaultPosition(Vector3 position);
    public abstract IEnumerator StartAnimation(Destination destination);

    public void VisualizeGameObjectAndEnableCollider(Component component) //отобразить игровой объект и включить его коллайдер
    {
        component.renderer.enabled = true;
        component.collider.enabled = true;
    }
    public void HideGameObjectAndDisableCollider(Component component) //убрать отображение игрового объекта и его коллайдер
    {
        component.renderer.enabled = false;
        component.collider.enabled = false;
    }

    void OnMouseEnter()
    {
        MyCursor.SetClick();
    }

    void OnMouseExit()
    {
        MyCursor.SetNormal();
    }
}

﻿using UnityEngine;
using System.Collections;

public class LabsGUI : TextProcessing {

	// Use this for initialization
	void Start () {
	
	}

    Vector2 scrollPosition = Vector2.zero;
    bool show = false;

    public void Show(string text)
    {
        workText = text;
        scrollPosition = Vector2.zero;
        show = true;
    }

    public void Show(string[] text)
    {
        workText = "";
        
        for (int i = 0; i < text.Length - 1; i++)
        {
            workText += text[i];
            workText += '\n';
        }
        workText += text[text.Length - 1];
        
        scrollPosition = Vector2.zero;
        show = true;
    }

    public void Hide()
    {
        show = false;
    }

    string workText = "";
    void OnGUI()
    {
        if (show)
        {
            Rect position = new Rect(Screen.width * 0.53f, Screen.height * 0.1f, Screen.width / 3.5f + 20, Screen.height * 0.5f);
            GUIStyle style = new GUIStyle(GUI.skin.label);
            GUIContent content = new GUIContent(workText);
            style.fontSize = GetFontSize();
            style.normal.textColor = Color.black;
            float height = style.CalcHeight(content, Screen.width / 3.5f);
            scrollPosition = GUI.BeginScrollView(position, scrollPosition, new Rect(0, 0, Screen.width / 3.5f, height));
            GUI.TextArea(new Rect(0, 0, Screen.width / 3.5f, height), workText, style);
            GUI.EndScrollView();
        }
    }

}

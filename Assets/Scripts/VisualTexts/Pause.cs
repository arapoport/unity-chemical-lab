﻿using UnityEngine;

public class Pause : TextProcessing {

    public bool Paused;

    void Start()
    {
        if(Application.loadedLevelName == "Lab0")
        {
            bezopasnost = GameObject.Find("accident_prevention").GetComponent<AccidentPrevention>();
            notebook = GameObject.Find("notebook").GetComponent<BookLab0>();
            inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
            control = GameObject.Find("Player").GetComponent<ControlLab0>();
            run = GameObject.Find("Run").GetComponent<ShowRun>();
        }
       
        try
        {
            tips = GameObject.Find("Tips").GetComponent<Tips>();
        }
        catch
        {
        }
        ready = GameObject.Find("ready").GetComponent<CheckLabs>();
    }
    
    public void Play()
    {        
        Paused = false; 
        Time.timeScale = 1;   
        if(Application.loadedLevelName == "Lab0")
        {
            MyCursor.Hide();
            inventory.show();
            control.MoveEnable();
            control.RotateEnable();
        }
        
    }

    InventoryLab0 inventory;
    ControlLab0 control;
    ShowRun run;
    Tips tips;
    AccidentPrevention bezopasnost;
    CheckLabs ready;

    BookLab0 notebook;

    string pauseText;
    string exitButtonText;
    string continueButtonText;
    public void SetLanguage(string language)
    {
        var translator = gameObject.AddComponent<Translator>();
        pauseText = translator.GetTranslation(NameSpace.ShortTexts, "pauseText", language);
        exitButtonText = translator.GetTranslation(NameSpace.Buttons, "exitButtonText", language);
        continueButtonText = translator.GetTranslation(NameSpace.Buttons, "continueButtonText", language);
        Destroy(translator);
    }

    public void pause()
    {
        var block = false;
        if (run != null)
        {
            block = block || run.ShowFlag();
        }
        if (tips != null)
        {
            block = block || tips.enabled;
        }
        if (bezopasnost != null)
        {
            block = block || bezopasnost.ShowFlag();
        }
        if (notebook != null)
        {
            block = block || (!notebook.CloseBook);
        }
        block = block || ready.Lose;

        if (Paused == false && !block)
        {
            Paused = true;
            Time.timeScale = 0;           
            if(Application.loadedLevelName == "Lab0")
            {
                inventory.hide();
                control.RotateDisable();
                control.MoveDisable();
                MyCursor.Show();
            }
        }
    }

    float width = 200;
    float height = 150;

    float buttonH=30;
    float buttonW = 180;

    void OnGUI()
    {    
        if (Paused)
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
            GUI.Box(new Rect(Screen.width / 2 - width/2, Screen.height / 2 - height/2, width, height), pauseText);

            if (GUI.Button(new Rect(Screen.width / 2 - buttonW/2, Screen.height / 2 - height/2+20, buttonW, buttonH), continueButtonText))
            { // Продолжить
                Play();
            }
            if (GUI.Button(new Rect(Screen.width / 2 - buttonW/2, Screen.height / 2 + height/2-buttonH-10, buttonW, buttonH), exitButtonText))
            { // Выход из игры
                Application.Quit();
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Pause))
        {
            if (!Paused)
            {
                pause();
            }           
        }
    }


}

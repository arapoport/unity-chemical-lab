﻿using UnityEngine;

public class AccidentPrevention : TextProcessing
{
    bool show;
    void Start()
    {        
        Accept = false;
        show = false;        

        control = GameObject.Find("Player").GetComponent<ControlLab0>();
        interf = GameObject.Find("interface").GetComponent<Interf>();    
        ReSize();
    }
    GUITexture texture;
    string acceptButtonText;
    string rejectButtonText;
    public void SetLang(string language)
    {
        switch (language)
        {
            case "rus": texture = guiTexture; break;
            case "eng": texture = Eng; break;
        }
        var translator = gameObject.AddComponent<Translator>();
        acceptButtonText = translator.GetTranslation(NameSpace.Buttons, "acceptButtonText", language);
        rejectButtonText = translator.GetTranslation(NameSpace.Buttons, "rejectButtonText", language);
        Destroy(translator);
    }

    public GUITexture Eng;
    public bool Accept; //true если правила безопасности приняты

    float textHeight;
    float textWidth;

    int buttonH = 70;
    int buttonW = 150;

    ControlLab0 control;
    Interf interf;

    public void Show()
    {
        show = true;        
        control.RotateDisable();
        control.MoveDisable();
        interf.Hide();       
    }

    public bool ShowFlag()
    {
        return show;
    }

    public void ReSize()
    {
        textWidth = Screen.width / 1.5f;
        var bezDel = guiTexture.pixelInset.width / textWidth;
        textHeight = guiTexture.pixelInset.height / bezDel;        
    }

    Vector2 scrollPosition = Vector2.zero;

    void OnGUI()
    {
        if (show && Accept == false)
        {
            
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
            var style = new GUIStyle(GUI.skin.button);
            style.fontSize = 20;
            scrollPosition = GUI.BeginScrollView(new Rect(Screen.width/2 - textWidth/2 - 10, 0, textWidth+20, Screen.height), scrollPosition, new Rect(0, 0, textWidth, textHeight+buttonH));
            /*changes made in the below 2 lines */          
            GUI.DrawTexture(new Rect(0, 0, textWidth,textHeight), texture.texture);

            if (GUI.Button(new Rect(0, textHeight, buttonW, buttonH), rejectButtonText, style))
            {
                Application.Quit();
            }

            if (GUI.Button(new Rect(textWidth-buttonW, textHeight, buttonW, buttonH), acceptButtonText,style))
            {
                Accept = true;
                show = false;
                var notebook = GameObject.Find("notebook").GetComponent<BookLab0>();
                notebook.Show();
            }
            GUI.EndScrollView();              
        }
    }
}

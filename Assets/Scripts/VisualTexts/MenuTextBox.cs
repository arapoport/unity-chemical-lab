﻿using UnityEngine;
using System.Collections;

public class MenuTextBox : TextProcessing
{

    // Use this for initialization
    private void Start()
    {
        glass = gameObject.GetComponent<Glass>();
    }

    private Glass glass;
    private float menuHeight = 100;
    private float menuWidth = 200;
    private float buttonWidth = 70;
    private float buttonHeight = 50;
    private string enterAmountText;

    public void setLanguage(string lang)
    {
        Translator translator = gameObject.AddComponent<Translator>();
        enterAmountText = translator.GetTranslation(NameSpace.ShortTexts, "enterAmount", lang);
        errorParseText = translator.GetTranslation(NameSpace.ShortTexts, "errorParse", lang);
        exceedingLimitText = translator.GetTranslation(NameSpace.ShortTexts, "exceedingLimit", lang);
        notFitText = translator.GetTranslation(NameSpace.ShortTexts, "notFit", lang);
        Destroy(translator);
    }

    public void showMenu()
    {
        show = true;
    }

    private bool show = false;
    private bool showError = false;

    private string enteredText = "";
    private string errorParseText = "";
    private string exceedingLimitText = "";
    private string notFitText = "";
    private string errorMessage;

    private void ButtonOK()
    {

        int enteredVolume = -999;
        if (int.TryParse(enteredText, out enteredVolume))
        {
            if (glass.volume >= enteredVolume)
            {
                if (glass.setSubtract(enteredVolume))
                {
                    show = false;
                    StartCoroutine(glass.Resume());
                }
                else
                {
                    show = false;
                    showError = true;
                    errorMessage = notFitText;
                }
            }
            else
            {
                show = false;
                showError = true;
                errorMessage = exceedingLimitText;
            }
        }
        else
        {
            show = false;
            showError = true;
            errorMessage = errorParseText;
        }
    }


    void OnGUI()
    {
        Rect position = new Rect(Screen.width / 2 - menuWidth / 2, Screen.height / 2 - menuHeight / 2, menuWidth, menuHeight);
        Rect position2 = new Rect(Screen.width / 2 - menuWidth / 2, Screen.height / 2 - menuHeight / 2 + 20, menuWidth, 20);
        Rect position3 = new Rect(Screen.width / 2 + menuWidth / 2 - buttonWidth, Screen.height / 2 + menuHeight / 2 - buttonHeight, buttonWidth, buttonHeight);
        if (show)
        {            
            GUI.Box(position, enterAmountText);
            GUI.SetNextControlName("textBox");
            enteredText = GUI.TextField(position2, enteredText);
            GUI.FocusControl("textBox");
            if (GUI.Button(position3, "OK"))
            {
                ButtonOK();
            }           
        }

        if (showError)
        {
            GUI.Box(position, errorMessage);

            if (GUI.Button(position3, "OK"))
            {
                show = true;
                showError = false;
            }
            
        }
    }
}

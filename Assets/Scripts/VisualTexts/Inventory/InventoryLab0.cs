﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryLab0 : TextProcessing {

	// Use this for initialization
	void Start () {

        titlesRus = new List<string>(); //название
        titlesEng = new List<string>();
        textures = new List<Texture>(); //текстура в инвентаре
        go = new List<TakeLab0>();       

        leftInventory = GameObject.Find("left").GetComponent<LeftInvetar>();
        rightInventory = GameObject.Find("right").GetComponent<RightInventar>();

        ChangeResolution();
    }

    LeftInvetar leftInventory;
    RightInventar rightInventory;

    public void ChangeResolution()
    {
        itemSize = 100;
        bottomWidth = Screen.width / 1.2f;
        bottomWidth -= bottomWidth % itemSize;
        otstup = (Screen.width - bottomWidth) / 2;
        CountInRow = (int)(bottomWidth / itemSize); ;
        leftInventory.AutoPosition();
        rightInventory.AutoPosition();
    }

    public void SetLanguage(string lang)
    {
        language = lang;
    }

    string language;
    float bottomWidth;
    float otstup;

    //public bot_panel bottom_panel;

    int border = 4;
    public int CountInRow;

    GUIStyle styleBaton; //стиль кнопок инвентаря (предметы)

    float itemSize; //размер ребра иконки

    public int Begin; //первый элемент инвентаря

    public void hide()
    {
        showFlag = false;
        rightInventory.guiTexture.enabled = false;
        leftInventory.guiTexture.enabled = false;
    }

    public void show()
    {
        showFlag = true;
        rightInventory.guiTexture.enabled = true;
        leftInventory.guiTexture.enabled = true;
    }

    bool showFlag = true;
    void OnGUI()
    {
        if (showFlag)
        {
            styleBaton = new GUIStyle(GUI.skin.button);
            styleBaton.border.right = border;
            styleBaton.border.left = border;
            styleBaton.border.bottom = border;
            styleBaton.border.top = border;

            var prTitle = "";
            var title = "";

            var offsetHorizont = 0;

            var count = 0;

            if (go.Count <= CountInRow)
            {
                leftInventory.Hide();
                rightInventory.Hide();
                Begin = 0;
                count = go.Count;

            }
            else
            {
                count = CountInRow;
                leftInventory.Show();
                rightInventory.Show();
            }


            for (var i = Begin; i < count + Begin; i++)
            {
                try
                {
                    var temp = new Rect(otstup + offsetHorizont * itemSize,
                                         Screen.height - itemSize,
                                        itemSize, itemSize);


                    GUIContent gc;

                    switch (language)
                    {
                        case "rus": gc = new GUIContent("", textures[i], titlesRus[i]); break;
                        case "eng": gc = new GUIContent("", textures[i], titlesEng[i]); break;
                        default: gc = new GUIContent("", textures[i], titlesEng[i]); break;
                    }   
                   


                    if (GUI.Button(temp, gc, styleBaton))
                    {
                        StartCoroutine(go[i].DropThis());
                        go.RemoveAt(i);
                        textures.RemoveAt(i);
                        titlesRus.RemoveAt(i);
                        titlesEng.RemoveAt(i);
                        if (Begin > 0)
                        {
                            Begin--;
                        }
                        i = i - 2;
                    }

                    title = GUI.tooltip;

                    if (title != prTitle)
                    {
                        var tempHint = new Rect(otstup + offsetHorizont * itemSize,
                                         Screen.height - itemSize - 20,
                                        300, 20);

                        GUI.Label(tempHint, title);
                        prTitle = title;
                    }
                    
                    offsetHorizont++;
                }
                catch
                {
                    // Debug.Log("i="+i);
                }

            }
        }        

        
    }

    List<string> titlesRus;
    List<string> titlesEng;
    List<Texture> textures;
    List<TakeLab0> go;
   // public static List<int> positions;

    public string[] GetNames()
    {
        var names = new string[go.Count];
        for (var i = 0; i < names.Length; i++)
        {
            names[i] = go[i].name;
        }
        return names;
    }

    public void Add(TakeLab0 obj)
    {
        go.Add(obj);
        var translator = gameObject.AddComponent<Translator>();
        var rus = translator.GetTranslation(NameSpace.Hints, obj.gameObject.name, "rus");
        titlesRus.Add(rus);
        var eng = translator.GetTranslation(NameSpace.Hints, obj.gameObject.name, "eng");
        titlesEng.Add(eng);
        textures.Add(obj.Texture);
        Destroy(translator);
    }
}

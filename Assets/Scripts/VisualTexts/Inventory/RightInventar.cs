﻿using UnityEngine;

public class RightInventar : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

    public void AutoPosition()
    {
        float itemSize = 100;

        var bottomWidth = Screen.width / 1.2f;
        bottomWidth -= bottomWidth % itemSize;
        var otstup = (Screen.width - bottomWidth) / 2;

        var otstup2 = otstup + bottomWidth;

        guiTexture.pixelInset = new Rect(otstup2, 0, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
        inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
    }

    InventoryLab0 inventory;

    void OnMouseUp()
    {
        if (inventory.Begin+inventory.CountInRow<inventory.GetNames().Length)
        {
            inventory.Begin++;
        }
    }

    

    public void Hide()
    {
        guiTexture.enabled = false;
    }

    public void Show()
    {
        guiTexture.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

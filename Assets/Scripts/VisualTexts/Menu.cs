﻿using UnityEngine;

public class Menu : MonoBehaviour {

	// Use this for initialization
	void Start () {
        parent = transform.parent.GetComponent<Pipette>();
        var count = transform.childCount;
        options = new Option[count];
        for (var i = 0; i < count; i++)
        {
            options[i] = transform.GetChild(i).GetComponent<Option>();
        }
        Hide();
	}
    Pipette parent;
    Option[] options;
    public string SelectedValue
    {
        get { return selectedValue; }
    }

    string selectedValue;

    public void SetValue(string value)
    {
        selectedValue = value;
        StartCoroutine(parent.Resume());
        Hide();
    }

    public void Show(Vector3 position)
    {
        foreach (var option in options)
        { option.Show(); }
        transform.position = position;
    }

    private void Hide()
    {
        foreach (var t in options)
        { t.Hide(); }
    }
}

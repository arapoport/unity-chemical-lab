﻿using UnityEngine;

public class WorkAlgorithm : TextProcessing {

	// Use this for initialization
	void Start () {
        control = GameObject.Find("Player").GetComponent<ControlLab0>();        
        inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
        tipH = Screen.height / 1.5f;
        tipW = Screen.width / 1.5f;
	}

    InventoryLab0 inventory;
    ControlLab0 control; //управление на Player
    GUIStyle style;

    private bool show;
    float tipW;
    float tipH;
    string workAlgorithmText;

    public int Number=-1;
    public void SetLanguage(string language)
    {
        if (Number <= -1) return;
        guiTexture.enabled = true;
        var translator = new Translator();
        workAlgorithmText = translator.GetPlainTextTranslation(NameSpace.WorkAlgorithms, "Lab" + Number, language);
    }

    public void Show()
    {
        tipH = Screen.height / 1.2f;
        tipW = Screen.width / 1.2f;
        control.MoveDisable();
        control.RotateDisable();
        inventory.hide();
        show = true;
    }

    public bool IsShow()
    {
        return show;
    }

    void OnMouseDown()
    {
        if (Number != -1)
        {
            Show();
        }
    }

    Vector2 scrollPosition = Vector2.zero;

    void OnGUI()
    {
        if (!show) return;
        style = new GUIStyle(GUI.skin.textArea) {fontSize = 20, alignment = TextAnchor.MiddleCenter};
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");

        GUILayout.BeginArea(new Rect(Screen.width / 2 - tipW / 2, Screen.height / 2 - tipH / 2, tipW, tipH));
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(tipW), GUILayout.Height(tipH));
        /*changes made in the below 2 lines */
        GUI.skin.box.wordWrap = true;     // set the wordwrap on for box only.
        GUILayout.Box(workAlgorithmText, style);        // just your message as parameter.

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("ОК",  GUILayout.Width(100), GUILayout.Height(50)))
        {
            show = false;
            control.RotateEnable();
            control.MoveEnable();
            inventory.show();
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();        

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

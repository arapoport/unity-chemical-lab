﻿using UnityEngine;

public class Option : MonoBehaviour {

	// Use this for initialization
	void Start () {
        menu = transform.parent.GetComponent<Menu>();
        text = transform.FindChild("text").gameObject;
        source = transform.parent.transform.parent.gameObject.GetComponent<SourceParent>();
	}

    Menu menu;
    GameObject text;
    SourceParent source;
    public string option;
    public string Rus;
    public string Eng;
    public float Percent;

    public void SetLang(string language)
    {     
        var mesh = text.GetComponent<TextMesh>();
        switch (language)
        {
            case "rus": mesh.text = Rus; break;
            case "eng": mesh.text = Eng; break;
        }
    }

    void OnMouseUp()
    {
        menu.SetValue(option);
        source.Pour = Percent;
    }

    public void Hide()
    {
        text.renderer.enabled = false;
        renderer.enabled = false;
        collider.enabled = false;
      
    }

    public void Show()
    {
        text.renderer.enabled = true;
        renderer.enabled = true;
        collider.enabled = true;
    }

  
	
	// Update is called once per frame
	void Update () {
	
	}
}

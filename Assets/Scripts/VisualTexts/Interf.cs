﻿using UnityEngine;

public class Interf : TextProcessing {

	// Use this for initialization
	void Start () {        
        top = Screen.height - NormalHeight;
        guiTexture.pixelInset = new Rect(0, top, Screen.width, NormalHeight); 
        font = Resources.Load<Font>("Fonts/NeoSansW1G-Medium_0");
        fontSize = GetFontSize();         
        LoadText(LabNumber);

        try
        {
            tips = GameObject.Find("Tips").GetComponent<Tips>();
        }
        catch
        {
        }

        try
        {
            wr = GameObject.FindObjectOfType<WorkResults>();
            inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
            book = GameObject.Find("notebook_gui").GetComponent<BookSizeLab0>();
            bezopasnost = GameObject.Find("accident_prevention").GetComponent<AccidentPrevention>();
            run = GameObject.Find("Run").GetComponent<ShowRun>();
            control = GameObject.Find("Player").GetComponent<ControlLab0>();
            labs = GameObject.FindObjectsOfType<SelectLabs>();
        }
        catch
        {
        }
	}
    Tips tips;
    InventoryLab0 inventory;
    AccidentPrevention bezopasnost; //техника безопасности
    BookSizeLab0 book; //для нулевой лабы
    ShowRun run; //начальная картинка
    ControlLab0 control;
    SelectLabs[] labs;
    WorkResults wr;
    float NormalHeight;
    float OverHeight;
    public int Speed;
    float top;   
   
    public int LabNumber = 0;
    string purposeText;
    string detailsText;

    Font font;
    string language;
    public void SetLanguage(string language)
    {
        this.language = language;          
    }

    public void LoadText(int labNumber)
    {
        LabNumber = labNumber;
        detailsText = "";
        var translator = gameObject.AddComponent<Translator>();
        var str = GetLinesFromString(translator.GetPlainTextTranslation(NameSpace.Purposes, "Lab" + labNumber, language));
        Destroy(translator);

        purposeText = str[0];
        detailsText = GetStringFromLines(str, 1);
        reSize();
    }

    public void Hide()
    {
        guiTexture.enabled = false;
    }

    public void Show()
    {
        guiTexture.enabled = true;
    }

    bool detailsShow;
    int fontSize = 1;
    void OnGUI()
    {
        var style = new GUIStyle(GUI.skin.label) {fontSize = fontSize, font = font, fontStyle = FontStyle.Bold};
        
        GUIContent content = new GUIContent(purposeText);
        NormalHeight = style.CalcHeight(content, Screen.width - 300);
        if (NormalHeight < 60)
        {
            NormalHeight = 60;
        }
        content = new GUIContent(detailsText);
        OverHeight = style.CalcHeight(content, Screen.width - 100);
        OverHeight += NormalHeight;

        GUILayout.BeginArea(new Rect(60, 5, Screen.width-100, OverHeight));    
        GUILayout.Label(purposeText, style, GUILayout.Width(Screen.width - 300));        
        if (detailsShow)
        {            
            GUILayout.Label(detailsText, style, GUILayout.Width(Screen.width - 100));            
        }
        GUILayout.EndArea();
    }

    void OnMouseOver()
    {
        show = true;
        if (guiTexture.pixelInset.height < OverHeight)
        {
            top = top - Speed;
            guiTexture.pixelInset = new Rect(0, top, Screen.width, guiTexture.pixelInset.height + Speed);
        }
        else if (guiTexture.pixelInset.height >= OverHeight)
        {
            //раскрылся
            detailsShow = true;
        }
    }
    bool show;
    void OnMouseExit()
    {
        show = false;
    }

    public void reSize()
    {
        if (NormalHeight < 60)
        {
            Debug.Log("Мы тут");
            NormalHeight = 60;
        }
        top = Screen.height - NormalHeight;
        fontSize = GetFontSize();
        guiTexture.pixelInset = new Rect(0, top, Screen.width, NormalHeight);
       
    }
    int screenWidth;
    int screenHeight;
	// Update is called once per frame
	void Update () {
        if (!show && guiTexture.pixelInset.height > NormalHeight)
        {
            //Закрывается
            detailsShow = false;
            top = top + Speed;
            guiTexture.pixelInset = new Rect(0, top, Screen.width, guiTexture.pixelInset.height - Speed);
        }

        if (Input.GetKeyUp(KeyCode.F10))
        {
            Screen.SetResolution(99999, 99999, true);            
        }
        if (Screen.width != screenWidth || Screen.height != screenHeight)
        {
            if (!Screen.fullScreen)
            {
                try
                {
                    control.RotateDisable();
                }
                catch
                {
                }
            }
            Screen.lockCursor = !Screen.lockCursor;
            Screen.lockCursor = !Screen.lockCursor;
            Debug.Log("Сменилось разрешение");
            screenWidth = Screen.width;
            screenHeight = Screen.height;
            reSize();          
            try
            {
                tips.reSize();
            }
            catch
            {
            }

            try
            {
                wr.reSize();
                foreach (var lab in labs)
                {
                    lab.reSize();
                }
                inventory.ChangeResolution();
                book.ReSize();
                run.ReSize();
                bezopasnost.ReSize();
            }
            catch
            {
            }
        }

	}
}

﻿using UnityEngine;

public class WorkResults : TextProcessing {

	// Use this for initialization
	void Start () {
        try
        {
            control = GameObject.Find("Player").GetComponent<ControlLab0>();
            inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
        }
        catch
        {
        }
        reSize();      
	}
    InventoryLab0 inventory;
    ControlLab0 control; //управление на Player

    public bool IsShow { get; private set; }

    float tipW;
    float tipH;
    string workText;
    public int Number;
    string language;
    public void SetLanguage(string language)
    {        
        this.language = language;
    }

    public void Show()
    {
        if (Number != -1)
        {
            var translator = gameObject.AddComponent<Translator>();
            workText = translator.GetPlainTextTranslation(Application.loadedLevelName == "Lab0" ? NameSpace.WorkAlgorithms : NameSpace.Results, "Lab" + Number, language);
            Destroy(translator);
        }
        try
        {
            control.MoveDisable();
            control.RotateDisable();
            inventory.hide();
        }
        catch
        {
        }
        IsShow = true;
    }

    void OnMouseDown()
    {
        if (Number != -1)
        {
            Show();
        }
    }

    public void reSize()
    {
        tipH = Screen.height / 1.2f;
        tipW = Screen.width / 1.5f;
        fontSize = GetFontSize();
    }

    int fontSize;
    GUIStyle style;
    Vector2 scrollPosition = Vector2.zero;

    void OnGUI()
    {
        if (!IsShow) return;
        style = new GUIStyle(GUI.skin.textArea) { fontSize = fontSize, alignment = TextAnchor.MiddleCenter };
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");

        GUILayout.BeginArea(new Rect(Screen.width / 2 - tipW / 2, Screen.height / 2 - tipH / 2, tipW, tipH));
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(tipW), GUILayout.Height(tipH));
        /*changes made in the below 2 lines */
        GUI.skin.box.wordWrap = true;     // set the wordwrap on for box only.
        GUILayout.Box(workText, style);        // just your message as parameter.

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("ОК", GUILayout.Width(SizeConstants.ButtonW), GUILayout.Height(SizeConstants.ButtonH)))
        {
            IsShow = false;
            try
            {
                control.RotateEnable();
                control.MoveEnable();
                inventory.show();
            }
            catch
            {
            }
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
	
}

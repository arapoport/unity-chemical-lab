﻿using UnityEngine;

public class LanguageIcon : MonoBehaviour {

	// Use this for initialization
	void Start () {
        ChangeLang = transform.parent.GetComponent<ChangeLanguage>();

	}

    ChangeLanguage ChangeLang;

    void OnMouseDown()
    {
        if (ChangeLang.IsHide())
        {
            ChangeLang.Show();
        }
        else if (ChangeLang.IsShow())
        {            
            ChangeLang.SetLang(Language);
            ChangeLang.Hide();
        }
    }

    public string Language;



	// Update is called once per frame
	void Update () {

	}
}

﻿using UnityEngine;

public class ChangeLanguage : MonoBehaviour {

	// Use this for initialization
	void Awake () {

        language = PlayerPrefs.GetString("Language", "rus");

        if (language == "rus")
        {
            otherLang = Eng;
        }
        else if (language == "eng")
        {
            otherLang = Rus;
        }

	    top = otherLang.pixelInset.yMin;
        SetLang(language);
	}   
    

    public void SetLang(string l)
    {
        switch (l)
        {
            case "rus":
                Rus.transform.position = new Vector3(Rus.transform.position.x, Rus.transform.position.y, 3);
                Eng.transform.position = new Vector3(Eng.transform.position.x, Eng.transform.position.y, 2);
                language = l;
                break;
            case "eng":
                Eng.transform.position = new Vector3(Eng.transform.position.x, Eng.transform.position.y, 3);
                Rus.transform.position = new Vector3(Rus.transform.position.x, Rus.transform.position.y, 2);
                language = l;
                break;
        }

        if (Application.loadedLevelName == "Lab0")
        {
            var sl = FindObjectsOfType<SelectLabs>(); //для 0й лабы
            for (var i = 0; i < sl.Length; i++)
            { sl[i].SetLanguage(language); }

            var run = GameObject.Find("Run").GetComponent<ShowRun>();
            run.SetLang(language);

            var bezopasnost = GameObject.Find("accident_prevention").GetComponent<AccidentPrevention>();
            bezopasnost.SetLang(language);

            var inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
            inventory.SetLanguage(language);

            var options = FindObjectsOfType<Option>();
            for (var i = 0; i < options.Length; i++)
            { options[i].SetLang(language); }
        }
		try 
		{
        	var checkLabs = GameObject.Find("ready").GetComponent<CheckLabs>();
        	checkLabs.SetLanguage(language);
		}
		catch
		{
			Debug.Log ("checkLabs object not found");
		}

		try 
		{
        	var pause = GameObject.Find("Player").GetComponent<Pause>();
        	pause.SetLanguage(language);
		}
		catch
		{
			Debug.Log ("Player object not found");
		}
		try 
		{
        var wr = GameObject.Find("workResults").GetComponent<WorkResults>();
        wr.SetLanguage(language);
		}
		catch
		{
			Debug.Log ("workResults object not found");
		}

        var hints = FindObjectsOfType<Hint>();
        for (var i = 0; i < hints.Length; i++)
        {  hints[i].SetLanguage(language);  }

		try
		{
        	var readyButton = GameObject.Find("ready").GetComponent<MouseOverGuiTexture>();
        	readyButton.ChangeTexture(language);
		}
		catch
		{
			Debug.Log ("ready object not found");
		}

        try
        {
            var interf = GameObject.Find("interface").GetComponent<Interf>();
            interf.SetLanguage(language);
            interf.LoadText(interf.LabNumber);
        }
        catch
        {
        }

        try
        {
            var tips = GameObject.Find("Tips").GetComponent<Tips>();
            tips.SetLanguage(language);
        }
        catch
        {
        }

        try
        {
            var controller = GameObject.Find("controller").GetComponent<Controller>();
            controller.SetLanguage(language);
        }
        catch
        {
        }

        //try
       // {
            var menuTextBox = FindObjectsOfType<MenuTextBox>();
            foreach (var menu in menuTextBox)
            {
                menu.setLanguage(language);
            }
     //   }
      //  catch
     //   {
     //   }

         PlayerPrefs.SetString("Language", language);
    }

    public void Show()
    {
        show = true;
    }

    public void Hide()
    {
        show = false;
    }

    public bool IsHide()
    {
        if (show == false && offset == 0)
        {
            return true;
        }
        return false;
    }

    public bool IsShow()
    {
        if (show && offset == -40)
        {
            return true;
        }
        return false;
    }

    public GUITexture Rus;
    public GUITexture Eng;


    bool show;

    float offset;
    private const float Increment = 4f;
    float top;
    string language;

    GUITexture otherLang;



	// Update is called once per frame
	void Update () {
        if (show && offset > -40)
        {
            offset = offset - Increment;
            otherLang.pixelInset = new Rect(otherLang.pixelInset.x, top + offset, otherLang.pixelInset.width, otherLang.pixelInset.height);
        }
        else if (!show && offset < 0)
        {
            offset = offset + Increment;
            otherLang.pixelInset = new Rect(otherLang.pixelInset.x, top + offset, otherLang.pixelInset.width, otherLang.pixelInset.height);
        }
        else if (!show && offset == 0)
        {
            if (language == "rus")
            {
                otherLang = Eng;
            }
            else if (language == "eng")
            {
                otherLang = Rus;
            }

            top = otherLang.pixelInset.yMin;
        }
	}
}

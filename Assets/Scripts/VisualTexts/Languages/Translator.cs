﻿using System.Linq;
using UnityEngine;

public class Translator : TextProcessing {

    public string GetTranslation(NameSpace nameSpace, string key, string lang)
    {
        string value;
        var gotIt = false;
        var textAsset = Resources.Load("Translations/" + lang + "/" + nameSpace, typeof(TextAsset)) as TextAsset;
        try
        {
            var lines = GetLinesFromString(textAsset.text);
            var dict = lines.Select(l => l.Split('=')).ToDictionary(a => a[0].Trim(), a => a[1].Trim());
            gotIt = dict.TryGetValue(key, out value);
        }
        catch { value = "undefined"; }
        if (!gotIt) { value = "undefined"; }
        return value;
    }

    public string GetPlainTextTranslation(NameSpace nameSpace, string key, string lang)
    {
        var textAsset = Resources.Load("Translations/" + lang + "/" + nameSpace + "/" + key, typeof(TextAsset)) as TextAsset;
        return textAsset.text;
    }

}

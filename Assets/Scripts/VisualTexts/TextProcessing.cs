﻿using System;
using UnityEngine;

public class TextProcessing : MonoBehaviour {

    public enum NameSpace { Buttons, Hints, Tips, ShortTexts, WorkAlgorithms, Results, Purposes }

    public string[] GetLinesFromString(string inputString)
    {
        var archDelim = new[] { '\r', '\n' };
        return inputString.Split(archDelim, StringSplitOptions.RemoveEmptyEntries);
    }

    public int GetFontSize()
    {
        var size = 0;
        if (Screen.width >= 495 && Screen.width < 535)
        {
            size = 10;
        }
        else if (Screen.width >= 535 && Screen.width < 600)
        {
            size = 12;
        }
        else if (Screen.width >= 600 && Screen.width < 700)
        {
            size = 15;
        }
        else if (Screen.width >= 700 && Screen.width < 800)
        {
            size = 16;
        }
        else if (Screen.width >= 800 && Screen.width < 900)
        {
            size = 17;
        }
        else if (Screen.width >= 900 && Screen.width < 1000)
        {
            size = 18;
        }
        else if (Screen.width >= 1000 && Screen.width < 1100)
        {
            size = 19;
        }
        else if (Screen.width >= 1200 && Screen.width < 1300)
        {
            size = 20;
        }
        else if (Screen.width >= 1300)
        {
            size = 21;
        }
        return size;
    }

    public string GetStringFromLines(string[] inputLines, int startIndex)
    {
        var result="";
        for (var i = 1; i < inputLines.Length; i++)
        {
            result += inputLines[i];
            if (i < inputLines.Length - 1)
            {
                result += "\n";
            }
        }
        return result;
    }

}

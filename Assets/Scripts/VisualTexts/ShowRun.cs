﻿using UnityEngine;

public class ShowRun : TextProcessing {

	// Use this for initialization
	void Start () {
        control = GameObject.Find("Player").GetComponent<ControlLab0>();
        zaslonka = GameObject.Find("Zaslonka");
        ReSize();
        Show();
	}

    float runW;
    float runH;

    float temp;
    string language;

    bool show;

    int buttonH = 30;
    int buttonW = 100;
    string buttonText;

    GUITexture run;
    public GUITexture RunEng;
    public GUITexture controlRus;
    public GUITexture controlEng;

    ControlLab0 control; //Скрипт на player
    GameObject zaslonka;

    public void ReSize()
    {        
        runH = Screen.height - 80;
        var delitel = run.pixelInset.height / runH;
        runW = run.pixelInset.width / delitel;

        if (runW <= Screen.width) return;

        runW = Screen.width;
        delitel = run.pixelInset.width / runW;
        runH = run.pixelInset.height / delitel;      
    }

    public bool ShowFlag()
    {
        return show;
    }

    static bool firstRun = true;

    public void Show()
    {
        if (firstRun)
        {
            string login = "Admin";
            try
            {
                var loginGO = GameObject.FindGameObjectWithTag("Login");
                login = loginGO.name;
            }
            catch {}
            Session.Login = login;
            Debug.Log("Login=" + Session.Login);
            show = true;
            control.RotateDisable();
            control.MoveDisable();
            zaslonka.collider.enabled = true;
            firstRun = false;
        }
        else
        {
            Debug.Log("Мы запускаемся второй раз");
            control.RotateDisable();
            control.RotateEnable();
        }
    }

    public void SetLang(string language)
    {
        if (counter == 1)
        {
            switch (language)
            {
                case "rus": run = guiTexture; break;
                case "eng": run = RunEng; break;
            }
        }
        else if(counter == 2)
        {
            switch (language)
            {
                case "rus": run = controlRus; break;
                case "eng": run = controlEng; break;
            }
        }
        var translator = gameObject.AddComponent<Translator>();
        buttonText = translator.GetTranslation(NameSpace.Buttons, "closeButtonText", language);
        Destroy(translator);
        this.language = language;
    }



    int counter = 1; //для картинки с управлением

    void OnGUI()
    {
        if (show && run!=null)
        {
            GUI.DrawTexture(new Rect(Screen.width/2 - runW/2, 80, runW, runH), run.texture);        //80 это для выбора языка
            if (GUI.Button(new Rect(Screen.width/2+runW/2-buttonW, 80+runH-buttonH, buttonW, buttonH), buttonText))
            {
                if (counter == 1)
                {
                    counter = 2;
                    SetLang(language);
                    ReSize();
                }
                else if(counter == 2)
                {
                    show = false;
                    guiTexture.enabled = false;
                    control.MoveEnable();
                    control.RotateEnable();
                    zaslonka.collider.enabled = false;
                    var tips = GameObject.Find("Tips").GetComponent<Tips>();
                    tips.Show();
                }
                
            }  
        }
    }

}

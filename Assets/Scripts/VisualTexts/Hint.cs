﻿using UnityEngine;

public class Hint : TextProcessing {

	// Use this for initialization
	void Start () {
        image = Resources.Load("Textures/speech") as Texture;
        myCollider = GetComponent<BoxCollider>();
	}
    BoxCollider myCollider;
    public int OffsetY;
    public int OffsetX;
    bool mouseOver;
    Texture image;
    private string selectedText;

    public string GetSelectedText()
    {
        return selectedText;
    }

    public void SetLanguage(string lang)
    {
       var translator = gameObject.AddComponent<Translator>();
       selectedText = translator.GetTranslation(NameSpace.Hints, gameObject.name, lang);
       Destroy(translator);
    }

    void OnGUI()
    {
        if (!mouseOver) return;        
        var screenPosition = Camera.main.WorldToScreenPoint(colliderPos);
        var cameraRelative = Camera.main.transform.InverseTransformPoint(transform.position);
        if (cameraRelative.z < 0) return;
        var style = new GUIStyle
        {
            fontStyle = FontStyle.Bold,
            normal = {textColor = Color.black},
            alignment = TextAnchor.MiddleCenter
        };
        var positionSpeech = new Rect(screenPosition.x - 20 + OffsetX, Screen.height - screenPosition.y - OffsetY,  150, 100);
        var position = new Rect(screenPosition.x - 17 + OffsetX, Screen.height - screenPosition.y - OffsetY+7, 140, 50); //позиция текста
        GUI.Label(positionSpeech, image);              
        style.wordWrap = true;
        GUI.Label(position, selectedText, style);
    }
    Vector3 colliderPos;
    void OnMouseEnter()
    {
        colliderPos = transform.TransformPoint(myCollider.center);
        mouseOver = true;
    }
    
    void OnMouseExit()
    {
        mouseOver = false;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class Tips : TextProcessing {

	// Use this for initialization
	void Start () {
        scrollPosition = Vector2.zero;
        reSize();
	}

    public void reSize()
    {
        tipH = Screen.height / 1.5f;
        tipW = Screen.width / 1.5f;
    }

    string[] GetTips(string language)
    {
        var translator = gameObject.AddComponent<Translator>();
        var buf = translator.GetPlainTextTranslation(NameSpace.Tips, "lab" + Number, language);
        
        var tips = new List<string>();
        var indexes = new List<int>();

        for (var i = 0; i < buf.Length; i++)
        {
            if (buf[i] == '►')
            {
                indexes.Add(i);
            }
        }

        tips.Add(buf.Substring(0, indexes[0]));
        if (indexes.Count > 1)
        {
            for (var i = 1; i < indexes.Count; i++)
            {
                tips.Add(buf.Substring(indexes[i - 1]+2, indexes[i] - indexes[i - 1]-2));
            }
        }


        return tips.ToArray();        
    }

    string backButtonText;
    string nextButtonText;
    string startButtonText;


    public void SetLanguage(string language)
    {
        tips = GetTips(language);
        var translator = gameObject.AddComponent<Translator>();
        backButtonText = translator.GetTranslation(NameSpace.Buttons, "backButtonText", language);
        nextButtonText = translator.GetTranslation(NameSpace.Buttons, "nextButtonText", language);
        startButtonText = translator.GetTranslation(NameSpace.Buttons, "startButtonText", language);
        Destroy(translator);
    }

    ControlLab0 control; //Скрипт на player

    public void Show()
    {   
        enabled = true;        
        try
        {
            control = GameObject.Find("Player").GetComponent<ControlLab0>();
            control.RotateDisable();
            control.MoveDisable();
        }
        catch
        {
        }
    }

    public int Number;
    float tipW = 700;
    float tipH = 500;
    int counter;
    string[] tips;
    string rightButtonText = "";

    Vector2 scrollPosition = Vector2.zero;

    void OnGUI()
    {
        var style = new GUIStyle(GUI.skin.textArea) {fontSize = 20, alignment = TextAnchor.MiddleLeft};

        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");        

        GUILayout.BeginArea(new Rect(Screen.width / 2 - tipW / 2, Screen.height / 2-tipH/2, tipW, tipH+50));
        scrollPosition = GUILayout.BeginScrollView(scrollPosition); // GUILayout.Width(tipW), GUILayout.Height(tipH)
        /*changes made in the below 2 lines */
        GUI.skin.box.wordWrap = true;     // set the wordwrap on for box only.
        GUILayout.Box(tips[counter],style);        // just your message as parameter.

        if (counter < tips.Length - 1)
        {
            rightButtonText = nextButtonText;    
        }
        else
        {
            rightButtonText = startButtonText;
        }              
        
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (counter > 0)
        {
            if (GUILayout.Button(backButtonText, GUILayout.Width(100), GUILayout.Height(50)))
            {
                counter--;
            }
        }

        if (GUILayout.Button(rightButtonText, GUILayout.Width(100), GUILayout.Height(50)))
        {
            if (counter < tips.Length - 1)
            {
                counter++;
            }
            else
            {
                try
                {
                    control.MoveEnable();
                    control.RotateEnable();
                }
                catch
                {
                }
                enabled = false;
                Watches.LetTheCountBegin = true;
            }
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();

        GUILayout.EndArea();   
    }

}

﻿using UnityEngine;

public class TipsLab6 : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Tips = "";
	}


    public static bool Show;
    float tipW = 700;
    float tipH = 400;

    float buttonW = 100;
    float buttonH = 50;
    string nextButtonText = "";
    public static string Tips;


    void OnGUI()
    {
        if (Show)
        {
            var style = new GUIStyle(GUI.skin.textArea);
            style.fontSize = 20;
            style.alignment = TextAnchor.MiddleCenter;
            
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
            GUI.Label(new Rect(Screen.width / 2 - tipW / 2, Screen.height / 2 - tipH / 2, tipW, tipH),
                Tips,style);

                nextButtonText = "ОК";
            

            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 + tipH / 2, buttonW, buttonH), nextButtonText))
            {
                    Show = false;
                   // this.enabled = false;
            }
        }

    }

	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;

public class ShowLabDescription : MonoBehaviour {

	// Use this for initialization
	void Start () {
        textWidth = Screen.width / 3f;
        var bezDel = guiTexture.pixelInset.width / textWidth;
        textHeight = guiTexture.pixelInset.height / bezDel;
	}

    float textHeight;
    float textWidth;
    float temp;
    bool show;
    private const int ButtonH = 30;
    private const int ButtonW = 100;
    public GameObject Zaslonka;

    public void Show()
    {
        show = true;
        guiTexture.enabled = true;
        Zaslonka.collider.enabled = true;
    }

    void OnGUI()
    {
        if (!show) return;
        if (temp + -40 * Input.GetAxis("Mouse ScrollWheel") > 0
            && temp + -40 * Input.GetAxis("Mouse ScrollWheel") < -1 * (Screen.height - textHeight - ButtonH))
        {
            temp += -40 * Input.GetAxis("Mouse ScrollWheel");
        }
        GUI.depth = 0;
        var posButton = new Rect(Screen.width / 2 + textWidth / 2 - ButtonW, textHeight - temp, ButtonW, ButtonH);
        guiTexture.pixelInset = new Rect(Screen.width / 2 - textWidth / 2, Screen.height - textHeight + temp, textWidth, textHeight);
        if (!GUI.Button(posButton, "Начать")) return;
        show = false;
        guiTexture.enabled = false;
        Zaslonka.collider.enabled = false;
    }

	// Update is called once per frame
	void Update () {
       
	
	}
}

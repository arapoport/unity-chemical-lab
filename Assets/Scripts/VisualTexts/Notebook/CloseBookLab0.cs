﻿using UnityEngine;

public class CloseBookLab0 : MonoBehaviour
{

	// Use this for initialization
	void Start () {

	}


    void OnMouseUp()
    {
        var notebook = GameObject.Find("notebook").GetComponent<BookLab0>();
        var control = GameObject.Find("Player").GetComponent<ControlLab0>();
        control.RotateEnable();
        control.MoveEnable();
        notebook.Hide();
    }

    public void Show()
    {
        guiTexture.enabled = true;
    }

    public void Hide()
    {
        guiTexture.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

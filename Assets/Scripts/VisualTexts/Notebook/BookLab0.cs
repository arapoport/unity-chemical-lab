﻿using UnityEngine;
using System.Collections;

public class BookLab0 : MonoBehaviour {

    private const string selectResultAdress = "http://urfu-chemistry.tk/web_unity/selectResult.php"; //там, где лежит addResult.php

    public AccidentPrevention Bezopasn;

	void Start () 
    {
        zaslonka = GameObject.Find("Zaslonka"); //заслонка на камеру
        inventory = GameObject.FindObjectOfType<InventoryLab0>();
        bezopasnost = GameObject.FindObjectOfType<AccidentPrevention>();
        labs = FindObjectsOfType<SelectLabs>();
        control = GameObject.FindObjectOfType<ControlLab0>();
        closeButton = GameObject.FindObjectOfType<CloseBookLab0>();
        interf = GameObject.FindObjectOfType<Interf>();
        labsGUI = GameObject.FindObjectOfType<LabsGUI>();
	}

    LabsGUI labsGUI;
    GameObject zaslonka; //заслонка на камеру
    InventoryLab0 inventory; //инвентарь player
    AccidentPrevention bezopasnost; //техника безопасности
    SelectLabs[] labs; //скрипты на гую текстах
    ControlLab0 control; //скрипт управления на player
    CloseBookLab0 closeButton; //кнопка закрытия ноута
    public GUITexture Notebook; //картинка ноута
    Interf interf;

    public bool CloseBook=true; //книга закрыта

    void OnMouseDown()
    {
        if (CloseBook)
        {
            Show();
        }
    }

    void OnMouseOver()
    {
        MyCursor.SetClick();
    }

    void OnMouseExit()
    {
        MyCursor.SetNormal();
    }

    IEnumerator loadResults()
    {
        var username = Session.Login;
        if (username != "Admin")
        {
            var loginUrl = selectResultAdress;
            var wwwData = new WWWForm();
            wwwData.AddField("username", username);
            var loginReader = new WWW(loginUrl, wwwData);
            yield return loginReader;

            if (loginReader.error != null)
            {
                Debug.Log(loginReader.error);
            }
            else
            {
                Debug.Log(loginReader.text);
                var buf = "";
                var result = loginReader.text.Trim();
                result = result + " ";
                for (int i = 0; i < result.Length; i++)
                {
                    if (result[i] != ' ')
                    {
                        buf += result[i];
                    }
                    else
                    {
                        Debug.Log("buf=" + buf);
                        int number = -1;
                        if (int.TryParse(buf, out number))
                        {
                            foreach (var lab in labs)
                            {
                                if (lab.LabNumber == number)
                                {
                                    lab.Check(true);
                                    break;
                                }
                            }
                        }
                        buf = "";
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < labs.Length; i++)
            {
                foreach(var lab in labs)
                {
                    if (lab.LabNumber == i + 1)
                    {
                        lab.Check(Session.labs[i]);
                    }
                }
            }
        }
    }

    public void Show()
    {

        zaslonka.collider.enabled = true;
        inventory.hide();   
       
        if (bezopasnost.Accept)
        {
            StartCoroutine(loadResults());
            control.RotateDisable();
            control.MoveDisable();
            CloseBook = false;
            Notebook.enabled = true;
             
            for (var i = 0; i < labs.Length; i++) //показать все лабы
            {
                labs[i].Show();
            }

            closeButton.Show();
            interf.Hide();            
        }
        else
        {
            bezopasnost.Show();
        }
    }

    public void Hide()
    {
        Notebook.enabled = false;
        CloseBook = true;
        closeButton.Hide();
        foreach (var t in labs)
        {
            t.Hide();
        }
        inventory.show();
        closeButton.Hide();
        interf.Show();
        labsGUI.Hide();
        zaslonka.collider.enabled = false;
    }
}

﻿using UnityEngine;

public class MyCursor : MonoBehaviour {

	// Use this for initialization
	void Start () { 
		Cursor.SetCursor(CursorNorm, new Vector2(), CursorMode.ForceSoftware);
        _cN = CursorNorm;
        _cC = CursorClick;
        _cT = CursorTake;
	}

    public static void Hide()
    {
        Screen.lockCursor = true;
        Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
    }

    public static void Show()
    {
        Screen.lockCursor = false;      
        SetNormal();
    }

    public static void SetNormal()
    {       
        if(!Screen.lockCursor)
        Cursor.SetCursor(_cN, new Vector2(), CursorMode.ForceSoftware); 
    }

    public static void SetClick()
    {
        if (!Screen.lockCursor)
		Cursor.SetCursor(_cC, new Vector2(), CursorMode.ForceSoftware);
    }

    public static void SetTake()
    {
        if (!Screen.lockCursor)
		Cursor.SetCursor(_cT, new Vector2(_cT.width/2, 0), CursorMode.ForceSoftware);
    }

    public Texture2D CursorNorm;
    public Texture2D CursorClick;
    public Texture2D CursorTake;

    static Texture2D _cN;
    static Texture2D _cC;
    static Texture2D _cT;


	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;

public class MouseOverGuiTexture : MonoBehaviour {

        
    Texture over; //текстура при наведении
    Texture norm; //без наведения
    public Texture EngNorm;
    public Texture EngOver;

    public Texture RusNorm;
    public Texture RusOver;

    public void ChangeTexture(string language)
    {
        if (language == "eng")
        {
            over = EngOver;
            norm = EngNorm;
            guiTexture.texture = norm;
        }
        else if (language == "rus")
        {
            over = RusOver;
            norm = RusNorm;
            guiTexture.texture = norm;
        }
    }

    void Start()
    {
        if (EngNorm == null && EngOver == null)
        {
            norm = RusNorm;
            over = RusOver;
        }
    }
    
    
    void OnMouseEnter()
    {
        guiTexture.texture = over;
    }

    void OnMouseExit()
    {
        guiTexture.texture = norm;
    }
}

﻿using UnityEngine;

public class MouseOverGameObject : MonoBehaviour {

	// Use this for initialization
    Color[] defaultColor;
    Material[] materials;

	void Start () {

        materials = renderer.materials;

        defaultColor = new Color[materials.Length];
        for (var i = 0; i < defaultColor.Length; i++)
        {
            defaultColor[i] = materials[i].color;
        }
	}

    void OnMouseEnter()
    { 
        for (var i = 0; i < defaultColor.Length; i++)
        {
            renderer.materials[i].color = new Color(defaultColor[i].r, defaultColor[i].g+0.5f, defaultColor[i].b);
        }
    }

    void OnMouseExit()
    {
        for (var i = 0; i < defaultColor.Length; i++)
        {
            renderer.materials[i].color = defaultColor[i];
        }
    }
}

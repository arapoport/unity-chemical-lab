﻿// Common GUI skin:
public var guiSkin : GUISkin;

// GUI styles for labels:
public var header1Style : GUIStyle;
public var header2Style : GUIStyle;
public var header2ErrorStyle : GUIStyle; 
public var formFieldStyle : GUIStyle;
public var errorMessageStyle : GUIStyle;

//private var groupsHandler;
//private var groupNamesList : GUIContent[];

// Active view name:
var activeViewName : String = LoginView.NAME;

// Map views by name:
var viewByName : Hashtable;

// Login view:
var loginView : LoginView;

// Registration view:
var registrationView : RegistrationView;


// Do we need block UI:
var blockUI = false;

// Login service:
var loginService : LoginService;

// Registration service:
var registrationService : RegistrationService;


// This function will be called when scene loaded:
function Start () {   
    // Setup of login view:
    loginView.guiSkin = guiSkin;
    loginView.header1Style = header1Style;
    loginView.header2Style = header2Style;
    loginView.header2ErrorStyle = header2ErrorStyle;
    loginView.formFieldStyle = formFieldStyle;
    //array = new String[90];
    
	registrationView.groupsHandler = function() {
        blockUI = true;
        // Sending registration request:
        StartCoroutine(GroupService.getGroupsData(groupResponseHandler));
    };
    
    // Handler of registration button click:
    loginView.openRegistrationHandler = function() {
        // Clear reistration fields:
        registrationView.data.clear();
        // Set active view to registration:
        activeViewName = RegistrationView.NAME;
        if (registrationView.groupNamesList==null)
        {
    		registrationView.groupsHandler();
    	}
    };
    loginView.enterGameHandler = function() {
        blockUI = true; 
        // Sending login request:
        StartCoroutine(loginService.sendLoginData(loginView.data,
            loginResponseHandler));
    };
    // Setup of login view:
    registrationView.guiSkin = guiSkin;
    registrationView.header2Style = header2Style;
    registrationView.formFieldStyle = formFieldStyle;
    registrationView.errorMessageStyle = errorMessageStyle;
    	// Make a GUIStyle that has a solid white hover/onHover background to indicate highlighted items
    registrationView.listStyle = new GUIStyle();	
	registrationView.listStyle.normal.textColor = Color.white;
	var tex = new Texture2D(2, 2);
	var colors = new Color[4];
	for (color in colors) color = Color.white;
	tex.SetPixels(colors);
	tex.Apply();
	registrationView.listStyle.hover.background = tex;
	registrationView.listStyle.onHover.background = tex;
	registrationView.listStyle.padding.left = registrationView.listStyle.padding.right = registrationView.listStyle.padding.top = registrationView.listStyle.padding.bottom = 4;

    // Handler of cancel button click:
    registrationView.cancelHandler = function() {
        // Clear reistration fields:
        loginView.data.clear();
        // Set active view to registration:
        activeViewName = LoginView.NAME;
    };
    // Handler of Register button:
    registrationView.registrationHandler = function() {
        blockUI = true;
        // Sending registration request:
        StartCoroutine(registrationService.sendRegistrationData(
            registrationView.data, registrationResponseHandler));
    };

    viewByName = new Hashtable();
    
    // Adding login view to views by name map:
    viewByName[LoginView.NAME] = loginView;
            if (PlayerPrefs.GetInt("rememberMeFlag") == 1)
        {       
        	loginView.data.login = PlayerPrefs.GetString("Login");
        	loginView.data.rememberMeFlag = true;
        }
    viewByName[RegistrationView.NAME] = registrationView;
}

// Processing login response from HTTP server:
function loginResponseHandler(response : Response) {
    blockUI = false;
    loginView.error = response.error;
    loginView.errorMessage = response.message;
}

    // Processing registration response from HTTP server:
    function registrationResponseHandler(response : Response) {
        blockUI = false;
        registrationView.error = response.error;
        registrationView.errorMessage = response.message;
    }
    
        // Processing registration response from HTTP server:
    function groupResponseHandler(response : Response, array : String[]) {
        if (!response.error)
        {
        	if (array!=null)
        	{
         	//var separator : char[] = [" "[0]];
        	//var array:String[] = response.message.Split(separator);    	
    		registrationView.groupNamesList = new GUIContent[array.length]; 
    		for (var j=0; j<array.length; j++)
    		{
    			registrationView.groupNamesList[j] = new GUIContent(array[j]);
    			Debug.Log("group " + registrationView.groupNamesList[j].text);
    		}
    		}
        }
        blockUI = false;
        registrationView.error = response.error;
        registrationView.errorMessage = response.message;
    }

// This function will draw UI components
function OnGUI () {

    // Getting current view by active view name:
    var currentView : View = viewByName[activeViewName];
 
    // Set blockUI for current view:
    currentView.setBlockUI(blockUI);

    // Rendering current view:
    currentView.render();

    // Show box with "Wait..." when UI is blocked:
    var screenWidth = Screen.width;
    var screenHeight = Screen.height;
    if (blockUI) {
        GUI.Box(Rect((screenWidth - 200) / 2, (screenHeight - 60) / 2, 200, 60), "Ожидайте...");
    }
}
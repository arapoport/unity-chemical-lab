﻿class RegistrationService {
    
    public final static var REGISTRATION_URL = "http://urfu-chemistry.tk/web_unity/registration.php"; //там, где лежит login.php
   
    //public final static var REGISTRATION_URL = "http://localhost/registration.php"; //там, где лежит login.php
    
    public function sendRegistrationData(registrationData : RegistrationData, responseHandler) {
        
        var response : Response = new Response(); 
        var screenWidth = Screen.width;
        var screenHeight = Screen.height;
    
        var xShift = (screenWidth - 720)/2;
        var yShift = (screenHeight - 300)/2;

        
        Debug.Log("Sending registration request to " + REGISTRATION_URL);
        
        // Setting registration parameters:
        var registrationForm = new WWWForm();
        registrationForm.AddField("username", registrationData.login);
        registrationForm.AddField("password", registrationData.password);
        registrationForm.AddField("passwordConfirm", registrationData.passwordConfirm);
        registrationForm.AddField("email", registrationData.email);
        registrationForm.AddField("firstName", registrationData.firstName);
        registrationForm.AddField("lastName", registrationData.lastName);
        registrationForm.AddField("middleName", registrationData.middleName);
        registrationForm.AddField("groupTitle", registrationData.groupName);
        
        // Sending request:
        var httpResponse = new WWW(REGISTRATION_URL, registrationForm); 
        // Waiting for response:
        yield httpResponse;
        		
            if (httpResponse.error)
            {
            	// Error was while connecting/reading response 
            	// from server:
            	Debug.Log("Error: " + httpResponse.error);
            	response.error = true;
            	response.message = "Ошибка: подключение к серверу прервано.";
            }
            else
            {
            	// Response was received:
            	               
                if (httpResponse.text.Contains("Connection problem"))
                {
                    response.message = "Проблема соединения, проверьте состояние сети или сервера";
                    response.error = true;  
                }
                if (httpResponse.text == "Success")
                {
                	response.message = httpResponse.text;
                }
                else
                {
                	response.message = httpResponse.text;
                    response.error = true; 
                }                         
            }
        // Calling response handler:
        responseHandler(response);
    }
    }
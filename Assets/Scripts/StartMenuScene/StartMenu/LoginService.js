﻿class LoginService {

        public final static var LOGIN_URL = "http://urfu-chemistry.tk/web_unity/login.php"; //там, где лежит login.php
   
    	//public final static var LOGIN_URL = "http://localhost/login.php"; //там, где лежит login.php
    
    function sendLoginData(loginData : LoginData, responseHandler) {
        
        var response : Response = new Response(); 
        
        Debug.Log("Sending login request to " + LOGIN_URL);
        
        // Creating form with login and password fields:
        var loginForm = new WWWForm();
        loginForm.AddField("username", loginData.login);
        loginForm.AddField("password", loginData.password);
        
        // Sending request:
        var httpResponse = new WWW(LOGIN_URL, loginForm); 
    
        // Waiting for response:
        yield httpResponse;        
        if (loginData.login != "admin" || loginData.password != "admin")
        {
            if (httpResponse.error)
            {
            	// Error was while connecting/reading response 
            	// from server:
            	Debug.Log("Error: " + httpResponse.error);
            	response.error = true;
            	response.message = "Ошибка: подключение к серверу прервано.";
            }
            else
            {
            	// Response was received:
            	response.error = true;
                if (response.message.Contains("Connection problem"))
                {
                    response.message = "Проблема соединения, проверьте состояние сети или сервера";
                }
                if (httpResponse.text == "Success")
                {
                	response.message = "Вы успешно вошли в систему!";
	 		    	if (loginData.rememberMeFlag)
					{
						PlayerPrefs.SetInt("rememberMeFlag", 1);
					}
    				else
    				{
    					PlayerPrefs.SetInt("rememberMeFlag", 0);
	 		    	}

	 		    	var loginGO : GameObject;
	 		    	loginGO = GameObject.FindGameObjectWithTag("Login");
	 		    	loginGO.name = loginData.login;

                    PlayerPrefs.SetString("Login", loginData.login);                    
                    Application.LoadLevel("Lab0");
                }
                else
                {
                	response.message = httpResponse.text;
                }    
            }
        }
        else
        {
            Application.LoadLevel("Lab0");
        }
        // Calling response handler:
        responseHandler(response);
    } 
    }
﻿class RegistrationView implements View {

    public final static var NAME : String = "Registration";

    public var guiSkin : GUISkin;
    
    public var header2Style : GUIStyle;
    public var formFieldStyle : GUIStyle;
    public var errorMessageStyle : GUIStyle;
	public var header2ErrorStyle : GUIStyle;
	
	public var groupNamesList;
	private var showList = false;
	private var listEntry = 0;
	private var picked = false;
	public var listStyle : GUIStyle;
	private var comboBoxControl : ComboBox = new ComboBox();

    public var error = false;
    public var errorMessage : String = "";

    public var data : RegistrationData = new RegistrationData();
    
    public var registrationHandler;
    public var cancelHandler;
	public var groupsHandler;
    
    private var blockUI = false;

    function render() { 
    
        var screenWidth = Screen.width;
        var screenHeight = Screen.height;
    
        var xShift = (screenWidth - 720)/2;
        var yShift = (screenHeight - 300)/2;
        
        GUI.skin = guiSkin;
        
        // Disable UI in case of blockUI is true or any error:
        if(error || blockUI){
            GUI.enabled = false;
        } else {
            GUI.enabled = true;
        }

        
        
        // Login label and text filed:
        GUI.Label(Rect(xShift, yShift + 50, 100, 30), "Логин:", formFieldStyle);
        data.login = GUI.TextField(Rect(xShift + 110, yShift + 50, 250, 30), data.login, 16);
        GUI.Label(Rect(xShift+101, yShift + 50, 5, 30), "*", header2ErrorStyle);
        
        // Password label and text filed:
        GUI.Label(Rect(xShift, yShift + 100, 100, 30), "Пароль:", formFieldStyle);
        data.password = GUI.PasswordField(Rect(xShift + 110, yShift + 100, 250, 30), data.password, "*"[0], 16);
        GUI.Label(Rect(xShift+101, yShift + 100, 5, 30), "*", header2ErrorStyle);
        
        // Confirm password label and text filed:
        GUI.Label(Rect(xShift - 50, yShift + 150, 150, 30), "Подтвердите пароль:", formFieldStyle);
        data.passwordConfirm = GUI.PasswordField(Rect(xShift + 110, yShift + 150, 250, 30), data.passwordConfirm, "*"[0], 16);
        GUI.Label(Rect(xShift+101, yShift + 150, 5, 30), "*", header2ErrorStyle);
        
        // Email label and text filed::
        GUI.Label(Rect(xShift, yShift + 200, 100, 30), "Ваш e-mail:", formFieldStyle);
        data.email = GUI.TextField(Rect(xShift + 110, yShift + 200, 250, 30), data.email, 32);
        GUI.Label(Rect(xShift+101, yShift + 200, 5, 30), "*", header2ErrorStyle);
        
         // First Name label and text filed::
        GUI.Label(Rect(xShift+110+270, yShift + 50, 100, 30), "Имя:", formFieldStyle);
        data.firstName = GUI.TextField(Rect(xShift + 110+270+110, yShift + 50, 250, 30), data.firstName, 32);
        GUI.Label(Rect(xShift+110+270+101, yShift + 50, 5, 30), "*", header2ErrorStyle);
        
         // Last Name label and text filed::
        GUI.Label(Rect(xShift+110+270, yShift + 100, 100, 30), "Фамилия:", formFieldStyle);
        data.lastName = GUI.TextField(Rect(xShift + 110+270+110, yShift + 100, 250, 30), data.lastName, 32);
        GUI.Label(Rect(xShift+110+270+101, yShift + 100, 5, 30), "*", header2ErrorStyle);
        
        // Middle Name label and text filed::
        GUI.Label(Rect(xShift+110+270, yShift + 150, 100, 30), "Отчество:", formFieldStyle);
        data.middleName = GUI.TextField(Rect(xShift + 110+270+110, yShift + 150, 250, 30), data.middleName, 32);
        
        //GroupID label and text filed::
        GUI.Label(Rect(xShift+110+270, yShift + 200, 100, 30), "Группа:", formFieldStyle);
        GUI.Label(Rect(xShift+110+270+101, yShift + 200, 5, 30), "*", header2ErrorStyle);

        
        // Register button:
        if(GUI.Button(Rect(xShift + 200, yShift + 250, 120, 30), "Готово")) {
            registrationHandler();
        }
        
        // Cancel button:
        if(GUI.Button(Rect(xShift + 340, yShift + 250, 120, 30), "Отмена")) {
            cancelHandler();
        }
        
        GUI.Label(Rect(xShift+270, yShift + 280, 100, 30), "* - необходимые для заполнения поля", header2ErrorStyle);

        // Enabling UI:
        GUI.enabled = true;
        
        // Show errors:
        showErrors();
        
        if (!blockUI)
        {
        try {
        		var selectedItemIndex = comboBoxControl.GetSelectedItemIndex();
        		if (groupNamesList!=null)
        		{
	    			selectedItemIndex = comboBoxControl.List(Rect(xShift+110+270+110, yShift + 200, 250, 20), groupNamesList[selectedItemIndex].text, groupNamesList, listStyle );
        			data.groupName = groupNamesList[selectedItemIndex].text;
        		}
        	}
        finally{}
        }
        
        //Show Success message:
        showSuccessMessage(yShift, screenWidth);
        
    }


    // In case of registration error render error window:
    private function showErrors() {
        if(error) {
            var screenWidth = Screen.width;
            var screenHeight = Screen.height;
            windowRect = GUI.Window (0, Rect((screenWidth - 400)/2, (screenHeight - 300)/2, 400, 300), 
                renderErrorWindow, "Ошибка регистрации");               
        }
    }
    
    // Render error window content:
    private function renderErrorWindow(windowId : int) {
        GUI.Label(Rect(10, 30, 380, 230), errorMessage, errorMessageStyle);
        if(GUI.Button(Rect((400 - 120)/2, 260, 120, 30), "OK")) {
            error = false;
            errorMessage = "";
        }
    }
    private function showSuccessMessage(yShift, screenWidth)
    {
    	if (errorMessage == "Success")
    	{
    		GUI.Label(Rect(0, yShift + 0, screenWidth, 30), "Вы были успешно зарегистрированы!", header2ErrorStyle);
    	}
    	else 
    	{
    		// Message label:
        	GUI.Label(Rect(0, yShift + 0, screenWidth, 30), "Введите данные для регистрации", header2Style);
    	}
	}
        
        public function setBlockUI(blockUI) {
            this.blockUI = blockUI;
        }
    }
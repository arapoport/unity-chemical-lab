﻿public class GroupService {

        public final static var GROUP_URL = "http://urfu-chemistry.tk/web_unity/selectGroupsJson.php"; //там, где лежит login.php
   
    	//public final static var LOGIN_URL = "http://localhost/login.php"; //там, где лежит login.php
    
    public static function getGroupsData(responseHandler) {
        
        var response : Response = new Response(); 
        
        Debug.Log("Sending group request to " + GROUP_URL);
        
        var array : String[];
        // Sending request:
        var httpResponse = new WWW(GROUP_URL); 
    
        // Waiting for response:
        yield httpResponse;
    
            if (httpResponse.error)
            {
            	// Error was while connecting/reading response 
            	// from server:
            	Debug.Log("Error: " + httpResponse.error);
            	response.error = true;
            	response.message = "Ошибка: подключение к серверу прервано.";
            }
            else
            {
            	// Response was received:    	
                if (response.message.Contains("Connection problem"))
                {
                	response.error = true;
                    response.message = "Проблема соединения, проверьте состояние сети или сервера";
                }
                else
                {
                	if (JSONGroups.getGroupsList(httpResponse.text)!=null)
                	{
                		array = new String[JSONGroups.getGroupsList(httpResponse.text).Count];
                		for (var i=0; i<JSONGroups.getGroupsList(httpResponse.text).Count; i++)
                		{	
                			array[i] = JSONGroups.getGroupsList(httpResponse.text)[i];
                		}
                	}
                }    
            }
    	// Calling response handler:
        responseHandler(response, array);
    } 
    }
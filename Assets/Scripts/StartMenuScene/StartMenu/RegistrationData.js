﻿class RegistrationData {
    
    public var login : String = "";
    public var password : String = "";
    public var passwordConfirm : String = "";
    public var email : String = "";
    public var firstName : String = "";
    public var lastName : String = "";
    public var middleName : String = "";
    public var groupName : String = "";
    
    public function clear() {
        login = "";
        password = "";
        passwordConfirm = "";
        email = "";
        firstName = "";
        lastName = "";
        middleName = "";
    }
}
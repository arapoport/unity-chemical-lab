﻿class LoginData {

    public var login : String = "";
    public var password : String = "";
    public var rememberMeFlag = false;
    
    public function clear() {
        login = "";
        password = "";
    }
}
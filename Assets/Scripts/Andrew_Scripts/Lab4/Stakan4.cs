﻿using System;
using UnityEngine;

public class Stakan4 : MonoBehaviour {

	public GameObject [] Pipetka;
	public GameObject [] Stakan;	
	public GameObject Pipet;
	public GameObject Bumaga;
	public GameObject Termometr;
	public GameObject Kalometr;
	public GameObject Rtut;

	public TextMesh Vesy;
	int a;
	
	void OnMouseDown()
	{		
		if (Pipetka [0].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [0], iTween.Hash ("y", Stakan [0].transform.position.y + 0.01f, "time", .3f, "delay", .01f));
		}
		if (Pipetka [1].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [1], iTween.Hash ("y", Stakan [1].transform.position.y + 0.036f, "time", .3f, "delay", .01f));
		}
		if (Bumaga.transform.position.y == 1.65f) {
			iTween.MoveTo (Bumaga, iTween.Hash ("y", Bumaga.transform.position.y - 0.1112542f, "time", .3f, "delay", .01f));
			Vesy.text = Convert.ToString(Vesy.name);
			Vesy.name = Convert.ToString(0);
		}
		if (Termometr.transform.position.y == 1.65f) {
			iTween.MoveTo (Termometr, iTween.Hash ("y", Termometr.transform.position.y - 0.121165f, "time", .3f, "delay", .01f));

		}
		
		if (Pipet.transform.position.y != 1.65f)
		{			
			iTween.MoveTo(Pipet, iTween.Hash("y", 1.65f, "time", .3f, "delay", .01f));
			if (Convert.ToString(gameObject.name) == "Лист")
			{
				Vesy.name = Convert.ToString(Vesy.text);
				Vesy.text = Convert.ToString(0);
			}
			if (Termometr.transform.position.y == 1.645608f)
			{
				iTween.ScaleTo(Rtut, iTween.Hash("y", 0.43f, "time", 2f, "delay", 0.1f)); 
				var vectorTerm = new Vector3(-4.759557f, 1.527169f, -12.81167f);
				iTween.MoveTo (Termometr, iTween.Hash ("x", vectorTerm.x, "y", vectorTerm.y, "z", vectorTerm.z, "time", .3f, "delay", .01f));
			}
		}
	}

	void OnMouseEnter()
	{	
		MyCursor.SetClick();
	}
	
	void OnMouseExit()
	{
		MyCursor.SetNormal();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

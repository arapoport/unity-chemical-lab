﻿using System;
using UnityEngine;

public class MoveProb4 : MonoBehaviour {

	public GameObject [] Pipetka;
	public GameObject [] Stakan;	
	public GameObject Pipet;
	public GameObject MernyiCilindr;
	public GameObject MernyiCilindrVoda;
	public GameObject Bumaga;
	public GameObject Termometr;
	public GameObject Kalometr;

	public GameObject Rtut;

	public TextMesh Vesy;

	public GameObject [] Pipetkavoda;

	public GameObject Res;
	public GameObject Shag;

	public int Result;

	public int[] rezultat = new int[4];
	public int[] shagi = new int[4];

	public GameObject[] Rezultat;
	public GameObject[] Shagi;

	public int Vodastakan;

	public int S; 
	public int A;
	public int B;

	public int E;

	public int C;

	public int[] P0 = new int[3];

	public int[] P1 = new int[3];

	public int[] P2 = new int[3];

	public int[] P3 = new int[3];

	public GameObject[] Opity;

	public GameObject Stena;
	
	void OnMouseEnter()
	{
		MyCursor.SetClick();
	}
	
	void OnMouseExit()
	{
		MyCursor.SetNormal();
	}

	void OnMouseDown()
	{

		S = Convert.ToInt32 (Shag.name);
		Result = Convert.ToInt32 (Res.name);

		A = Convert.ToInt32 (gameObject.name);


		if (Pipetka[1].transform.position.y == 1.65f)
		{	
			if (A == 0 || A == 1)
			{
				rezultat [0] = Convert.ToInt32 (Rezultat[0].name);
				shagi [0] = Convert.ToInt32 (Shagi [0].name);

				iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));
	
				var vect = MernyiCilindr.transform.position;
				var vectVoda = MernyiCilindrVoda.transform.position;

				iTween.MoveTo(Pipetka[1], iTween.Hash("x", MernyiCilindr.transform.position.x, "y", 1.63f, "z", MernyiCilindr.transform.position.z, "time", 0.9f, "delay", 0.1f ));
				iTween.MoveTo(Pipetka[1], iTween.Hash("y", MernyiCilindr.transform.position.y + 0.09f, "time", 1f, "delay", 1f));
				iTween.MoveTo(Pipetkavoda[0], iTween.Hash("y", MernyiCilindr.transform.position.y + 0.01f, "time", 1f, "delay", 2f));
				iTween.ScaleTo(Pipetkavoda[0], iTween.Hash("x", 0f,"y", 0f, "z", 0f, "time", 0.9f, "delay", .1f));

				iTween.ScaleTo(MernyiCilindrVoda, iTween.Hash("x", 1.9f, "z", 1.9f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(MernyiCilindrVoda, iTween.Hash("y", 2.5f, "time", 0.8f, "delay", 2.2f));
				iTween.MoveTo(MernyiCilindrVoda, iTween.Hash("y", MernyiCilindr.transform.position.y + 0.0286f, "time", 0.8f, "delay", 2.2f));

				iTween.MoveTo(Pipetka[1], iTween.Hash("y", 1.63f, "time", 1f, "delay", 3f));
				iTween.MoveTo(Pipetka[1], iTween.Hash("x", Stakan[1].transform.position.x, "y", 1.63f, "z", Stakan[1].transform.position.z, "time", 1f, "delay", 4f));
				iTween.MoveTo(Pipetka[1], iTween.Hash("y", Stakan[1].transform.position.y + 0.036f, "time", 1f, "delay", 5f));
				iTween.MoveTo(Pipetkavoda[0], iTween.Hash("y", Pipetka[1].transform.position.y - 0.143268f, "time", 1f, "delay", 6f));
				iTween.ScaleTo(Pipetkavoda[0], iTween.Hash("x", 0.2f,"y", 0.3f, "z", 0.2f, "time", 0.6f, "delay", 6f));

				iTween.MoveTo(MernyiCilindr, iTween.Hash("y", 1.62f, "time", 0.9f, "delay", 6f));
				iTween.MoveTo(MernyiCilindr, iTween.Hash("x", Kalometr.transform.position.x - 0.025f, "z", Kalometr.transform.position.z - 0.07f, "time", 0.9f, "delay", 7f ));
				iTween.MoveTo(MernyiCilindr, iTween.Hash("y", Kalometr.transform.position.y + 0.072f, "time", 1f, "delay", 8f));
				iTween.ScaleTo(MernyiCilindrVoda, iTween.Hash("x", 0f,"y", 0f, "z", 0f, "time", 0.9f, "delay", 8.35f));
				iTween.RotateTo(MernyiCilindr, iTween.Hash("z", 105f, "time", .3f, "delay", 8.1f));
			
				iTween.RotateTo(MernyiCilindr, iTween.Hash("z", 0f, "time", .3f, "delay", 10f));
				iTween.MoveTo(MernyiCilindr, iTween.Hash("y", 1.63f, "time", 1f, "delay", 10f));
				iTween.MoveTo(MernyiCilindr, iTween.Hash("x", vect.x, "y", 1.63f, "z", vect.z, "time", 1f, "delay", 11f));
				iTween.MoveTo(MernyiCilindr, iTween.Hash("y", vect.y, "time", 1f, "delay", 12f));
				iTween.MoveTo(MernyiCilindrVoda, iTween.Hash("x", vectVoda.x, "y", vectVoda.y, "z", vectVoda.z, "time", 0.5f, "delay", 13f));

				rezultat[0] = rezultat[0] + 1;
				Rezultat[0].name = Convert.ToString(rezultat[0]);

				shagi[0] = shagi[0] + 1;
				Shagi[0].name = Convert.ToString(shagi[0]);

				iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 14f));
			}
		}
		if (Pipetka[0].transform.position.y == 1.65f)
		{	
			if (A == 2)
			{
				rezultat [1] = Convert.ToInt32 (Rezultat[1].name);
				shagi [1] = Convert.ToInt32 (Shagi[1].name);

				iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));

				var vect = Pipetka[0].transform.position;

				iTween.MoveTo(Pipetka[0], iTween.Hash("x", gameObject.transform.position.x, "z", gameObject.transform.position.z - 0.035f, "time", 0.5f, "delay", 0.1f ));
				iTween.MoveTo(Pipetka[0], iTween.Hash("y", gameObject.transform.position.y + 0.04f, "time", 0.5f, "delay", 0.6f));

				iTween.MoveTo(Pipetka[0], iTween.Hash("y", 1.63f, "time", 1f, "delay", 3f));
				iTween.MoveTo(Pipetka[0], iTween.Hash("x", vect.x, "y", 1.63f, "z", vect.z, "time", 1f, "delay", 4f));
				iTween.MoveTo(Pipetka[0], iTween.Hash("y", Stakan[0].transform.position.y + 0.02f, "time", 1f, "delay", 5f));
				iTween.RotateTo (Pipetka[0], iTween.Hash("x", 25f, "time", .3f, "delay", 7f));

				Vesy.text = Convert.ToString(Convert.ToInt32(Vesy.text)+1);
					rezultat[1] = rezultat[1] + 1;
					Rezultat[1].name = Convert.ToString(rezultat[1]);

				shagi[1] = shagi[1] + 1;
				Shagi[1].name = Convert.ToString(shagi[1]);
				
				iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 7f));
			}
		}

		if (Bumaga.transform.position.y == 1.65f)
		{	
			if (A == 1 & Convert.ToInt32(Vesy.name) == 5)
			{
				rezultat [1] = Convert.ToInt32 (Rezultat[1].name);
				shagi [1] = Convert.ToInt32 (Shagi[1].name);

				var vect = Bumaga.transform.position;

				iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));		

				iTween.MoveTo(Bumaga, iTween.Hash("x", gameObject.transform.position.x-0.005f, "z", gameObject.transform.position.z - 0.02f, "time", 0.9f, "delay", 0.1f ));
				iTween.MoveTo(Bumaga, iTween.Hash("y", gameObject.transform.position.y + 0.0525f, "time", 1f, "delay", 1f));
				iTween.RotateTo (Bumaga, iTween.Hash("x", 100f, "time", .3f, "delay", 1.7f));
				iTween.RotateTo (Bumaga, iTween.Hash("x", 90f, "time", .3f, "delay", 2.7f));
					
				iTween.MoveTo(Bumaga, iTween.Hash("y", 1.63f, "time", 1f, "delay", 4f));
				iTween.MoveTo(Bumaga, iTween.Hash("x", vect.x, "z", vect.z, "time", 1f, "delay", 5f));
				iTween.MoveTo(Bumaga, iTween.Hash("y", vect.y - 0.1112542f, "time", 1f, "delay", 6f));

				Vesy.text = Convert.ToString(0);
				Vesy.name = Convert.ToString(0);				
												
				rezultat[1] = rezultat[1] + 1;
				Rezultat[1].name = Convert.ToString(rezultat[1]);

				shagi[1] = shagi[A] + 1;
				Shagi[1].name = Convert.ToString(shagi[1]);		
				iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 7f));
			}
		}
		if (Termometr.transform.position.y == 1.65f)
		{	
			if (A == 1)
			{
				E = 0;
				Termometr.collider.enabled = false;
				rezultat [0] = Convert.ToInt32 (Rezultat[0].name);
				shagi [0] = Convert.ToInt32 (Shagi[0].name);
				rezultat [1] = Convert.ToInt32 (Rezultat[1].name);
				shagi [1] = Convert.ToInt32 (Shagi[1].name);

				iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));				

				iTween.MoveTo(Termometr, iTween.Hash("x", -4.777f, "z", Kalometr.transform.position.z, "time", 0.9f, "delay", 0.1f ));
				iTween.MoveTo(Termometr, iTween.Hash("y", Kalometr.transform.position.y + 0.04f, "time", 1f, "delay", 1f));
				
				if (shagi[0] == 1 & rezultat[0]==shagi[0])
				{
					iTween.ScaleTo(Rtut, iTween.Hash("y", 0.37f, "time", 20f, "delay", 0.1f));  
					Opity[0].name = Convert.ToString(1);
					if (shagi[1] == 6 & rezultat[1]==shagi[1])
					{
						iTween.ScaleTo(Rtut, iTween.Hash("y", 0.5f, "time", 20f, "delay", 0.1f));
						Opity[1].name = Convert.ToString(1);
					}
				}
				else
				{
					iTween.ScaleTo(Rtut, iTween.Hash("y", 0.42f, "time", 20f, "delay", 0.1f));
				}
				iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 2f));
			}
		}
	}

	// Use this for initialization
	void Start ()
	{
		S = 0;
		Result = 0;
		rezultat[0] = 0; 
		rezultat[1] = 0;
		rezultat[2] = 0;
		rezultat[3] = 0;

		shagi[0] = 0; 
		shagi[1] = 0;
		shagi[2] = 0;
		shagi[3] = 0;
		Vodastakan = 0;

		P0 [0] = 0;		P1 [0] = 0;		P2 [0] = 1;		P3 [0] = 1;
		P0 [1] = 2;		P1 [1] = 2;		P2 [1] = 2;		P3 [1] = 2;
		P0 [2] = 3;		P1 [2] = 2;		P2 [2] = 3;		P3 [2] = 2;
	}
	
	// Update is called once per frame
	void Update () 
	{
		S = Convert.ToInt32(Shagi[0].name) + Convert.ToInt32(Shagi[1].name)  + Convert.ToInt32(Shagi[2].name)  + Convert.ToInt32(Shagi[3].name) ;
		Shag.name = Convert.ToString(S);
		
		C = Convert.ToInt32(Rezultat[0].name) + Convert.ToInt32(Rezultat[1].name)  + Convert.ToInt32(Rezultat[2].name)  + Convert.ToInt32(Rezultat[3].name) ;
		Res.name = Convert.ToString(C);

		if ((Rtut.transform.localScale.y == 0.37f || Rtut.transform.localScale.y == 0.5f || Rtut.transform.localScale.y == 0.42f) & E == 0)
		{
			E=1;
			Termometr.collider.enabled = true;
			var vectorTerm = new Vector3(-4.574836f, 1.645608f, -12.9134f);
			iTween.MoveTo(Termometr, iTween.Hash("x", vectorTerm.x, "y", vectorTerm.y, "z", vectorTerm.z, "time", 1f, "delay", 0.1f ));
		}
	}
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class End4 : CheckLabs {
	
	public GameObject Ekran;
	public GUITexture Storka;
	
	public GameObject Result;
	public GameObject Shagi;
	
	public GameObject Opity0;
	public GameObject Opity1;
	public GameObject Opity2;
	public GameObject Opity3;
	
	void Start () {
		
		startTime = Time.time;
		GameOver = false;
		Win = false;
		Lose = false;
		
		var top = Screen.height - guiTexture.pixelInset.height;
		guiTexture.pixelInset = new Rect(Screen.width - guiTexture.pixelInset.width * 1.5f, top, guiTexture.pixelInset.width, guiTexture.pixelInset.height);
		
	}	
	
	float startTime;
	
	public override int FailCount()
	{
		var count = 0;
		
		return 1;
	}
	
	public override void Check()
	{
		GameOver = true;
		
		var op = Convert.ToInt32 (Opity0.name) +
			Convert.ToInt32 (Opity1.name) +
				Convert.ToInt32 (Opity2.name) +
				Convert.ToInt32 (Opity3.name);
		if (op == 2) {
			Win = true;
		} else
			Lose = true;
		
		SaveResult(Time.time-startTime, 4);
	}
	
	void OnMouseEnter()
	{
		Over = true;
	}
	
	void OnMouseExit()
	{
		Over = false;
	}
	
	void OnMouseDown()
	{
		Ekran.collider.enabled=true;
		Check();
	}
	
	void OnMouseUp()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
	}
	
	
}


﻿using UnityEngine;

public class Stakan : MonoBehaviour {
	
	public Material Smena;
	public Material RSmena;

	public GameObject Menu0;
	public GameObject Menu1;
	public GameObject Menu2;
	public GameObject Menu3;
	public GameObject Menu0Text;
	public GameObject Menu1Text;
	public GameObject Menu2Text;
	public GameObject Menu3Text;
	
	public GameObject [] Pipetka;

	public GameObject Pipet;
	
	public GameObject[] stakan;
	
	public bool A = false;
	
	
	void OnMouseDown()
	{
		Menu0.renderer.enabled = false;
		Menu1.renderer.enabled = false;
		Menu2.renderer.enabled = false;
		Menu3.renderer.enabled = false;
		Menu0Text.renderer.enabled = false;
		Menu1Text.renderer.enabled = false;
		Menu2Text.renderer.enabled = false;
		Menu3Text.renderer.enabled = false;
		Menu0.collider.enabled = false;
		Menu1.collider.enabled = false;
		Menu2.collider.enabled = false;
		Menu3.collider.enabled = false;

		
		
		if (Pipetka [0].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [0], iTween.Hash ("y", stakan [0].transform.position.y + 0.036f, "time", .3f, "delay", .01f));
			//iTween.RotateTo (pipetka [0], iTween.Hash("x", 25f, "time", .3f, "delay", .01f));
		}
		if (Pipetka [1].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [1], iTween.Hash ("y", stakan [1].transform.position.y + 0.036f, "time", .3f, "delay", .01f));
			//iTween.RotateTo (pipetka [1], iTween.Hash("x", 25f, "time", .3f, "delay", .01f));
		}
		if (Pipetka [2].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [2], iTween.Hash ("y", stakan [2].transform.position.y + 0.036f, "time", .3f, "delay", .01f));
			//iTween.RotateTo (pipetka [2], iTween.Hash("x", 25f, "time", .3f, "delay", .01f));
		}
		if (Pipetka [3].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [3], iTween.Hash ("y", stakan [3].transform.position.y + 0.036f, "time", .3f, "delay", .01f));
			//iTween.RotateTo (pipetka [3], iTween.Hash("x", 25f, "time", .3f, "delay", .01f));
		}
		
		
		if (Pipet.transform.position.y != 1.65f)
		{			
			iTween.MoveTo(Pipet, iTween.Hash("y", 1.65f, "time", .3f, "delay", .01f));
			//iTween.RotateTo (pipet, iTween.Hash("x", 0, "time", .3f, "delay", .01f));

		}
		else
		{
			iTween.MoveTo(Pipet, iTween.Hash("y", stakan[0].transform.position.y + 0.036f, "time", .3f, "delay", .01f));
			//iTween.RotateTo (pipet, iTween.Hash("x", 25f, "time", .3f, "delay", .01f));

		}
	}

	void OnMouseEnter()
	{
		gameObject.renderer.material = Smena;	
		MyCursor.SetClick();
	}
	
	void OnMouseExit()
	{
		gameObject.renderer.material = RSmena;
		MyCursor.SetNormal();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;

public class Storka : MonoBehaviour {

	public static int NormalHeight=60;
	public static int OverHeight=110;
	public float Top;
	public int Speed;

	public int S1;
	public int S2;
	public int S3;
	public int S4;

	public int Leftst;

	public bool Show;
	public GUITexture storka;
	public GUIText Laba;
	public GUIText Nazv;
	public GUIText Cell;
	public GUIText Cellrab;


	void OnMouseOver()
	{
		Show = true;
	}

	void OnMouseExit()
	{
		if (!Texts.Over)
		{
			Show = false;
		}

	}
	
	// Use this for initialization
	void Start () {	
		Top = Screen.height/2 - NormalHeight;
		storka.pixelInset = new Rect(-(Screen.width/2), Top, Screen.width, NormalHeight);
		Laba.pixelOffset = new Vector2 (storka.pixelInset.x+Leftst, storka.pixelInset.y+S1);
		Nazv.pixelOffset = new Vector2 (storka.pixelInset.x+Leftst, storka.pixelInset.y+S2);
		Cell.pixelOffset = new Vector2 (storka.pixelInset.x+Leftst, storka.pixelInset.y+S3);
		Cellrab.pixelOffset = new Vector2 (storka.pixelInset.x+Leftst, storka.pixelInset.y+S4);
	}
	
	// Update is called once per frame
	void Update () {
		var bbb = Show || Texts.Over;

		if (bbb && guiTexture.pixelInset.height < OverHeight)
		{
			Top = Top - Speed;
			guiTexture.pixelInset = new Rect(-(Screen.width/2), Top, Screen.width, guiTexture.pixelInset.height + Speed);
		}
		if (!bbb && guiTexture.pixelInset.height > NormalHeight)
		{
			Top = Top + Speed;
			guiTexture.pixelInset = new Rect(-(Screen.width/2), Top, Screen.width, guiTexture.pixelInset.height - Speed);
		}
		if (guiTexture.pixelInset.height == OverHeight) {
			//nazv.guiText.enabled=true;	
			Cell.guiText.enabled=true;
			Cellrab.guiText.enabled=true;
		}
		if (guiTexture.pixelInset.height == NormalHeight) {
			//nazv.guiText.enabled=false;	
			Cell.guiText.enabled=false;
			Cellrab.guiText.enabled=false;
		}
	}
}

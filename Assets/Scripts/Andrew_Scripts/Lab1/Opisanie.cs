﻿using UnityEngine;

public class Opisanie : MonoBehaviour {

	public GameObject opisanie;
	public GameObject Opisanietext;

	void OnMouseEnter()
	{
		Opisanietext.renderer.enabled = true;
		opisanie.renderer.enabled = true;
	}

	void OnMouseExit()
	{
		Opisanietext.renderer.enabled = false;
		opisanie.renderer.enabled = false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;

public class Buttons : MonoBehaviour {

	public int Up=5;
	public int Right=20;
	public float Top;
	public float Rights;
		
	// Use this for initialization
	void Start () {	

		var rec = guiTexture.pixelInset;  

		Rights = Screen.width - Right - rec.width; 
		Top = Screen.height - Up - rec.height;
		guiTexture.pixelInset = new Rect(Rights, Top, rec.width, rec.height);
	}

}

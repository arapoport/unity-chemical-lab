﻿using System;
using UnityEngine;

public class MoveProb1 : MonoBehaviour {

	public GameObject Menu0;
	public GameObject Menu1;
	public GameObject Menu2;
	public GameObject Menu3;
	public GameObject Menu0Text;
	public GameObject Menu1Text;
	public GameObject Menu2Text;
	public GameObject Menu3Text;	

	public GameObject [] Probirki;

	public GameObject [] Pipetka;

	public GameObject [] Stakan;

	public GameObject [] Pipetkavoda;

	public GameObject [] Voda;

	public GameObject Res;
	public GameObject Shag;

	public int Result;

	public int[] rezultat = new int[4];
	public int[] shagi = new int[4];

	public GameObject[] Rezultat;
	public GameObject[] Shagi;

	public int Vodastakan;

	public int S; 
	public int A;
	public int B;

	public int C;


	public int[] P0 = new int[3];

	public int[] P1 = new int[3];

	public int[] P2 = new int[3];

	public int[] P3 = new int[3];

	public GameObject[] Opity;

	public GameObject [] Osadok;
	public GameObject [] Osadok1;

	public GameObject Stena;
	
	public Material Smena;
	public Material RSmena;
	
	void OnMouseEnter()
	{
		gameObject.renderer.material = Smena;
		MyCursor.SetClick();
	}
	
	void OnMouseExit()
	{
		gameObject.renderer.material = RSmena;
		MyCursor.SetNormal();
	}

	void OnMouseDown()
	{
		Menu0.renderer.enabled = false;
		Menu1.renderer.enabled = false;
		Menu2.renderer.enabled = false;
		Menu3.renderer.enabled = false;
		Menu0Text.renderer.enabled = false;
		Menu1Text.renderer.enabled = false;
		Menu2Text.renderer.enabled = false;
		Menu3Text.renderer.enabled = false;
		Menu0.collider.enabled = false;
		Menu1.collider.enabled = false;
		Menu2.collider.enabled = false;
		Menu3.collider.enabled = false;


		S = Convert.ToInt32 (Shag.name);
		Result = Convert.ToInt32 (Res.name);

		A = Convert.ToInt32 (gameObject.name);

		rezultat [A] = Convert.ToInt32 (Rezultat[A].name);
		shagi [A] = Convert.ToInt32 (Shagi [A].name);

		//if (pipetka[0].transform.position.y != 1.65f & pipetka[1].transform.position.y != 1.65f  & pipetka[2].transform.position.y != 1.65f & pipetka[3].transform.position.y != 1.65f)
		//{
		//	iTween.MoveTo(gameObject, iTween.Hash("y", 1.65f, "time", .3f));
		//}
		if (Pipetka[0].transform.position.y == 1.65f)
		{	
			iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));

			B = Convert.ToInt32 (Pipetka[0].name);
			Vodastakan = B;

			iTween.MoveTo(Pipetka[0], iTween.Hash("y", 1.63f, "time", 0.1f));
			iTween.MoveTo(Pipetka[0], iTween.Hash("x", gameObject.transform.position.x, "y", 1.63f, "z", gameObject.transform.position.z, "time", 0.9f, "delay", 0.1f ));
			iTween.MoveTo(Pipetka[0], iTween.Hash("y", gameObject.transform.position.y + 0.04f, "time", 1f, "delay", 1f));
			iTween.MoveTo(Pipetkavoda[0], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 1f, "delay", 2f));
			iTween.ScaleTo(Pipetkavoda[0], iTween.Hash("x", 0f,"y", 0f, "z", 0f, "time", 0.6f, "delay", 2.2f));

			iTween.MoveTo(Pipetka[0], iTween.Hash("y", 1.63f, "time", 1f, "delay", 3f));
			iTween.MoveTo(Pipetka[0], iTween.Hash("x", Stakan[0].transform.position.x, "y", 1.63f, "z", Stakan[0].transform.position.z, "time", 1f, "delay", 4f));
			iTween.MoveTo(Pipetka[0], iTween.Hash("y", Stakan[0].transform.position.y + 0.036f, "time", 1f, "delay", 5f));
			iTween.MoveTo(Pipetkavoda[0], iTween.Hash("y", Pipetka[0].transform.position.y - 0.143268f, "time", 1f, "delay", 6f));
			iTween.ScaleTo(Pipetkavoda[0], iTween.Hash("x", 0.2f,"y", 0.3f, "z", 0.2f, "time", 0.6f, "delay", 6f));


			if (shagi[A] == 0 & rezultat[A]==shagi[A])
			{
				iTween.ScaleTo(Voda[B], iTween.Hash("x", .9f, "z", .9f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Voda[B], iTween.Hash("y", .35f, "time", 0.8f, "delay", 2.2f));
				iTween.MoveTo(Voda[B], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 0.8f, "delay", 2.2f));
				rezultat[A] = rezultat[A] + 1;
				Rezultat[A].name = Convert.ToString(rezultat[A]);

			}
			shagi[A] = shagi[A] + 1;
			Shagi[A].name = Convert.ToString(shagi[A]);

			iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 7f));
		}
		if (Pipetka[1].transform.position.y == 1.65f)
		{	
			iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));
			
			B = Convert.ToInt32 (Pipetka[1].name);
			Vodastakan = B;
			
			iTween.MoveTo(Pipetka[1], iTween.Hash("y", 1.63f, "time", 0.1f));
			iTween.MoveTo(Pipetka[1], iTween.Hash("x", gameObject.transform.position.x, "y", 1.63f, "z", gameObject.transform.position.z, "time", 0.9f, "delay", 0.1f ));
			iTween.MoveTo(Pipetka[1], iTween.Hash("y", gameObject.transform.position.y + 0.04f, "time", 1f, "delay", 1f));
			iTween.MoveTo(Pipetkavoda[1], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 1f, "delay", 2f));
			iTween.ScaleTo(Pipetkavoda[1], iTween.Hash("x", 0f,"y", 0f, "z", 0f, "time", 0.6f, "delay", 2.2f));
			
			iTween.MoveTo(Pipetka[1], iTween.Hash("y", 1.63f, "time", 1f, "delay", 3f));
			iTween.MoveTo(Pipetka[1], iTween.Hash("x", Stakan[1].transform.position.x, "y", 1.63f, "z", Stakan[1].transform.position.z, "time", 1f, "delay", 4f));
			iTween.MoveTo(Pipetka[1], iTween.Hash("y", Stakan[1].transform.position.y + 0.036f, "time", 1f, "delay", 5f));
			iTween.MoveTo(Pipetkavoda[1], iTween.Hash("y", Pipetka[1].transform.position.y - 0.143268f, "time", 1f, "delay", 6f));
			iTween.ScaleTo(Pipetkavoda[1], iTween.Hash("x", 0.2f,"y", 0.3f, "z", 0.2f, "time", 0.6f, "delay", 6f));

			
			if (shagi[A]== 0 & rezultat[A]==shagi[A])
			{
				iTween.ScaleTo(Voda[B], iTween.Hash("x", .9f, "z", .9f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Voda[B], iTween.Hash("y", .35f, "time", 0.8f, "delay", 2.2f));
				iTween.MoveTo(Voda[B], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 0.8f, "delay", 2.2f));
				rezultat[A] = rezultat[A] + 1;
				Rezultat[A].name = Convert.ToString(rezultat[A]);
			}
			shagi[A] = shagi[A] + 1;
			Shagi[A].name = Convert.ToString(shagi[A]);		
			iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 7f));
		}
		if (Pipetka[2].transform.position.y == 1.65f)
		{	
			iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));
			
			B = Convert.ToInt32 (Pipetka[2].name);
			
			iTween.MoveTo(Pipetka[2], iTween.Hash("y", 1.63f, "time", 0.1f));
			iTween.MoveTo(Pipetka[2], iTween.Hash("x", gameObject.transform.position.x, "y", 1.63f, "z", gameObject.transform.position.z, "time", 0.9f, "delay", 0.1f ));
			iTween.MoveTo(Pipetka[2], iTween.Hash("y", gameObject.transform.position.y + 0.04f, "time", 1f, "delay", 1f));
			iTween.MoveTo(Pipetkavoda[2], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 1f, "delay", 2f));
			iTween.ScaleTo(Pipetkavoda[2], iTween.Hash("x", 0f,"y", 0f, "z", 0f, "time", 0.6f, "delay", 2.2f));
			
			iTween.MoveTo(Pipetka[2], iTween.Hash("y", 1.63f, "time", 1f, "delay", 3f));
			iTween.MoveTo(Pipetka[2], iTween.Hash("x", Stakan[2].transform.position.x, "y", 1.63f, "z", Stakan[2].transform.position.z, "time", 1f, "delay", 4f));
			iTween.MoveTo(Pipetka[2], iTween.Hash("y", Stakan[2].transform.position.y + 0.036f, "time", 1f, "delay", 5f));
			iTween.MoveTo(Pipetkavoda[2], iTween.Hash("y", Pipetka[2].transform.position.y - 0.143268f, "time", 1f, "delay", 6f));
			iTween.ScaleTo(Pipetkavoda[2], iTween.Hash("x", 0.2f,"y", 0.3f, "z", 0.2f, "time", 0.6f, "delay", 6f));

			
			if (shagi[A] == 1 & rezultat[A]==shagi[A])
			{
				iTween.ScaleTo(Voda[Vodastakan], iTween.Hash("y", 0f, "time", 0.8f, "delay", 2f));
				iTween.ScaleTo(Voda[Vodastakan], iTween.Hash("x", 0f, "z", 0f, "time", 0.2f, "delay", 2.2f));
				iTween.MoveTo(Voda[Vodastakan], iTween.Hash("y", gameObject.transform.position.y - 0.01686768f, "time", 0.8f, "delay", 2.2f));
				iTween.ScaleTo(Osadok[Vodastakan], iTween.Hash("x", .9f, "z", .9f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Osadok[Vodastakan], iTween.Hash("y", .15f, "time", 0.8f, "delay", 2.2f));
				iTween.MoveTo(Osadok[Vodastakan], iTween.Hash("y", gameObject.transform.position.y - 0.0198086768f, "time", 0.8f, "delay", 2.2f));
				iTween.ScaleTo(Osadok1[Vodastakan], iTween.Hash("x", .9f, "z", .9f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Osadok1[Vodastakan], iTween.Hash("y", .35f, "time", 0.8f, "delay", 2.2f));
				iTween.MoveTo(Osadok1[Vodastakan], iTween.Hash("y", gameObject.transform.position.y - 0.01486768f, "time", 0.8f, "delay", 2.2f));
			}

			if (shagi[A] == 2 & rezultat[A]==shagi[A])
			{
				if (Vodastakan == 0)
				{
					Opity[0].name = Convert.ToString(1);
				}
				if (Vodastakan == 1)
				{
					iTween.ScaleTo(Voda[Vodastakan], iTween.Hash("x", .9f, "z", .9f, "time", 0.2f, "delay", 2f));
					iTween.ScaleTo(Voda[Vodastakan], iTween.Hash("y", .35f, "time", 0.8f, "delay", 2.2f));
					iTween.MoveTo(Voda[Vodastakan], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 0.8f, "delay", 2.2f));
					
					iTween.ScaleTo(Osadok[Vodastakan], iTween.Hash("x", 0f, "z", 0f, "time", 0.2f, "delay", 2f));
					iTween.ScaleTo(Osadok[Vodastakan], iTween.Hash("y", 0f, "time", 0.5f, "delay", 2.2f));
					
					iTween.ScaleTo(Osadok1[Vodastakan], iTween.Hash("x", 0f, "z", 0f, "time", 0.2f, "delay", 2f));
					iTween.ScaleTo(Osadok1[Vodastakan], iTween.Hash("y", 0f, "time", 0.5f, "delay", 2.2f));

					Opity[1].name = Convert.ToString(1);
				}
			}

			if (shagi[A] == 1 || shagi[A] == 2)
			{
				if (rezultat[A]==shagi[A])
				{
					rezultat[A] = rezultat[A] + 1;
					Rezultat[A].name = Convert.ToString(rezultat[A]);
				}
			}

			shagi[A] = shagi[A] + 1;
			Shagi[A].name = Convert.ToString(shagi[A]);		
			iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 7f));
		}
		if (Pipetka[3].transform.position.y == 1.65f)
		{	
			iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));
			
			B = Convert.ToInt32 (Pipetka[3].name);
			
			iTween.MoveTo(Pipetka[3], iTween.Hash("y", 1.63f, "time", 0.1f));
			iTween.MoveTo(Pipetka[3], iTween.Hash("x", gameObject.transform.position.x, "y", 1.63f, "z", gameObject.transform.position.z, "time", 0.9f, "delay", 0.1f ));
			iTween.MoveTo(Pipetka[3], iTween.Hash("y", gameObject.transform.position.y + 0.04f, "time", 1f, "delay", 1f));
			iTween.MoveTo(Pipetkavoda[3], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 1f, "delay", 2f));
			iTween.ScaleTo(Pipetkavoda[3], iTween.Hash("x", 0f,"y", 0f, "z", 0f, "time", 0.6f, "delay", 2.2f));
			
			iTween.MoveTo(Pipetka[3], iTween.Hash("y", 1.63f, "time", 1f, "delay", 3f));
			iTween.MoveTo(Pipetka[3], iTween.Hash("x", Stakan[3].transform.position.x, "y", 1.63f, "z", Stakan[3].transform.position.z, "time", 1f, "delay", 4f));
			iTween.MoveTo(Pipetka[3], iTween.Hash("y", Stakan[3].transform.position.y + 0.036f, "time", 1f, "delay", 5f));
			iTween.MoveTo(Pipetkavoda[3], iTween.Hash("y", Pipetka[3].transform.position.y - 0.143268f, "time", 1f, "delay", 6f));
			iTween.ScaleTo(Pipetkavoda[3], iTween.Hash("x", 0.2f,"y", 0.3f, "z", 0.2f, "time", 0.6f, "delay", 6f));

			
			if (shagi[A] == 2 & rezultat[A]==shagi[A])
			{
				iTween.ScaleTo(Voda[Vodastakan], iTween.Hash("x", .9f, "z", .9f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Voda[Vodastakan], iTween.Hash("y", .35f, "time", 0.8f, "delay", 2.2f));
				iTween.MoveTo(Voda[Vodastakan], iTween.Hash("y", gameObject.transform.position.y - 0.01586768f, "time", 0.8f, "delay", 2.2f));
				
				iTween.ScaleTo(Osadok[Vodastakan], iTween.Hash("x", 0f, "z", 0f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Osadok[Vodastakan], iTween.Hash("y", 0f, "time", 0.5f, "delay", 2.2f));
				
				iTween.ScaleTo(Osadok1[Vodastakan], iTween.Hash("x", 0f, "z", 0f, "time", 0.2f, "delay", 2f));
				iTween.ScaleTo(Osadok1[Vodastakan], iTween.Hash("y", 0f, "time", 0.5f, "delay", 2.2f));
				rezultat[A] = rezultat[A] + 1;
				Rezultat[A].name = Convert.ToString(rezultat[A]);

				if (Vodastakan == 0)
				{
					Opity[2].name = Convert.ToString(1);
				}
				if (Vodastakan == 1)
				{
					Opity[3].name = Convert.ToString(1);
				}

			}

			shagi[A] = shagi[A] + 1;
			Shagi[A].name = Convert.ToString(shagi[A]);		
			iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 7f));
		}

		
		


			
		//stena.collider.enabled = false;
	}

	void OnMouseUp ()
	{
		iTween.MoveTo(gameObject, iTween.Hash("y", 1.523161f, "delay", .01f));
	}

	// Use this for initialization
	void Start ()
	{
		S = 0;
		Result = 0;
		rezultat[0] = 0; 
		rezultat[1] = 0;
		rezultat[2] = 0;
		rezultat[3] = 0;

		shagi[0] = 0; 
		shagi[1] = 0;
		shagi[2] = 0;
		shagi[3] = 0;
		Vodastakan = 0;

		P0 [0] = 0;		P1 [0] = 0;		P2 [0] = 1;		P3 [0] = 1;
		P0 [1] = 2;		P1 [1] = 2;		P2 [1] = 2;		P3 [1] = 2;
		P0 [2] = 3;		P1 [2] = 2;		P2 [2] = 3;		P3 [2] = 2;
	}
	
	// Update is called once per frame
	void Update () 
	{
		S = Convert.ToInt32(Shagi[0].name) + Convert.ToInt32(Shagi[1].name)  + Convert.ToInt32(Shagi[2].name)  + Convert.ToInt32(Shagi[3].name) ;
		Shag.name = Convert.ToString(S);
		
		C = Convert.ToInt32(Rezultat[0].name) + Convert.ToInt32(Rezultat[1].name)  + Convert.ToInt32(Rezultat[2].name)  + Convert.ToInt32(Rezultat[3].name) ;
		Res.name = Convert.ToString(C);
	}
}

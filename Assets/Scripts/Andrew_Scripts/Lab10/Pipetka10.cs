﻿using UnityEngine;

public class Pipetka10 : MonoBehaviour {

	public GameObject [] Pipetka;

	public GameObject[] Stakan;

	public bool A = false;

	// Use this for initialization
	void Start () {
		iTween.MoveTo(Pipetka[0], iTween.Hash("x", Stakan[0].transform.position.x, "y", Stakan[0].transform.position.y + 0.036f, "z", Stakan[0].transform.position.z, "time", .01f, "delay", .01f));
		iTween.MoveTo(Pipetka[1], iTween.Hash("x", Stakan[1].transform.position.x, "y", Stakan[1].transform.position.y + 0.036f, "z", Stakan[1].transform.position.z, "time", .01f, "delay", .01f));
		iTween.MoveTo(Pipetka[2], iTween.Hash("x", Stakan[2].transform.position.x, "y", Stakan[2].transform.position.y + 0.036f, "z", Stakan[2].transform.position.z, "time", .01f, "delay", .01f));
		iTween.MoveTo(Pipetka[3], iTween.Hash("x", Stakan[3].transform.position.x, "y", Stakan[3].transform.position.y, "z", Stakan[3].transform.position.z, "time", .01f, "delay", .01f));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

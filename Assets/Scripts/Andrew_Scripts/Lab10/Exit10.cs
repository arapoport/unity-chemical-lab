﻿using UnityEngine;

public class Exit10 : MonoBehaviour {

	public Texture Smena;
	public Texture RSmena;

	void OnMouseOver()
	{
		guiTexture.texture = Smena;
	}
	
	void OnMouseExit()
	{
		guiTexture.texture = RSmena;
	}

	void OnMouseDown()
	{
		Application.Quit ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

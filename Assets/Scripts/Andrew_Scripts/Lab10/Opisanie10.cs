﻿using UnityEngine;

public class Opisanie10 : MonoBehaviour {

	public GameObject Opisanie;
	public GameObject Opisanietext;

	void OnMouseEnter()
	{
		Opisanietext.renderer.enabled = true;
		Opisanie.renderer.enabled = true;
	}

	void OnMouseExit()
	{
		Opisanietext.renderer.enabled = false;
		Opisanie.renderer.enabled = false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using System;
using UnityEngine;

public class Stakan9 : MonoBehaviour {

	public GameObject Menu0;
	public GameObject Menu1;
	public GameObject Menu2;
	public GameObject Menu3;
	public GameObject Menu0Text;
	public GameObject Menu1Text;
	public GameObject Menu2Text;
	public GameObject Menu3Text;

	public GameObject Pipet;

	public GameObject [] Pipetka;

	public GameObject [] Stakan;

	void OnMouseDown()
	{
		Menu0.renderer.enabled = false;
		Menu1.renderer.enabled = false;
		Menu2.renderer.enabled = false;
		Menu3.renderer.enabled = false;
		Menu0Text.renderer.enabled = false;
		Menu1Text.renderer.enabled = false;
		Menu2Text.renderer.enabled = false;
		Menu3Text.renderer.enabled = false;
		Menu0.collider.enabled = false;
		Menu1.collider.enabled = false;
		Menu2.collider.enabled = false;
		Menu3.collider.enabled = false;
		
		
		if (Pipetka [0].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [0], iTween.Hash ("y", Stakan [0].transform.position.y, "time", .3f, "delay", .01f));
			iTween.RotateTo(Pipetka[0], iTween.Hash("z", 0, "time", .2f, "delay", .1f));
		}
		if (Pipetka [1].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [1], iTween.Hash ("y", Stakan [1].transform.position.y, "time", .3f, "delay", .01f));
			iTween.RotateTo(Pipetka[1], iTween.Hash("z", 0, "time", .2f, "delay", .1f));
		}
		if (Pipetka [2].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [2], iTween.Hash ("y", Stakan [2].transform.position.y, "time", .3f, "delay", .01f));
			iTween.RotateTo(Pipetka[2], iTween.Hash("z", 0, "time", .2f, "delay", .1f));
		}
		if (Pipetka [3].transform.position.y == 1.65f) {
			iTween.MoveTo (Pipetka [3], iTween.Hash ("y", Stakan [3].transform.position.y+0.036f, "time", .3f, "delay", .01f));
			//iTween.RotateTo(pipetka[2], iTween.Hash("z", 0, "time", .2f, "delay", .1f));
		}
		
		if (Pipet.transform.position.y != 1.65f)
		{			
			iTween.MoveTo(Pipet, iTween.Hash("y", 1.65f, "time", .3f, "delay", .01f));
			if (Convert.ToInt32(Pipet.name)==0||Convert.ToInt32(Pipet.name)==1||Convert.ToInt32(Pipet.name)==2)
			{
				iTween.RotateTo(Pipet, iTween.Hash("z", 90f, "time", .2f, "delay", .1f));
			}
		}
	}

	void OnMouseEnter()
	{
			
		MyCursor.SetClick();
	}
	
	void OnMouseExit()
	{

		MyCursor.SetNormal();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

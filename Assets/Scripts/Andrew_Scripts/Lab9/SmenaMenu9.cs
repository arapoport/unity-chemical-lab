﻿using System;
using UnityEngine;

public class SmenaMenu9 : MonoBehaviour {

	public Material Smena;
	public Material RSmena;

	public GameObject Stena;

	public GameObject Menu0;
	public GameObject Menu1;
	public GameObject Menu2;
	public GameObject Menu3;
	public GameObject Menu0Text;
	public GameObject Menu1Text;
	public GameObject Menu2Text;
	public GameObject Menu3Text;

	public GameObject [] Prob;
	public GameObject [] Res;
	public GameObject [] Shagi;
	public GameObject Nomerv;
	public int A;
	public int B;
	public int C;

	public float X1;
	public float Y1;
	public float Z1;
	public float X2;
	public float Y2;
	public float Z2;	

	void OnMouseDown()
	{
		Menu0.renderer.enabled = false;
		Menu1.renderer.enabled = false;
		Menu2.renderer.enabled = false;
		Menu3.renderer.enabled = false;
		Menu0Text.renderer.enabled = false;
		Menu1Text.renderer.enabled = false;
		Menu2Text.renderer.enabled = false;
		Menu3Text.renderer.enabled = false;
		Menu0.collider.enabled = false;
		Menu1.collider.enabled = false;
		Menu2.collider.enabled = false;
		Menu3.collider.enabled = false;

		A = Convert.ToInt32 (gameObject.name);
		C = Convert.ToInt32 (Nomerv.name);

		var i = 0;
		for (i = 0; i < 8; i++) 
		{
			if (A == Convert.ToInt32(Prob[i].name))
			{
				iTween.MoveTo(Stena, iTween.Hash("y", 1.65f, "time", 0.1f));

				X1 = Prob[i].transform.position.x;
				Y1 = Prob[i].transform.position.y;
				Z1 = Prob[i].transform.position.z;

				X2 = Prob[C].transform.position.x;
				Y2 = Prob[C].transform.position.y;
				Z2 = Prob[C].transform.position.z;



				iTween.MoveTo(Prob[i], iTween.Hash("y", Prob[i].transform.position.y + 0.1f, "time", 1f));
				iTween.MoveTo(Prob[i], iTween.Hash("x", X2-0.017f, "z", Z2+0.006f, "time", 1f, "delay", 1f));
				iTween.MoveTo(Prob[i], iTween.Hash("y", Y2, "time", 1f, "delay", 2f));


				iTween.MoveTo(Prob[C], iTween.Hash("y", Prob[C].transform.position.y + 0.1f, "time", 1f, "delay", 3f));
				iTween.MoveTo(Prob[C], iTween.Hash("x", X1, "z", Z1, "time", 1f, "delay", 4f));
				iTween.MoveTo(Prob[C], iTween.Hash("y", Y1, "time", 1f, "delay", 5f));

				Res[Convert.ToInt32(Prob[i].name)].name = Convert.ToString(0);
				Shagi[Convert.ToInt32(Prob[i].name)].name = Convert.ToString(0);

				B = Convert.ToInt32(Prob[C].name);
				Prob[C].name = Prob[i].name;
				Prob[i].name = Convert.ToString(B);
				C=C+1;
				Nomerv.name = Convert.ToString(C);
				Prob[i].collider.enabled=false;
				Prob[i].name = Convert.ToString(9);
				i=7;

				iTween.MoveTo(Stena, iTween.Hash("y", 10f, "time", 0.1f, "delay", 6f));

			}		
		}
	}


	void OnMouseEnter()
	{
		gameObject.renderer.material = Smena;				
	}
	
	void OnMouseExit()
	{
		gameObject.renderer.material = RSmena;
	}

	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;

public class StakanPoloski9 : MonoBehaviour {

	public GameObject Menu0;
	public GameObject Menu1;
	public GameObject Menu2;
	public GameObject Menu3;
	public GameObject Menu0Text;
	public GameObject Menu1Text;
	public GameObject Menu2Text;
	public GameObject Menu3Text;
	
	public int P;
	
	public GameObject [] Poloska;
	
	public GameObject Stakan;
	
	void OnMouseDown()
	{
		Menu0.renderer.enabled = false;
		Menu1.renderer.enabled = false;
		Menu2.renderer.enabled = false;
		Menu3.renderer.enabled = false;
		Menu0Text.renderer.enabled = false;
		Menu1Text.renderer.enabled = false;
		Menu2Text.renderer.enabled = false;
		Menu3Text.renderer.enabled = false;	
		Menu0.collider.enabled = false;
		Menu1.collider.enabled = false;
		Menu2.collider.enabled = false;
		Menu3.collider.enabled = false;

		if (Poloska[P].transform.position.y != 1.495f) 
		{
					
		}
		else
		{
			P=P+1;
		}

		if (Poloska[P].transform.position.y != 1.65f)
		{			
			iTween.MoveTo(Poloska[P], iTween.Hash("y", 1.65f, "time", .3f, "delay", .01f));
		}
		else
		{
			iTween.MoveTo(Poloska[P], iTween.Hash("y", Stakan.transform.position.y, "time", .3f, "delay", .01f));
		}

	}

	void OnMouseEnter()
	{

		MyCursor.SetClick();
	}
	
	void OnMouseExit()
	{

		MyCursor.SetNormal();
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using UnityEngine;

public class Poloski9 : MonoBehaviour {

	public GameObject Menu0;
	public GameObject Menu1;
	public GameObject Menu2;
	public GameObject Menu3;
	public GameObject Menu0Text;
	public GameObject Menu1Text;
	public GameObject Menu2Text;
	public GameObject Menu3Text;
	
	public GameObject [] Poloska;
	
	public GameObject Stakan;	
	
	void OnMouseDown()
	{
		Menu0.renderer.enabled = false;
		Menu1.renderer.enabled = false;
		Menu2.renderer.enabled = false;
		Menu3.renderer.enabled = false;
		Menu0Text.renderer.enabled = false;
		Menu1Text.renderer.enabled = false;
		Menu2Text.renderer.enabled = false;
		Menu3Text.renderer.enabled = false;
		Menu0.collider.enabled = false;
		Menu1.collider.enabled = false;
		Menu2.collider.enabled = false;
		Menu3.collider.enabled = false;
		
		


		if (gameObject.transform.position.y != 1.63f)
		{	

		}
		else
		{
			iTween.MoveTo(gameObject, iTween.Hash("y", Stakan.transform.position.y, "time", .3f, "delay", .01f));
		}
	}
	
	// Use this for initialization
	void Start () {
		iTween.MoveTo(Poloska[0], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[1], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[2], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[3], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[4], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[5], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[6], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
		iTween.MoveTo(Poloska[7], iTween.Hash("y", Stakan.transform.position.y,"time", .01f, "delay", .01f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

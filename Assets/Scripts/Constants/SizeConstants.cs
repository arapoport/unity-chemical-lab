﻿using UnityEngine;
using System.Collections;

public class SizeConstants {

    public SizeConstants()
    {
        
    }

    public const float MessageW = 300;
    public const float MessageH = 300;
    public const float ButtonW = 100;
    public const float ButtonH = 50;
	
}

﻿using DG.Tweening;
using UnityEngine;

public class CamRotate : MonoBehaviour { //Для поворота камеры, вешать на камеру

	// Use this for initialization
	void Start () {
        camRotationYDef = gameObject.transform.rotation.eulerAngles.y;
        camRotationXDef = gameObject.transform.rotation.eulerAngles.x;
        readyButton = GameObject.Find("ready").GetComponent<CheckLabs>();
        cam = camera;
        pause = GameObject.Find("Player").GetComponent<Pause>();
	}

    Camera cam;
    CheckLabs readyButton;
    Pause pause;

    float camRotationYDef; // начальный поворот камеры по y
    float camRotationXDef; // начальный поворот камеры по x

    float amplituda = 30f;
    float camSpeed = 1f;

    float temp;


	// Update is called once per frame
	void Update () {
        if (pause.Paused == false && readyButton.GameOver == false)
        {
            if (Input.mousePosition.x < 20 && transform.eulerAngles.y > camRotationYDef - amplituda)
            {
                transform.DOLocalRotate(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y - camSpeed, transform.eulerAngles.z), 0);
            }
            else if (Input.mousePosition.x > Screen.width - 20 && transform.eulerAngles.y < camRotationYDef + amplituda)
            {
                transform.DOLocalRotate(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + camSpeed, transform.eulerAngles.z), 0);
            }
            else if (Input.mousePosition.y > Screen.height - 20 && transform.eulerAngles.x > camRotationXDef - amplituda / 1.5)
            {
                transform.DOLocalRotate(new Vector3(transform.eulerAngles.x - camSpeed, transform.eulerAngles.y, transform.eulerAngles.z), 0);
            }
            else if (Input.mousePosition.y < 20 && transform.eulerAngles.x < camRotationXDef + amplituda / 3)
            {
                transform.DOLocalRotate(new Vector3(transform.eulerAngles.x + camSpeed, transform.eulerAngles.y, transform.eulerAngles.z), 0);
            }

            temp = cam.fieldOfView + -20 * Input.GetAxis("Mouse ScrollWheel");

            if (temp > 20 && temp <= 60)
            {
                cam.fieldOfView = temp;
            }
        }
	}
}

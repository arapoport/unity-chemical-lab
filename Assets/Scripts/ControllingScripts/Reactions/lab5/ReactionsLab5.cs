﻿using UnityEngine;

public class ReactionsLab5 : Reactions {

	// Use this for initialization
	void Start () {
        Fail = Resources.Load("materials/fail") as Material;
	}

    public Material Water;
    public Material Ferum;
    public Material AmmoniumChloride;

    public Material WaterFerum;
    public Material WaterFerumThiocyanate;
    public Material WaterFerumThiocyanateStick;

    public Material WaterFerumThiocyanateStickFerum;
    public Material WaterFerumThiocyanateStickThiocyanate;
    public Material WaterFerumThiocyanateStickChloride;

    public override Material GetBubblesMaterial(string content)
    {
        return Fail;
    }

    public override Material GetMaterial(string content, out string result)
    {
        Material material;
        switch (content)
        {
            case "empty+distilled_water": material = Water;
                result = "distilled_water";
                break;//воду в пустую пробирку
            case "empty+ferric_chloride": material = Ferum;
                result = "fail";
                break;//хлорид железа в пустую пробирку;
            case "empty+ammonium_chloride": material = AmmoniumChloride;
                result = "fail";
                break;//хлорид аммония в пустую пробирку
            case "empty+ammonium_thiocyanate": material = Water;
                result = "fail";
                break;//тиоцинат амония в пустую пробирку
            case "distilled_water+ferric_chloride": material = WaterFerum;
                result = content;
                break;//в воду капнули железа
            case "distilled_water+ferric_chloride+ammonium_thiocyanate": material = WaterFerumThiocyanate;
                result = content;
                break;//вода+железо+тиоцинат аммония
            case "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick": material = WaterFerumThiocyanateStick;
                result = content;
                break;//вода+железо+тиоцинат аммония+помешали
            case "empty+distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick": material = WaterFerumThiocyanateStick;
                result = "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick";
                break;//вода+железо+тиоцинат аммония+помешали и разлили по пробиркам
            case "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick+ferric_chloride": material = WaterFerumThiocyanateStickFerum;
                result = content;
                break;//вода+железо+тиоцинат аммония+помешали вылили в пробирку и добавили железа
            case "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick+ammonium_thiocyanate": material = WaterFerumThiocyanateStickThiocyanate;
                result = content;
                break;//вода+железо+тиоцинат аммония+помешали вылили в пробирку и добавили тиоцинат аммония
            case "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick+ammonium_chloride": material = WaterFerumThiocyanateStickChloride;
                result = content;
                break;//вода+железо+тиоцинат аммония+помешали вылили в пробирку и добавили хлорид аммония
            default: material = Fail;
                result = "fail";
                break;//ошибка 
        }

        return material;
    }

}

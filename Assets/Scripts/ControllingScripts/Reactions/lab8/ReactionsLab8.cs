﻿using UnityEngine;

public class ReactionsLab8 : Reactions {

	// Use this for initialization
	void Start () {
        Fail = Resources.Load("materials/fail") as Material;
	}

    public Material CopperSulphate; //раствор сульфата меди
    public Material Alkaline; //раствор щелечи
    public Material SulfuricAcid; //серная кислота

    public Material CopperSulphateAlkalineMany; //сульфат меди+щелочь много
    public Material CopperSulphateAlkalineSmall; //сульфат меди+щелочь мало

    public override Material GetBubblesMaterial(string content)
    {
        return Fail;
    }

	/*TODO: разобраться со всеми "empty+...", чтобы их не было в case у switch. 
	*Позже (или сразу), можно составить какой-нибудь Dictionary со всеми этими
	*вариантами реакций, чтобы к нему делать запрос. Смысл в том, что всё равно после
	*завершения лабы проверка на содержимое будет. Поэтому пусть пользователь и делает реакции,
	*не нужные для какой-то конкретной лабы, но зато он делает правильную реакцию саму по себе.
	*То есть, при повторении каких-то реакций в разных лабах у нас не будет создаваться одинаковый
	*case. Хотя, если делать общий словарь, то он будет возможно каждый раз пробегаться 
	*по множеству ненужных вариантов, что будет медленно при большой куче этапов. Вот надо это изучить,
	*и понять, что и как лучше.*/
    public override Material GetMaterial(string content, out string result)
    {
        Material material;
        switch (content)
        {
            case "empty+copper_sulphate_solution": material = CopperSulphate; result = "copper_sulphate_solution"; break; //в пустую пробирку сульфат меди
            case "empty+sulfuric_acid": material = SulfuricAcid; result = "fail"; break; //в пустую пробирку серную кислоту
            case "empty+alkaline_solution_small": material = Alkaline; result = "fail"; break; //раствор щелочи в пустую пробирку
            case "empty+alkaline_solution_many": material = Alkaline; result = "fail"; break; //раствор щелочи в пустую пробирку
            
            case "copper_sulphate_solution+alkaline_solution_many": material = CopperSulphateAlkalineMany; result = content; break;
            case "copper_sulphate_solution+alkaline_solution_small": material = CopperSulphateAlkalineSmall; result = content; break;
            case "empty+copper_sulphate_solution+alkaline_solution_small": material = CopperSulphateAlkalineSmall; result = "copper_sulphate_solution+alkaline_solution_small"; break; //перелили в пустую пробирку то что намешали

            case "copper_sulphate_solution+alkaline_solution_small+alkaline_solution_small": material = CopperSulphateAlkalineMany; result = content; break; //тможно так это та пробирка в которую перелили
            case "copper_sulphate_solution+alkaline_solution_small+alkaline_solution_many": material = CopperSulphateAlkalineMany; result = content; break; //и так

            case "copper_sulphate_solution+alkaline_solution_small+sulfuric_acid": material = CopperSulphate; result = content; break; //в ту пробирку из которой отлили добавили кислоты

            default: material = Fail; result = "fail"; break;
        }

        return material;
    }
}
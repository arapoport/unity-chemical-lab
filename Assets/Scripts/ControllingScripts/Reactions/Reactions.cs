﻿using UnityEngine;

public abstract class Reactions : MonoBehaviour {
    public abstract Material GetMaterial(string content, out string result);
    public abstract Material GetBubblesMaterial(string content);
    protected Material Fail;
}
﻿using UnityEngine;

public class ReactionsLab3 : Reactions {

    public Material Water;
    public Material MuriaticAcid;

    public Material LitmusSolution;
    public Material SodiumSilicate;
    public Material WaterLitmusSolution;
    public Material Bubbles;
    public Material WaterLitmusSolutionKippApparatus;
    public Material SodiumSilicateKippApparatus;
    public Material SodiumSilicateMuriaticAcid;

	// Use this for initialization
	void Start () {
        Fail = Resources.Load("materials/fail") as Material;
	}
    public override Material GetMaterial(string content, out string result)
    {
        Material material;
        //Debug.Log("content =" + content);
        switch (content)
        {
            case "empty+distilled_water": material = Water;
                result = "distilled_water";
                break;//воду в пустую пробирку
            case "empty+litmus_solution": material = LitmusSolution;
                result = "fail";
                break;//лакмус в пустую пробирку
            case "empty+muriatic_acid": material = MuriaticAcid;
                result = "fail";
                break;//соляную кислоту в пустую пробирку
            case "empty+sodium_silicate": material = SodiumSilicate;
                result = "sodium_silicate";
                break;//силикат натрия в пустую пробирку
            case "distilled_water+litmus_solution": material = WaterLitmusSolution;
                result = content;
                break;//вода + лакмус
            case "sodium_silicate+muriatic_acid": material = SodiumSilicateMuriaticAcid;
                result = content;
                break;//силикат натрия + соляная кислота
            case "sodium_silicate+kippApparatus": material = SodiumSilicateKippApparatus;
                result = content;
                break;//силикат натрия + аппарат Киппа
            case "distilled_water+litmus_solution+kippApparatus": material = WaterLitmusSolutionKippApparatus;
                result = content;
                break;//вода + лакмус + аппарат Киппа
            default: material = Fail;
                result = "fail";
                break;//ошибка 
        }

        return material;
    }
    public override Material GetBubblesMaterial(string content)
    {
        Material material;
        switch (content)
        {
            case "kippApparatus": material = Bubbles;
                break;//аппарат Киппа
            default: material = Fail;
                break;//ошибка
        }
        return material;
    }
}

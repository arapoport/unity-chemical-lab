﻿using UnityEngine;

public class ReactionsLab2 : Reactions {

	// Use this for initialization
	void Start () {
        Fail = Resources.Load("materials/fail") as Material;
	}

    public Material Water; //вода
    public Material Phenolphthalein; //фенолфталеин
    public Material CalciumOxide; //оксид кальция
    public Material MagnesiumOxide; //оксид магния
    public Material HydrochloricAcid; //соляная кислота

    public Material CalciumOxideHydrochloricAcid; //кальций+соляная кислота
    public Material MagnesiumOxideHydrochloricAcid; //магний+соляная кислота
    public Material CalciumOxideWater; //кальций+вода
    public Material MagnesiumOxideWater; //магний+вода

    public Material CalciumOxideWaterPhenolphthalein; //кальций+вода+фенолфталеин
    public Material MagnesiumOxideWaterPhenolphthalein; //магний+вода+фенолфталеин

    public Material Bubbles;

    public override Material GetBubblesMaterial(string content)
    {
        Material material;
        switch (content)
        {
		case "calcium_oxide+hydrochloric_acid": material = Bubbles; break;
		case "magnesium_oxide+hydrochloric_acid": material = Bubbles; break;
		default: material = Fail; break;
        }
        return material;
    }

    public override Material GetMaterial(string content, out string result)
    {
        Material material;
        switch (content)
        {
            case "empty+distilled_water":                            material = Water; 
                                                                      result = "fail"; 
                                                                      break;//воду в пустую пробирку
            case "empty+phenolphthalein":                            material = Phenolphthalein;
                                                                      result = "fail"; 
                                                                      break;//фенолфталеин в пустую пробирку
            case "empty+calcium_oxide":                              material = CalciumOxide; 
                                                                      result = "calcium_oxide";  
                                                                      break;//кальций в пустую пробирку
            case "empty+magnesium_oxide":                            material = MagnesiumOxide;
                                                                      result = "magnesium_oxide";  
                                                                      break;//магний в пустую пробирку
            case "empty+hydrochloric_acid":                          material = HydrochloricAcid; 
                                                                      result = "empty+hydrochloric_acid";  
                                                                      break;//кислоту в пустую пробирку
            case "calcium_oxide+hydrochloric_acid":                  material = CalciumOxideHydrochloricAcid;
                                                                      result = "calcium_oxide+hydrochloric_acid"; 
                                                                      break;//кальций + соляная кислота
            case "magnesium_oxide+hydrochloric_acid":                material = MagnesiumOxideHydrochloricAcid;
                                                                      result = "magnesium_oxide+hydrochloric_acid"; 
                                                                      break;//магний + соляная кислота
            case "calcium_oxide+distilled_water":                    material = CalciumOxideWater;
                                                                      result = "calcium_oxide+distilled_water"; 
                                                                      break;//кальций + вода
            case "magnesium_oxide+distilled_water":                  material = MagnesiumOxideWater;
                                                                      result = "magnesium_oxide+distilled_water";  
                                                                      break;//магний + вода
            case "calcium_oxide+distilled_water+phenolphthalein":   material = CalciumOxideWaterPhenolphthalein;
                                                                      result = "calcium_oxide+distilled_water+phenolphthalein";  
                                                                      break;//кальций + вода + фенол
            case "magnesium_oxide+distilled_water+phenolphthalein": material = MagnesiumOxideWaterPhenolphthalein;
                                                                      result = "magnesium_oxide+distilled_water+phenolphthalein";
                                                                      break; //магний + вода + фенол
            default:                                                  material = Fail; 
                                                                      result = "fail"; 
                                                                      break;//ошибка 
        }
        
        return material;
    }

}

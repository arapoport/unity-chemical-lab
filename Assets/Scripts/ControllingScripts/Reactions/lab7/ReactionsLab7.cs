﻿using UnityEngine;

public class ReactionsLab7 : Reactions {

    public Material CalciumHydroxide;
    public Material Bubbles;
    public Material CalciumHydroxideKipp;
    public Material CalciumHydroxideKippCalciumHydroxide;

	// Use this for initialization
	void Start () {
        Fail = Resources.Load("materials/fail") as Material;
	}
    public override Material GetMaterial(string content, out string result)
    {
        Material material;
        //Debug.Log("content =" + content);
        switch (content)
        {
            case "empty+calcium_hydroxide": material = CalciumHydroxide;
                result = "calcium_hydroxide";
                break;//гидроксид кальция в пустую пробирку
            case "calcium_hydroxide+kippApparatus": material = CalciumHydroxideKipp;
                result = content;
                break;//гидроксид кальция + аппарат Киппа
            case "calcium_hydroxide+kippApparatus+calcium_hydroxide": material = CalciumHydroxideKippCalciumHydroxide;
                result = content;
                break;//гидроксид кальция + аппарат Киппа + гидроксид кальция
            default: material = Fail;
                result = "fail";
                break;//ошибка 
        }

        return material;
    }
    public override Material GetBubblesMaterial(string content)
    {
        Material material;
        switch (content)
        {
            case "kippApparatus": material = Bubbles;
                break;//аппарат Киппа
            default: material = Fail;
                break;//ошибка
        }
        return material;
    }
}

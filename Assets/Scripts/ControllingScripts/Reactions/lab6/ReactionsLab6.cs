﻿using UnityEngine;

public class ReactionsLab6 : Reactions {

    public Material Water;
    public Material SodiumCarbonate;
    public Material Bubbles;
    public Material SodiumCarbonateWater;

	// Use this for initialization
	void Start () {
        Fail = Resources.Load("materials/fail") as Material;
	}
    public override Material GetMaterial(string content, out string result)
    {
        Material material;
        if (content.Length>17 && content.Substring(0, 17).Contains("measuringCylinder"))
        {
            var cylinder = content.Substring(0, 22);
            content = content.Substring(23, content.Length - 23) + "+" + cylinder;
        }
        else if (content.Length>6 && content.Substring(0, 6).Contains("beaker"))
        {
            var beaker = content.Substring(0, 6);
            content = content.Substring(7, content.Length - 7) + "+" + beaker;
        }
        //Debug.Log("content = " + content);
        switch (content)
        {
            case "sodium_carbonate+measuringCylinder250ml": material = SodiumCarbonate;
                result = content;
                break;//сначала налили в 250мл цилиндр карбонат натрия
            case "distilled_water+measuringCylinder250ml": material = Water;
                result = "fail";
                break;//сначала налили в 250мл цилиндр воду - ошибка!
            case "sodium_carbonate+measuringCylinder100ml": material = SodiumCarbonate;
                result = content;
                break;//перелили раствор из стакана в 100мл цилиндр
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml": material = SodiumCarbonate;
                result = content;
                break;//перелили из 100мл цилиндра в 250мл цилиндр
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water": material = SodiumCarbonate;
                result = content;
                break;//долили воду до 250 мл в цилиндр ёмкостью 250 мл
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker": material = SodiumCarbonate;
                result = content;
                break;//перелили из большого цилиндра в пустой стакан (в первый раз)
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker+measuringCylinder250ml": material = SodiumCarbonate;
                result = content;
                break;//перелили из стакана в пустой большой цилиндр (в первый раз)
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker+measuringCylinder250ml+beaker": material = SodiumCarbonate;
                result = content;
                break;//перелили из большого цилиндра в пустой стакан (во второй раз)
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker+measuringCylinder250ml+beaker+measuringCylinder250ml": material = SodiumCarbonate;
                result = content;
                break;//перелили из стакана в пустой большой цилиндр (во второй раз)
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker+measuringCylinder250ml+beaker+measuringCylinder250ml+beaker": material = SodiumCarbonate;
                result = content;
                break;//перелили из большого цилиндра в пустой стакан (в третий раз)
            case "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker+measuringCylinder250ml+beaker+measuringCylinder250ml+beaker+measuringCylinder250ml": material = SodiumCarbonate;
                result = content;
                break;//перелили из стакана в пустой большой цилиндр (в третий раз)
            default: material = Fail;
                result = "fail";
                break;//ошибка 
        }
        //Debug.Log(result);
        return material;
    }
    public override Material GetBubblesMaterial(string content)
    {
        Material material;
        switch (content)
        {
            case "kippApparatus": material = Bubbles;
                break;//аппарат Киппа
            default: material = Fail;
                break;//ошибка
        }
        return material;
    }
}

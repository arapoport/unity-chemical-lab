﻿using UnityEngine;

public class Controller : TextProcessing {

	// Use this for initialization
	void Start () {
        finishFlag = true;
	    readyButton = FindObjectOfType<CheckLabs>();
        try
        {
            testTubes = FindObjectOfType<MainStand>();
            substanceState = GameObject.Find("substanceState").GetComponent<SubstanceState>();
        }
        catch
        {
            // ignored
        }
	}

    CheckLabs readyButton;

    void OnGUI()
    {
        if (!show) return;
        var style = new GUIStyle(GUI.skin.textArea) {fontSize = 30, alignment = TextAnchor.MiddleCenter};
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
        GUI.Label(new Rect(Screen.width / 2 - SizeConstants.MessageW / 2, Screen.height / 2 - SizeConstants.MessageH / 2, SizeConstants.MessageW, SizeConstants.MessageH),
            message, style);
        if (!GUI.Button(new Rect(Screen.width/2 - SizeConstants.ButtonW/2, Screen.height/2 + SizeConstants.MessageH/2, SizeConstants.ButtonW, SizeConstants.ButtonH), "ОК")) return;
        finishFlag = true;
        show = false;
    }

    public void ShowMessage()
    {
        FinishSelect();
        show = true;
    }

    MainStand testTubes;
    MyGameObject myGameObject;
    Apparatuses apparat;
    SourceParent source;
    TestTube testTube;
    Destination destination;
    SubstanceState substanceState;
    bool show;

    public void AddMyGameObject(MyGameObject myGameObject)
    {
        var destInSource = myGameObject.GetComponent<Destination>();
        var source = myGameObject.GetComponent<SourceParent>();
        var testTube = myGameObject.GetComponent<TestTube>();
        var apparat = myGameObject.GetComponent<Apparatuses>();
        if (destInSource != null && !destInSource.Active) return;
        if (destination != null || !finishFlag) return;
        readyButton.guiTexture.enabled = false;
        if (this.myGameObject == myGameObject) //если снова ткнули по тому же
        {
            finishFlag = false;
            StartCoroutine(myGameObject.Deselect()); //отменить выбор (опустить его)
            this.source = null;
            this.testTube = null;
            this.myGameObject = null;
            this.apparat = null;
        }
        else
        {
            //Debug.Log("source add");
            finishFlag = false;
            if (this.myGameObject != null) //если до этого был выбран другой
            {
                StartCoroutine(this.myGameObject.Deselect()); //отменить выбор (опустить его)
            }
            this.myGameObject = myGameObject;
            this.testTube = testTube;
            this.source = source;
            this.apparat = apparat;
            StartCoroutine(this.myGameObject.Select());
        }
    }

    public void Reserve(TestTube testTube)
    {
        if (!finishFlag) return;
        if (myGameObject != null) //если до этого было что то выбрано
        {
            StartCoroutine(myGameObject.Deselect()); //отменить выбор (опустить его)
        }
        var position = testTubes.GetPosEmptySlot();
        if (position == Vector3.zero) return;
        finishFlag = false;
        testTube.SetNewDefaultPosition(position);
        try
        {
            var glass = testTube.GetComponent<Glass>();
            glass.SetNewDefaultPosition(position);
        }
        catch
        {
        }
        StartCoroutine(testTube.Replace(position));
    }

    public void AddTrash(Trash trash)
    {
        if (!testTube || !finishFlag) return;
        finishFlag = false;
        testTubes.DelTestTube(testTube.transform.position);
        StartCoroutine(trash.Remove(testTube));
    }

    public void CheckResidue(ResiduePlate residuePlate)
    {
        if (!testTube || !finishFlag) return;
        finishFlag = false;
        substanceState.Check(testTube, residuePlate);
    }

    public void SetLanguage(string language)
    { 
		Translator translator = gameObject.AddComponent<Translator>();
        message = translator.GetTranslation(NameSpace.ShortTexts, "notFit", language);
        Destroy(translator);
    }

    string message = "";

    public void AddDestination(Destination destination)
    {
        MyGameObject temp = null;
        if (source)
        {
            MenuTextBox menu = source.gameObject.GetComponent<MenuTextBox>();
            if (source.gameObject != destination.gameObject)
            {
                if (!menu)
                {
                    if (destination.CheckAvailableVolume(source))
                    {
                        temp = source;
                    }
                    else
                    {
                        ShowMessage();
                    }
                }
                else
                {
                    temp = source;
                }
            }
        }
        else if (apparat)
        {
            temp = apparat;
        }

        if (!temp || this.destination || destination.gameObject == temp.gameObject || !destination.Active)
            return;
        this.destination = destination;
        readyButton.guiTexture.enabled = false;
        finishFlag = false;
        StartCoroutine(temp.StartAnimation(destination));
    }

    bool finishFlag;

    public void FinishSelect()
    {
        readyButton.guiTexture.enabled = true;
        finishFlag = true;
    }

    public void Finish()
    {
        readyButton.guiTexture.enabled = true;
        finishFlag = true;
        testTube = null;
        myGameObject = null;
        source = null;
        apparat = null;
        destination = null;
    }
	
}

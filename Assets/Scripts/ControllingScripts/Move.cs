﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Move : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DOTween.Init();
        water = GameObject.Find("water");
	}

    public void Shake(GameObject gameobject, float duration)
    {
       myTween = gameobject.transform.DOShakePosition(duration, 0.01f, 50);
    }

    public void Rotate(GameObject gameobject, float x, float y, float z, float duration)
    {
        var angle = new Vector3(x, y, z);
        myTween = gameobject.transform.DORotate(angle, duration);        
    }

    public void Rotate(GameObject gameobject, Vector3 angle, float duration)
    {
        myTween = gameobject.transform.DORotate(angle, duration);
    }

    public void RotateRelative(GameObject gameobject, float x, float y, float z, float duration)
    {
        var angle = new Vector3(gameobject.transform.eulerAngles.x + x, gameobject.transform.eulerAngles.y + y, gameobject.transform.eulerAngles.z + z);
        myTween = gameobject.transform.DORotate(angle, duration);
    }

    public void DoMove(GameObject gameobject, Vector3 destination, float duration)
    {
        myTween = gameobject.transform.DOMove(destination, duration);
    }

    public void MoveRelative(GameObject gameobject, float x, float y, float z, float duration)
    {
        var v3 = new Vector3(gameobject.transform.position.x + x, gameobject.transform.position.y + y, gameobject.transform.position.z + z);
        myTween = gameobject.transform.DOMove(v3, duration);
    }

    GameObject go;
    GameObject water;

    Vector3 destination;
    float moveDuration;

    public YieldInstruction WaitForCompletion()
    {
        return myTween.WaitForCompletion();
    }


    Tween myTween;

    public IEnumerator Drop(GameObject drop, Vector3 destination, float duration) //падение капли
    {
		myTween = drop.transform.DOMove(new Vector3(destination.x, destination.y + drop.transform.localScale.y, destination.z), duration - 0.2f);
		yield return new WaitForSeconds(duration - 0.2f);
		myTween = drop.transform.DOScaleY(0, 0.2f);
		myTween = drop.transform.DOMove(destination, 0.2f);
		yield return myTween.WaitForCompletion();
		drop.renderer.enabled = false;
    }


    public void WaterStart(Vector3 src, Vector3 dest, Material material, float duration)
    {
        water.transform.position = src;
        water.renderer.enabled = true;
        water.renderer.material = material;
        myTween = water.transform.DOMoveY((src.y + dest.y) / 2, duration);
        myTween = water.transform.DOScaleY((Mathf.Abs(src.y) - Mathf.Abs(dest.y))/2, duration);
    }

    public IEnumerator WaterEnd(Vector3 dest, float duration)
    {		
		myTween = water.transform.DOMoveY(dest.y, duration);
		myTween = water.transform.DOScaleY(0, duration);
		yield return new WaitForSeconds(duration);
		water.renderer.enabled = false;
    }

    public IEnumerator Pour(GameObject srcSubst, GameObject destSubst, float percent, float duration)
    {      
		var minScale = 0f;
		
		if (percent == 100)
		{
			minScale = 0.0000001f;
		}
		var srcLossyY = srcSubst.transform.lossyScale.y;
		var srcLossyX = srcSubst.transform.lossyScale.x;
		var srcLocalY = srcSubst.transform.localScale.y;
		var x1 = srcLossyY / srcLocalY;
		var volume = srcLossyX * srcLossyX * (srcLocalY - minScale) * x1 * (percent / 100);
		var decreaseLocalY = (srcLocalY - minScale) * (percent / 100); //на сколько мы уменьшим localscale
		
		myTween = srcSubst.transform.DOScaleY(srcLocalY - decreaseLocalY, duration);
		myTween = srcSubst.transform.DOLocalMoveY(srcSubst.transform.localPosition.y - decreaseLocalY, duration);
		
		//наливаем
		destSubst.renderer.enabled = true;
		var destLossyY = destSubst.transform.lossyScale.y; //типа высота
		var destLossyX = destSubst.transform.lossyScale.x; //типа радиус
		var destLocalY = destSubst.transform.localScale.y;
		var x2 = destLocalY / destLossyY;
		var destInscreaseLossy = volume / (destLossyX * destLossyX); //на сколько бы мы увеличили lossyScale
		
		var increaseLocal = x2 * destInscreaseLossy; //на сколько увеличиваем localScale.y
		
		myTween = destSubst.transform.DOScaleY(destLocalY + increaseLocal, duration);
		myTween = destSubst.transform.DOLocalMoveY(destSubst.transform.localPosition.y + increaseLocal, duration);
		
		yield return myTween.WaitForCompletion();
		
		if (percent == 100)
		{
			srcSubst.renderer.enabled = false;
		}
    }
}

﻿using UnityEngine;

public class ControlLab0 : MonoBehaviour {

	// Use this for initialization
	void Start () {
            start = GameObject.Find("ready").GetComponent<StartLab0>();  
            mouseLookX = GameObject.Find("Player").GetComponent<MouseLook>();            
            mouseLookY = GameObject.Find("Main Camera").GetComponent<MouseLook>();
            characterMotor = GameObject.Find("Player").GetComponent<CharacterMotor>();
            pause = GetComponent<Pause>();
            wr = GameObject.Find("workResults").GetComponent<WorkResults>();
            notebook = GameObject.Find("notebook").GetComponent<BookLab0>();
            tips = GameObject.Find("Tips").GetComponent<Tips>();
            run = GameObject.Find("Run").GetComponent<ShowRun>();
            bezopasnost = GameObject.Find("accident_prevention").GetComponent<AccidentPrevention>();         
           
       }

    Pause pause; //пауза
    StartLab0 start; //скрипт на кнопке старт
    MouseLook mouseLookX; //поворот по х скрипт на player
    MouseLook mouseLookY; //поворот по y скрипт на Main Camera
    CharacterMotor characterMotor; //скрипт перемещения персонажа находится на player

    Interf interf;
    
   
    WorkResults wr; //алгоритм работы
    BookLab0 notebook; //ноутбук
    Tips tips; //подсказки на кнопке i
    ShowRun run; //начальная картинка
    AccidentPrevention bezopasnost; //техника безопасности

    public Camera Cam;    

    public GUITexture Dot;

    public void RotateDisable()
    {
        mouseLookX.enabled = false;
        mouseLookY.enabled = false;
        Dot.enabled = false;

        rotateEnableFlag = false;
        MyCursor.Show();       
    }

    public void RotateEnable()
    {
        mouseLookX.enabled = true;
        mouseLookY.enabled = true;
        Dot.enabled = true;

        rotateEnableFlag = true;
        MyCursor.Hide();
    }

    public void MoveEnable()
    {
        characterMotor.enabled = true;
    }
    public void MoveDisable()
    {
        characterMotor.enabled = false;
    }
    bool rotateEnableFlag = true;


	// Update is called once per frame
	void Update () {        
        
        //если не проиграл и не на паузе и не показан алгоритм работы и ноутбук закрыт и подсказки выключены и не показана начальная картинка и не показана техника безопасности
        var ok = !start.Lose && !pause.Paused && !wr.IsShow && notebook.CloseBook && !tips.enabled && !run.ShowFlag() && !bezopasnost.ShowFlag();
        if (Input.GetKeyUp(KeyCode.Mouse1)) //Правая клавиша
        {
            if (rotateEnableFlag)
            {
                RotateDisable();
            }
            else
            {                
                if ( ok )
                {
                    RotateEnable();
                }
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse2)) //колесико
        {
            Cam.fieldOfView = 60;
        }

        temp = Cam.fieldOfView + -20 * Input.GetAxis("Mouse ScrollWheel");
        
        if (temp > 20 && temp <= 60 && ok)
        {
            Cam.fieldOfView = temp;
        }
        
        
        
	}

    float temp;
}

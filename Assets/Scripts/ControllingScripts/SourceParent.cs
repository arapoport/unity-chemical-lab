﻿using UnityEngine;

public abstract class SourceParent : MyGameObject{        
    public float Pour = 1; //в процентах

	public GameObject GetSubstance() {
		return transform.FindChild("substance").gameObject;
	}
}

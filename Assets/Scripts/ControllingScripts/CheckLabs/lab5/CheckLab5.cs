﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckLab5 : CheckLabs {

	// Use this for initialization
	void Start () {
        base.Start();
        TestTubes = GetDestinationTestTubes();
	}

    public override int FailCount()
    {
        var count = TestTubes.Where(testTube => testTube.Active).Sum(testTube => testTube.FailCounter);

        var glassGO = GameObject.Find("glass");
        var destGlass = glassGO.GetComponent<Destination>();
        count += destGlass.FailCounter;
        return count;
    }
	
	public override void Check()
	{
        var mixed = (from testTube in TestTubes where testTube.Active select testTube.Content).ToList();

	    var need = new List<string>
	    {
	        "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick+ferric_chloride",
	        "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick+ammonium_thiocyanate",
	        "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick+ammonium_chloride",
	        "distilled_water+ferric_chloride+ammonium_thiocyanate+mixingStick"
	    };


        GameOver = true;
        
        if (IsListCorrect(mixed, need))
        {
            Win = true;
        }
        else
        {
            Lose = true;
        }
        SaveResult(Time.time-StartTime, 5);
	}

    void OnMouseEnter()
    {       
        Over = true;
    }

    void OnMouseExit()
    {
        Over = false;
    }    

    void OnMouseUp()
    {
        Check();
    }	
}

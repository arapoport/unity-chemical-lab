﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckLab2 : CheckLabs{

	// Use this for initialization    
	void Start () {
        base.Start();
        TestTubes = GetDestinationTestTubes();     
	}

    public override int FailCount()
    {
        return TestTubes.Where(testTube => testTube.Active).Sum(testTube => testTube.FailCounter);
    }

    public override void Check()
    {
        var mixed = (from testTube in TestTubes where testTube.Active select testTube.Content).ToList();

        var need = new List<string>
        {
            "magnesium_oxide+distilled_water+phenolphthalein",
            "calcium_oxide+distilled_water+phenolphthalein",
            "calcium_oxide+hydrochloric_acid",
            "magnesium_oxide+hydrochloric_acid"
        };

        GameOver = true;
        
        if (IsListCorrect(mixed, need))
        {
            Win= true;
        }
        else
        {
            Lose = true;
        }
        SaveResult(Time.time-StartTime, 2);      
    }

    void OnMouseEnter()
    {
        Over = true;
    }

    void OnMouseExit()
    {
        Over = false;
    }
   
    void OnMouseUp()
    {
        Check();
    }

}

﻿using System.Collections.Generic;
using UnityEngine;

public class StartLab0 : CheckLabs {

	// Use this for initialization
	void Start () {
        control = GameObject.Find("Player").GetComponent<ControlLab0>();
        inventory = GameObject.Find("Player").GetComponent<InventoryLab0>();
    }

    ControlLab0 control;
    InventoryLab0 inventory;
    
    void OnMouseEnter()
    {
        Over = true;
    }

    void OnMouseExit()
    {
        Over = false;
    }

    public void Show()
    {
        guiTexture.enabled = true;
    }

	public override int FailCount()
	{
		return 1;
	}
	
	public override void Check()
	{
        var interf = GameObject.Find("interface").GetComponent<Interf>();
        inventory.hide();

        control.MoveDisable();
        control.RotateDisable();

	    var inv = new List<string>(inventory.GetNames());
        var need = new List<string>();

        if (interf.LabNumber == 10)
        {

            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            need.Add("microspatula");

            need.Add("pipette");
            need.Add("pipette");
            need.Add("pipette");

            need.Add("alkaline_solution");
            need.Add("sulfuric_acid"); //серн кислота
            need.Add("potassium_permanganate_solution"); //Раст перманганат калия
            need.Add("sodium_sulfite"); //сульф натр

            if (CheckArrays(need, inv))
            {
                Application.LoadLevel("Lab10");
            }
            else
            {
                // Time.timeScale = 0;
                //Debug.Log("10я лаба");
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21352";
            }

        }
        else if (interf.LabNumber == 1)
        {
            need.Add("zinc_salt_solution"); //раствор соли цинка
            need.Add("nickel_salt_solution"); //раствор соли никеля

            //это
            need.Add("muriatic_acid"); //солян кисл
            //или это
            need.Add("sulfuric_acid"); //серн кислота

            need.Add("alkaline_solution");

            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            need.Add("pipette");
            need.Add("pipette");
            need.Add("pipette");
            need.Add("pipette");


            if (inv.IndexOf("muriatic_acid") > -1 && inv.IndexOf("sulfuric_acid") == -1)
            {
                inv.Add("sulfuric_acid");
            }
            else if (inv.IndexOf("muriatic_acid") == -1 && inv.IndexOf("sulfuric_acid") > -1)
            {
                inv.Add("muriatic_acid");
            }
            else if (inv.IndexOf("muriatic_acid") > -1 && inv.IndexOf("sulfuric_acid") > -1)
            {
                inv.Add("Ошибка");
            }

            if (CheckArrays(need, inv))
            {
                //Debug.Log("Lab1Start");
                Application.LoadLevel("Lab1");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21349";
            }

        }
        else if (interf.LabNumber == 2)
        {
            need.Add("calcium_oxide"); //оксид кальц
            need.Add("magnesium_oxide"); //оксид магн
            need.Add("muriatic_acid"); //сол кисл
            need.Add("distilled_water");
            need.Add("phenolphthalein");

            need.Add("pipette");

            need.Add("microspatula");
            need.Add("microspatula");

            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            //Debug.Log("В инвентаре " + inv.Attempts);
            //Debug.Log("Нужно " + need.Attempts);
            if (CheckArrays(need, inv))
            {
                //Debug.Log("Lab2Start");
                Application.LoadLevel("Lab2");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21349";
            }
        }
        else if (interf.LabNumber == 3)
        {
            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            need.Add("pipette");
            need.Add("pipette");

            need.Add("sodium_silicate"); //силикат натрия
            need.Add("litmus_solution"); //индикатор раствор лакмуса
            need.Add("muriatic_acid"); //сол кисл
            need.Add("distilled_water");
            need.Add("kippApparatus");

            if (CheckArrays(need, inv))
            {
                //Debug.Log("Lab3Start");
                Application.LoadLevel("Lab3");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21349";
            }
        }
        else if (interf.LabNumber == 4)
        {
            need.Add("scales"); //весы
            need.Add("calorimeter");
            need.Add("thermometer");
            need.Add("measuringCylinder100ml");
            need.Add("distilled_water");
            need.Add("copper_sulfate_anhydrous");

            if (CheckArrays(need, inv))
            {
                Application.LoadLevel("Lab4");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21350";
            }

        }
        else if (interf.LabNumber == 5)
        {
            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            need.Add("microspatula");

            need.Add("pipette");
            need.Add("pipette");

            need.Add("ferric_chloride"); //хлорид железа
            need.Add("ammonium_thiocyanate"); //раствор тиоцината аммония
            need.Add("ammonium_chloride");
            need.Add("distilled_water");

            if (CheckArrays(need, inv))
            {
                //Debug.Log("Lab5Start");
                Application.LoadLevel("Lab5");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21351";
            }
        }
        else if (interf.LabNumber == 6)
        {
            need.Add("areometer");
            need.Add("distilled_water");
            need.Add("sodium_carbonate_solution"); //раст карбоната натрия
            var need2 = new List<string>
            {
                "areometer",
                "distilled_water",
                "sodium_carbonate_solution",
                "measuringCylinder100ml",
                "measuringCylinder250ml",
                "measuringCylinder250ml",
                "beaker"
            };
            Debug.Log(CheckArrays(need2, inv));
            if (CheckArrays(need, inv) || CheckArrays(need2, inv))
            {
                Application.LoadLevel("Lab6");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21352";
            }

        }
        else if (interf.LabNumber == 7)
        {
            need.Add("kippApparatus");
            need.Add("testTube");
            need.Add("pipette");
            need.Add("calcium_hydroxide");
            if (CheckArrays(need, inv))
            {
                Application.LoadLevel("Lab7");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21353";
            }

        }
        else if (interf.LabNumber == 8)
        {
            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            need.Add("pipette");
            need.Add("pipette");
            need.Add("pipette");

            need.Add("copper_sulphate_solution"); //раствор сульфата меди
            need.Add("alkaline_solution"); //раствор щелочи
            need.Add("sulfuric_acid"); //серн кислота

            if (CheckArrays(need, inv))
            {
                //Debug.Log("Lab8Start");
                Application.LoadLevel("Lab8");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21353";
            }
        }
        else if (interf.LabNumber == 9)
        {
            need.Add("indicator_paper");
            need.Add("distilled_water");

            need.Add("testTube");
            need.Add("testTube");
            need.Add("testTube");

            need.Add("aluminum_chloride"); //хлорил алюминия
            need.Add("sodium_carbonate"); //карбоната натрия
            need.Add("potassium_sulfate"); //сульфат калия

            if (CheckArrays(need, inv))
            {
                Application.LoadLevel("Lab9");
            }
            else
            {
                // Time.timeScale = 0;
                Lose = true;
                hyperlink = "http://learn.urfu.ru/resource/index/index/subject_id/589/resource_id/21354";
            }
        }
	}


    bool CheckArrays(List<string> need, List<string> inv)
    {
        var del = false;

        for (var i = 0; i < inv.Count; i++)
        {
            for (var j = 0; j < need.Count; j++)
            {
                if (inv[i] == need[j])
                {;
                    inv.RemoveAt(i);
                    need.RemoveAt(j);
                    del = true;
                    break;
                }
            }
            if (del)
            {
                i = -1;
                del = false;
            }
        }

        if (need.Count == 0 && inv.Count == 0)
        {
            return true;
        }
        return false;
    }

    string hyperlink;

    void OnMouseUp()
    {
        Check();
    }

    string wrongSet;
    string clickLinkText;
    string restartText;
    public override void SetLanguage(string language)
    {
        var translator = gameObject.AddComponent<Translator>();
        ExitButtonText = translator.GetTranslation(NameSpace.Buttons, "exitButtonText", language);
        wrongSet = translator.GetTranslation(NameSpace.ShortTexts, "wrongSet", language);
        clickLinkText = translator.GetTranslation(NameSpace.ShortTexts, "clickLinkText", language);
        restartText = translator.GetTranslation(NameSpace.ShortTexts, "restartText", language);
        Destroy(translator);
    }

    void OnGUI()
    {
        if (Lose)
        {
            var style = new GUIStyle(GUI.skin.textArea);
            style.fontSize = 30;
            style.alignment = TextAnchor.MiddleCenter;
            var buttonStyle = new GUIStyle(GUI.skin.button);
            buttonStyle.fontSize = 15;
            buttonStyle.wordWrap = true;

            var position = new Rect(Screen.width / 2-TextWidth/2, Screen.height / 2-TextHeight/2, TextWidth, TextHeight);
          
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
            GUI.Label(position, wrongSet + "\n" + clickLinkText,style);

            if (GUI.Button(new Rect(Screen.width / 2 - ButtonWidth/2, Screen.height / 2+TextHeight/2, ButtonWidth, ButtonHeight), "learn.urfu.ru", buttonStyle))
            {
                Debug.Log("Переход по ссылке");
                Application.OpenURL(hyperlink);
                
            }
            
            if (GUI.Button(new Rect(Screen.width / 2+TextWidth/2 - ButtonWidth, Screen.height / 2 + TextHeight/2, ButtonWidth, ButtonHeight), ExitButtonText, buttonStyle))
            { // Выход из игры
                if (!Application.isWebPlayer)
                {
                    Application.Quit();
                }
                else
                {
                    Screen.fullScreen = false;
                }
            }
            if (GUI.Button(new Rect(Screen.width / 2 - TextWidth/2, Screen.height / 2 + TextHeight/2, ButtonWidth, ButtonHeight), restartText, buttonStyle))
            { // Выход из игры
                Lose = false;
                Application.LoadLevel("Lab0");                
            }
        }
    }
}

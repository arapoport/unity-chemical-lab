﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckLab3 : CheckLabs {


    ApparatusTime apparatusTime;

	// Use this for initialization
	void Start () {
        base.Start();
        TestTubes = GetDestinationTestTubes();
        apparatusTime = GameObject.Find("apparatusTime").GetComponent<ApparatusTime>();
	}

	public override int FailCount()
	{
        return TestTubes.Where(testTube => testTube.Active).Sum(testTube => testTube.FailCounter);
	}
	
	public override void Check()
	{
        var mixed = (from testTube in TestTubes where testTube.Active select testTube.Content).ToList();

	    var need = new List<string>
	    {
	        "distilled_water+litmus_solution+kippApparatus",
	        "sodium_silicate+muriatic_acid",
	        "sodium_silicate+kippApparatus"
	    };


        GameOver = true;
        var timePassedWithPipe = apparatusTime.GetTimeWithPipeByContent("sodium_silicate+kippApparatus");
        var timeNoPipe = apparatusTime.GetTimeNoPipeByContent("sodium_silicate+kippApparatus");
        if (IsListCorrect(mixed, need) && (timePassedWithPipe == 2 || timePassedWithPipe == 3) && (timeNoPipe >= 5))
        {
            Win = true;

        }
        else
        {
            Lose = true;
        }
        SaveResult(Time.time-StartTime, 3);
	}


	void OnMouseEnter()
	{
		Over = true;
	}
	
	void OnMouseExit()
	{
		Over = false;
	}
	
	void OnMouseUp()
	{
        Check();
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckLab6 : CheckLabs {
	
	// Use this for initialization
	void Start () {
        Watches.LetTheCountBegin = true;
        GameOver = false;
        Win = false;
        Lose = false;
        //cylinder250 = GameObject.Find("measuringCylinder250ml").GetComponent<Destination>();
        cylinders250GameObject = GameObject.FindGameObjectsWithTag("Cylinder250");
        cylinders250 = new Destination[cylinders250GameObject.Length];
        var i = 0;
	    foreach (var cylinderGameObject in cylinders250GameObject)
	    {
	        cylinders250[i] = cylinderGameObject.GetComponent<Destination>();
	        i++;
	    }
        StartTime = Time.time;
	}

    private GameObject[] cylinders250GameObject;
    private Destination[] cylinders250;
	public override int FailCount()
	{
	    return cylinders250.Where(cylinder250 => cylinder250.Active).Sum(cylinder250 => cylinder250.FailCounter);
	}

    public override void Check()
	{

        var mixed = (from cylinder250 in cylinders250 where cylinder250.Active select cylinder250.Content).ToList();
        //Debug.Log("mv.src.name = " + mv.src.name);
        var need = new List<string>
        {
            "sodium_carbonate+measuringCylinder100ml+measuringCylinder250ml+distilled_water+beaker+measuringCylinder250ml+beaker+measuringCylinder250ml+beaker+measuringCylinder250ml",
            "sodium_carbonate+measuringCylinder250ml"
        };

        GameOver = true;
        if (IsListCorrect(mixed, need))
        {
            Win = true;

        }
        else
        {
            Lose = true;
        }
        SaveResult(Time.time-StartTime, 6);
	}

	void OnMouseEnter()
	{
		Over = true;
	}
	
	void OnMouseExit()
	{
		Over = false;
	}
	
	void OnMouseUp()
	{
		Check();
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class CheckLabs : TextProcessing {

    private const string AddResultAdress = "http://urfu-chemistry.tk/web_unity/addResult.php"; //там, где лежит addResult.php

	// Use this for initialization
	protected void Start () {
        GameOver = false;
        Win = false;
        Lose = false;
        StartTime = Time.time;
        Debug.Log("CheckLabs старт");  
	}

    public bool Over;
    
	public abstract int FailCount();
	public abstract void Check();

    public bool GameOver;
    public bool Win;
    public bool Lose;

    protected float TextWidth = 450;
    protected float TextHeight = 200;
    protected float ButtonWidth = 150;
    protected float ButtonHeight = 75;
    protected float StartTime;
    protected Destination[] TestTubes;
    protected string ExitButtonText;

    string returnButtonText;
    string winText;
    string loseText;
    string mistakeText;
    string remainingText;
    string attemptsText;

    int labNumber;
    int time;
    protected void SaveResult(float time, int labNumber)
    {
        this.time = (int)time;
        this.labNumber = labNumber;
        StartCoroutine(SaveIt());
    }

    IEnumerator SaveIt()
    {
        var username = Session.Login;
        var finish = "";
        Debug.Log(Session.Login);
        if (username != "Admin")
        {
            var wwwData = new WWWForm();
            wwwData.AddField("username", username);
            wwwData.AddField("lab_number", labNumber);
            wwwData.AddField("time", time);
            if (Win) { finish = "1"; }
            wwwData.AddField("finish", finish);
            var loginReader = new WWW(AddResultAdress, wwwData);
            yield return loginReader;

            if (loginReader.error != null)
            {
                Debug.Log(loginReader.error);
                errorMessage = "Check your Internet connection";
                yield return new WaitForSeconds(1);
                StartCoroutine(SaveIt());
            }
            else
            {
                Debug.Log(loginReader.text);
                errorMessage = "";
            }
        }
        else
        {
            Debug.Log("Это лаба " + labNumber);
            if (Win)
            {
                Session.labs[labNumber - 1] = true;
            }
            else if (Lose)
            {
                Session.labs[labNumber - 1] = false;
            }
        }
    }

    string errorMessage;

    public virtual void SetLanguage(string language)
    {
        var translator = gameObject.AddComponent<Translator>();
        ExitButtonText = translator.GetTranslation(NameSpace.Buttons, "exitButtonText", language);
        returnButtonText = translator.GetTranslation(NameSpace.Buttons, "returnButtonText", language);
        winText = translator.GetTranslation(NameSpace.ShortTexts, "winText", language);
        loseText = translator.GetTranslation(NameSpace.ShortTexts, "loseText", language);
        mistakeText = translator.GetTranslation(NameSpace.ShortTexts, "mistakeText", language);
        remainingText = translator.GetTranslation(NameSpace.ShortTexts, "remainingText", language);
        attemptsText = translator.GetTranslation(NameSpace.ShortTexts, "attemptsText", language);
        Destroy(translator);
    }

    
    protected Destination[] GetDestinationTestTubes()
    {
        var testTubesGO = GameObject.FindGameObjectsWithTag("TestTube");
        return testTubesGO.Select(testTubeGO => testTubeGO.GetComponent<Destination>()).ToArray();
    }

    protected bool IsListCorrect(List<string> mixed, List<string> need)
    {
        Debug.Log("mixed count=" + mixed.Count);
        for (var i = 0; i < mixed.Count; i++)
        {
            for (var j = 0; j < need.Count; j++)
            {
                if (mixed[i] != need[j]) continue;
                mixed.RemoveAt(i);
                need.RemoveAt(j);
                i = -1;
                break;
            }
        }
        return need.Count == 0 && mixed.Count == 0;
    }


    void OnGUI()
    {
        if (!GameOver) return;
        var style = new GUIStyle(GUI.skin.textArea) {fontSize = 30, alignment = TextAnchor.MiddleCenter};
        var buttonStyle = new GUIStyle(GUI.skin.button) {fontSize = 15, wordWrap = true};
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
        var position = new Rect(Screen.width / 2 - TextWidth/2, Screen.height / 2 - TextHeight/2, TextWidth, TextHeight);
        var buttonPosition = new Rect(Screen.width/2 + TextWidth/2 - ButtonWidth, Screen.height/2 + TextHeight/2, ButtonWidth, ButtonHeight);
        var buttonPosition2 = new Rect(Screen.width / 2 - TextWidth / 2, Screen.height / 2 + TextHeight / 2, ButtonWidth, ButtonHeight);
        GUI.Label(new Rect(Screen.width / 2 - TextWidth / 2, Screen.height / 2 - TextHeight / 2 - 20, TextWidth, 20), errorMessage);
        if (Win)
        {    
            GUI.Label(position, winText ,style);  
            if (GUI.Button(buttonPosition, returnButtonText,buttonStyle))
            {
                Session.Attempts = 4;
                GameOver = false;
                Win = false;
                Lose = false;
                Application.LoadLevel("Lab0");
            }                
        }
        else if (Lose)
        { 
            GUI.Label(position, loseText,style);
            if (Session.Attempts > 0)
            {
                if (GUI.Button(buttonPosition, returnButtonText + "\n" + remainingText + " " + Session.Attempts + " " + attemptsText, buttonStyle))
                {
                    Session.Attempts--;
                    GameOver = false;
                    Win = false;
                    Lose = false;
                    Application.LoadLevel("Lab0");
                }
            }
            else
            {
                GUI.Label(buttonPosition, mistakeText, style);
            }
        }
        if (GUI.Button(buttonPosition2, ExitButtonText,buttonStyle))
        {
            Application.Quit();
        }
    }
}

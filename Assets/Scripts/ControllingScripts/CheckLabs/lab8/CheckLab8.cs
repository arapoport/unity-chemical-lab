﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckLab8 : CheckLabs{

	// Use this for initialization
	void Start () {
        base.Start();
        TestTubes = GetDestinationTestTubes();
	}

	public override int FailCount()
	{
	    return TestTubes.Where(testTube => testTube.Active).Sum(testTube => testTube.FailCounter);
	}

    public override void Check()
	{
        var mixed = (from testTube in TestTubes where testTube.Active select testTube.Content).ToList();

        var need = new List<string>
        {
            "copper_sulphate_solution+alkaline_solution_many",
            "copper_sulphate_solution+alkaline_solution_small+alkaline_solution_small",//это
            "copper_sulphate_solution+alkaline_solution_small+sulfuric_acid"
        };

        var need2 = new List<string>
        {
            "copper_sulphate_solution+alkaline_solution_many",
            "copper_sulphate_solution+alkaline_solution_small+alkaline_solution_many",//или это
            "copper_sulphate_solution+alkaline_solution_small+sulfuric_acid"
        };
        
        GameOver = true;  
        if (IsListCorrect(new List<string>(mixed), need) || IsListCorrect(new List<string>(mixed), need2))
        {
            Win = true;
        }
        else
        {
            Lose = true;
        }
        Debug.Log(Time.time - StartTime);
        SaveResult(Time.time-StartTime, 8);
	}   

    void OnMouseEnter()
    {
        Over = true;
    }

    void OnMouseExit()
    {
        Over = false;
    }
   
    void OnMouseUp()
    {
        Check();
    }
}

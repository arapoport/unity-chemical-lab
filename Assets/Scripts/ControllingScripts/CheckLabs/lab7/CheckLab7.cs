﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckLab7 : CheckLabs {
	
	// Use this for initialization
	void Start () {
        bubbles = GameObject.Find("bubbles").GetComponent<BubblesLab7>();
        base.Start();
        TestTubes = GetDestinationTestTubes();
	}
    BubblesLab7 bubbles;
	public override int FailCount()
	{
        return TestTubes.Where(testTube => testTube.Active).Sum(testTube => testTube.FailCounter);
	}
	
	public override void Check()
	{
        var mixed = (from testTube in TestTubes where testTube.Active select testTube.Content).ToList();

	    var need = new List<string> {"calcium_hydroxide+kippApparatus+calcium_hydroxide"};
        GameOver = true;
        if (IsListCorrect(mixed, need) && !bubbles.PlayerInterruptedKippPrematurely)
        {
            Win = true;
        }
        else
        {
            Lose = true;
        }
        SaveResult(Time.time-StartTime, 7);
	}

	void OnMouseEnter()
	{
		Over = true;
	}
	
	void OnMouseExit()
	{
		Over = false;
	}
	
	void OnMouseUp()
	{
		Check();
	}
}

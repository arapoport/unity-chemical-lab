﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class BubblesLab7 : Bubbles {

    public Material CalciumHydroxideBubblesFirst;
    public Material CalciumHydroxideBubblesSecond;
    public bool PlayerInterruptedKippPrematurely;

    public override void StartBubbles(GameObject substance)
    {
        this.substance = substance;
        start = true;
        transformation.TransformBubbles(substance, TimeConstants.BubblesDefaultDuration, true);
        StartCoroutine(ChangeBubbleTexture(new[] {CalciumHydroxideBubblesFirst, CalciumHydroxideBubblesSecond}, new[] {3f, 3f}));;
    }
    protected override IEnumerator ChangeBubbleTexture(Material[] materials, float[] delays) //Изменять текстуру вещества с течением времени
    {
        for (var i = 0; i < materials.Length; i++)
        {
            yield return new WaitForSeconds(delays[i]);
            if (!start)
            {
                PlayerInterruptedKippPrematurely = true;
                yield break;
            }
            substance.renderer.material = materials[i];
            yield return null;
        }
    }
}

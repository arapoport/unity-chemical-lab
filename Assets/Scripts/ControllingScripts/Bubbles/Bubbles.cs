﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class Bubbles : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	protected Transformation transformation = new Transformation(); 
    protected GameObject substance;
    protected bool start;

    public virtual void StartBubbles(GameObject substance)
    {
        this.substance = substance;
        start = true;
		transformation.TransformBubbles(substance, TimeConstants.BubblesDefaultDuration, true);
    }
    public virtual void BubbleWithoutScaling(GameObject substance)
    {
        this.substance = substance;
        start = true;
    }
    public virtual void StopBubbles()
    {
        transformation.TransformBubbles(substance, TimeConstants.BubblesDefaultDuration, false);
        start = false;       
    }
    public IEnumerator Bubble(float duration)
    {
        start = true;
		transformation.TransformBubbles(substance, duration, true);
        yield return new WaitForSeconds(duration);
		transformation.TransformBubbles(substance, duration, false);
        yield return new WaitForSeconds(duration);
        start = false;
    }

    protected virtual IEnumerator ChangeBubbleTexture(Material[] materials, float[] delays) //Изменять текстуру вещества с течением времени
    {
        yield return null;
    }

	// Update is called once per frame
	void Update () {
        if (start)
        {
            substance.renderer.material.mainTextureOffset =
                new Vector2(substance.renderer.material.mainTextureOffset.x,
                            substance.renderer.material.mainTextureOffset.y + 0.05f);
        }
	}
}

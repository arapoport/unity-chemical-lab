﻿using UnityEngine;

public class Destination : MonoBehaviour {

	// Use this for initialization
	void Start () {
        controller = GameObject.Find("controller").GetComponent<Controller>();
        DefaultContent = Content;
		checkLabs = GameObject.Find("ready").GetComponent<CheckLabs>();
	}

    Controller controller;

    public bool Active;
    public int FailCounter;
    public string Content = "empty";
    CheckLabs checkLabs;

    public string DefaultContent { get; private set; }

    public GameObject GetSubstance() {
		return transform.FindChild("substance").gameObject;
	}

	public GameObject GetTop() //пустой объект в destination который расположен сверху
	{ 
		return transform.FindChild("top").gameObject;
	}
	public GameObject GetBottom() //пустой объект в destination который расположен снизу
	{ 
		return transform.FindChild("bot").gameObject;
	}

	public void SetDestinationSubstanceMaterialAndContent(string objectName, CheckLabs check, Reactions reactions)
	{
		string result;
		var material = reactions.GetMaterial(Content + "+" + objectName, out result);
		if (material.name != "fail")
		{
			GetSubstance().renderer.material = material;
		}
		SetContent(result);
	}

	public void SetContent(string result)
	{
		Content = result;
	    if (result != "fail") return;
	    FailCounter++;
	    if (checkLabs.FailCount() > 4)
	    {
	        checkLabs.Check();
	    }
	}

    public bool CheckAvailableVolume(SourceParent source)
    {
        var result = false;
        var srcSubst = source.GetSubstance();
        var substanceH = srcSubst.transform.lossyScale.y;
        var substanceR = srcSubst.transform.lossyScale.x;
        var destSubst = GetSubstance();
        var top = transform.FindChild("top").gameObject; 
        var volume = substanceR * substanceR * substanceH * (source.Pour / 100);
        var destLossyY = destSubst.transform.lossyScale.y; //типа высота
        var destLossyX = destSubst.transform.lossyScale.x; //типа радиус 
        var destLocalY = destSubst.transform.localScale.y;
        var destInscreaseLossy = volume / (destLossyX * destLossyX); //на сколько бы мы увеличили lossyScale
        var x = destLocalY / destLossyY;
        var increaseLocal = x * destInscreaseLossy; //на сколько увеличиваем localScale.y
        var destSubstNewPos = destSubst.transform.localPosition.y + increaseLocal;
        var destSubstNewScale = destSubst.transform.localScale.y + increaseLocal;
        var t1 = destSubstNewPos + destSubstNewScale;
        var t2 = Mathf.Abs(top.transform.localPosition.y);
        if (t1 <= t2)
        {
            result = true;
        }
        else
        {
            Debug.Log("t1=" + t1);
            Debug.Log("top=" + t2);
        }

        return result;
    }

    public float GetSourcePercent(SourceParent source) //получить процент для источника чтоб заполнить до краев
    {
        var destSubst = GetSubstance();
        var srcSubst = source.GetSubstance();
        var srcLossyY = srcSubst.transform.lossyScale.y;
        var srcLossyX = srcSubst.transform.lossyScale.x;       
        var destLossyY = destSubst.transform.lossyScale.y; //типа высота
        var destLossyX = destSubst.transform.lossyScale.x; //типа радиус  
        var x = destLossyY / destSubst.transform.localScale.y;
        var top = transform.FindChild("top").gameObject;
        var increaseLocal = (Mathf.Abs(top.transform.localPosition.y) - destSubst.transform.localPosition.y - destSubst.transform.localScale.y) / 2;        
        var increaseLossy = x * increaseLocal;
        var volume = increaseLossy * (destLossyX * destLossyX);
        var pour = (volume * 100) / (srcLossyX * srcLossyX * srcLossyY);
        return pour-0.5f;
    }

    void OnMouseEnter()
    {
        MyCursor.SetClick();
    }

    void OnMouseExit()
    {
        MyCursor.SetNormal();
    }


    void OnMouseUp()
    {
        controller.AddDestination(this);
    }
}

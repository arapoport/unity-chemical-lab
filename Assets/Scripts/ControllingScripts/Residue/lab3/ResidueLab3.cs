﻿using UnityEngine;

public class ResidueLab3 : Residue {

	// Use this for initialization
	void Start () {
        Controller = GameObject.Find("controller").GetComponent<Controller>();
        ApparatusTime = GameObject.Find("apparatusTime").GetComponent<ApparatusTime>();
	}

	private string content = "";	//текст содержимого пробирки

    protected override void DetermineResidueStatus(TestTube testTube) //определить статус осадка в пробирке (и записать его в поле)
    {
		content = testTube.GetContent();
        Status = GetStatusByContent(content);
	}

	public override bool GetStatusByContent(string content)
	{
        var timeNoPipe = 0; //сколько времени прошло после того, как шланг аппарата Киппа вынули.
		switch (content) //смотрим, что есть в пробирке
		{
		    case "sodium_silicate+kippApparatus": //если в пробирке силикат натрия, и туда опускали трубку аппарата Киппа
		        try { timeNoPipe = ApparatusTime.GetTimeNoPipeByContent(content); } //то мы смотрим, сколько времени прошло после того, как трубку вынули
		        catch { }
		        if (timeNoPipe < 5) //если трубку аппарата Киппа вынули менее, чем 5 минут назад
		        {
		            Status = false; //осадка нет
		        }
		        else if (timeNoPipe >= 5) //если трубку аппарата Киппа вынули более, чем 5 минут назад, либо ровно 5 минут назад
		        {
		            Status = true; //осадок есть
		        }
		        break;
		    case "sodium_silicate+muriatic_acid": //если в пробирке силикат натрия и соляная кислота
		        Status = true; //осадок есть
		        break;
		    default: //если в пробирке что-то другое
		        Status = false; //осадка нет
		        break;
		}
		return Status;
	}

	// Update is called once per frame
	void Update () {
		
	}
}

﻿using UnityEngine;

public abstract class Residue : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

    protected Controller Controller;
    protected ApparatusTime ApparatusTime;
    protected bool Status; //статус осадка. False = нет осадка. True = осадок есть.

    public string GetActionName() //получить имя действия (либо осадок есть, значит метод будет называться TrueResidueMove, либо осадка нет, метод FalseResidueMove)
    {
        if (Status) //есть осадок
        {
            return "TrueResidueMove";
        }
        return "FalseResidueMove";
    }

    public void SetResidueStatus(bool status)
    {
        Status = status;
    }
    public bool GetResidueStatus()
    {
        return Status;
    }

    protected abstract void DetermineResidueStatus(TestTube testTube); //определить статус осадка в пробирке (и записать его в поле)

	public abstract bool GetStatusByContent(string content);

    public void Check(TestTube testTube, ResiduePlate residuePlate) //проверить пробирку на наличие осадка, пытаясь вылить содержимое в тарелку.
    {
        DetermineResidueStatus(testTube); //определяем статус осадка в пробирке
        residuePlate.StartCoroutine(GetActionName(), testTube); //задаём пробирке движение в зависимости от того, есть там осадок, или нет.
    }

	// Update is called once per frame
	void Update () {
	
	}
}

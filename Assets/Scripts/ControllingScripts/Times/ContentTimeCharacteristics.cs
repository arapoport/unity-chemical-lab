﻿public class ContentTimeCharacteristics {
    public string Name { get; set; }
    public int TimePassedWithPipe { get; set; }
    public int TimeWhenPipeDisconnected { get; set; }
}

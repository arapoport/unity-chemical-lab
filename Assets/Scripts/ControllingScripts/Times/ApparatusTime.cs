using System;
using System.Collections.Generic;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

public class ApparatusTime : MonoBehaviour
{
	// Use this for initialization
	void Start () {
	}

    private int timeWhenPipeConnected;
    private int timeWhenPipeDisconnected;
    public Dictionary<string, ContentTimeCharacteristics> ContentTimes = new Dictionary<string, ContentTimeCharacteristics>();

    public int GetTimeNoPipeByContent(string content)
    {
        var theContentTime = new ContentTimeCharacteristics();
        ContentTimes.TryGetValue(content, out theContentTime);
        return (int)(Convert.ToDouble(Watches.GetTimeInSeconds() - theContentTime.TimeWhenPipeDisconnected) / 60);
    }
    public int GetTimeWithPipeByContent(string content)
    {
        ContentTimeCharacteristics theContentTime;
        ContentTimes.TryGetValue(content, out theContentTime);
        Debug.Assert(theContentTime != null, "theContentTime != null");
        return theContentTime.TimePassedWithPipe;
    }

    public void AddToDictionary(Dictionary<string, ContentTimeCharacteristics> contentTimes,
        string name, int timePassedWithPipe, int timeWhenPipeDisconnected)
    {
        var theContentTime = new ContentTimeCharacteristics
        {
            Name = name,
            TimePassedWithPipe = timePassedWithPipe,
            TimeWhenPipeDisconnected = timeWhenPipeDisconnected
        };
        contentTimes.Add(theContentTime.Name, theContentTime);
    }


    public void SetTimeWhenPipeConnected(int time)
    {
        timeWhenPipeConnected = time;
    }

    public int GetTimeWhenPipeDisconnected()
    {
        return timeWhenPipeDisconnected;
    }

    public void SetTimeWhenPipeDisconnected(int time)
    {
        timeWhenPipeDisconnected = time;
    }

    public int GetTimeWithPipe()
    {
        return (int)(Convert.ToDouble(timeWhenPipeDisconnected - timeWhenPipeConnected) / 60);     
    }

	// Update is called once per frame
	void Update () {
	}
}

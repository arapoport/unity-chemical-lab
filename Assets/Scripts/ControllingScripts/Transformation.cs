using System.Collections;
using DG.Tweening;
using UnityEngine;
using System;
public class Transformation
{
	public Transformation ()
	{
			
	}
	
	public void TransformScaleYWithSamePositionInTube(GameObject substance, float diffCoef, float duration) {
		substance.transform.DOScaleY((1 + diffCoef) * substance.transform.localScale.y, duration);
		substance.transform.DOLocalMoveY(substance.transform.localPosition.y + diffCoef *  substance.transform.localScale.y, duration);
	}

	public void TransformBubbles(GameObject substance, float duration, bool isNeededMore) {
		float signCoef = isNeededMore ? 0.5f : -0.5f;
		TransformScaleYWithSamePositionInTube(substance, signCoef, duration);
	}
}



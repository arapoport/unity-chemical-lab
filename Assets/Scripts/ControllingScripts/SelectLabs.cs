﻿using UnityEngine;

public class SelectLabs : TextProcessing {

	// Use this for initialization
	void Start () {
        labsGUI = GameObject.FindObjectOfType<LabsGUI>();
	}

    LabsGUI labsGUI;
    public string TextRus;
    public string TextEng;
    string language;


    public void SetLanguage(string lang)
    {
        if (lang == "rus")
        {
            guiText.text = guiText.text[0] + TextRus.Substring(1, TextRus.Length-1);
        }
        else if (lang == "eng")
        {
            guiText.text = guiText.text[0] + TextEng.Substring(1, TextEng.Length - 1);
        }
        language = lang;
    }

    public void Check(bool check)
    {
        if (check)
        {
            guiText.text = "☑ " + guiText.text.Substring(2, guiText.text.Length - 2);
        }
        else
        {
            guiText.text = "☐ " + guiText.text.Substring(2, guiText.text.Length - 2);
        }
    }


    public void Show()
    {
        guiText.enabled = true;
        guiText.fontSize = GetFontSize();
    }

    public void reSize()
    {
        guiText.fontSize = GetFontSize();
    }

    public void Hide()
    {
        guiText.enabled = false;
    }

    string workText="";
    
    void OnMouseEnter()
    {
        guiText.fontStyle = FontStyle.BoldAndItalic;
        var translator = gameObject.AddComponent<Translator>();
        var str = GetLinesFromString(translator.GetPlainTextTranslation(NameSpace.Purposes, "Lab" + LabNumber, language));
        Destroy(translator);  
        labsGUI.Show(str);
    }

    void OnMouseExit()
    {
        guiText.fontStyle = FontStyle.Normal;       
    }



    private int LastIndexOf(string str, char ch, int start, int count)
    {
        var result = -1;
        for (var i = start; i < start + count; i++)
        {
            if (str[i] == ch)
            {
                result = i;
            }
        }

        return result;
    }

    public int LabNumber;

    

    void OnMouseUp()
    {
        var interf = GameObject.FindObjectOfType<Interf>();
        interf.LoadText(LabNumber);

        var wr = GameObject.FindObjectOfType<WorkResults>();
        wr.guiTexture.enabled = true;
        wr.Number = LabNumber;
        wr.Show();

        var notebook = GameObject.FindObjectOfType<BookLab0>();
        notebook.Hide();
        var start = GameObject.FindObjectOfType<StartLab0>();
        start.Show();
    }   

}

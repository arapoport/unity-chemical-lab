﻿using System;
using UnityEngine;

public class SubstanceStateLab3 : SubstanceState {

    // Use this for initialization
    void Start()
    {
        Controller = GameObject.Find("controller").GetComponent<Controller>();
        ApparatusTime = GameObject.Find("apparatusTime").GetComponent<ApparatusTime>();
    }

    private string content = "";	//текст содержимого пробирки

    protected override void DetermineSubstanceStatus(TestTube testTube) //определить статус осадка в пробирке (и записать его в поле)
    {
        /*считаем время, которое вещество стоит после того, как из него вынули трубку аппарата Киппа. Для этого вычитаем из текущего времени
          то значение времени, когда трубку вынули. Причём, если в тот момент значение секунд было больше, то из итога вычитаем 1 минуту, так как она - неполная.  */
        content = testTube.GetContent();
        SetSubstanceState((State) GetSubstanceStatusByContent(content)) ;
    }

    public override Enum GetSubstanceStatusByContent(string content)
    {
        var state = State.Liquid;
        var timePassedWithPipe = 0;
        var timeNoPipe = 0; //сколько времени прошло после того, как шланг аппарата Киппа вынули.
        if (content == null)
        {
            return state;
        }
        switch (content)
        {
            case "sodium_silicate+kippApparatus":
                try
                {
                    timePassedWithPipe = ApparatusTime.GetTimeWithPipeByContent(content);
                    timeNoPipe = ApparatusTime.GetTimeNoPipeByContent(content);
                }
                catch { }
                if (timePassedWithPipe == 2 || timePassedWithPipe == 3)
                {
                    state = timeNoPipe < 5 ? State.Amorphous : State.Solid;
                }
                break;
            case "sodium_silicate+muriatic_acid":
                state = State.Solid; //осадок есть
                break;
            default:
                state = State.Liquid; //осадка нет
                break;
        }
        return state;
    }
	// Update is called once per frame
	void Update () {
	
	}
}

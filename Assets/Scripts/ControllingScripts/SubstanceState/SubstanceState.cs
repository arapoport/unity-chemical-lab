﻿using System;
using UnityEngine;

public enum State { Liquid = 0, Solid = 1, Amorphous = 2 }; //состояние вещества (жидкое, твердое, аморфное)

public abstract class SubstanceState : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    protected Controller Controller;
    protected ApparatusTime ApparatusTime;

    State state;
    public string GetActionName() //получить имя действия
    {
        switch (Enum.GetName(typeof(State), state))
        {
            case "Solid":
                return "SolidSubstanceMove";
            case "Liquid":
                return "LiquidSubstanceMove";
        }
        return "AmorphousSubstanceMove";
    }

    public void SetSubstanceState(State state)
    {
        this.state  = state;
    }
    public Enum GetResidueStatus()
    {
        return state;
    }

    protected abstract void DetermineSubstanceStatus(TestTube testTube); //определить статус осадка в пробирке (и записать его в поле)

    public abstract Enum GetSubstanceStatusByContent(string content);

    public void Check(TestTube testTube, ResiduePlate residuePlate) //проверить пробирку на наличие осадка, пытаясь вылить содержимое в тарелку.
    {
        DetermineSubstanceStatus(testTube); //определяем статус вещества в пробирке
        residuePlate.StartCoroutine(GetActionName(), testTube); //задаём пробирке движение в зависимости от того, каково состояние вещества.
    }

	// Update is called once per frame
	void Update () {
	
	}
}

﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.IO;
using LitJson;
using System.Collections.Specialized;

public class JSONGroups {
	public static ArrayList getGroupsList(String json)
	{
		try
		{
			ArrayList array = new ArrayList ();
			List<Groups> groups = JsonMapper.ToObject<List<Groups>> (json);
			foreach (var g in groups)
 			{
				array.Add(g.title);
			}
			return array;
		}
		catch {				}
		return null;
	}	
}
